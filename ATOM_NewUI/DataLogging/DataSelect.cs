﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using ATOM_Class;

namespace ATOM_NewUI.DataLogging
{
    public partial class DataSelect : DevExpress.XtraEditors.XtraForm
    {
        List<VariableInfo> constantVariables = null;
        List<VariableInfo> selectedVariables = null;
        CommunicationConnector communicationConnector;
        public DataSelect(List<VariableInfo> constantVariables)
        {
            InitializeComponent();
            communicationConnector = CommunicationConnector.GetInstance();
            this.constantVariables = constantVariables;

            cbChannelList.Properties.Items.Clear();

            foreach (CommunicationType commType in (CommunicationType[])Enum.GetValues(typeof(CommunicationType)))
            {
                cbChannelList.Properties.Items.Add(commType.ToString());
            }

            cbChannelList.SelectedIndex = 0;
        }

        private void cbChannelList_SelectedIndexChanged(object sender, EventArgs e)
        {
            gridControl1.BeginUpdate();
            selectedVariables = communicationConnector.GetVariables((CommunicationType)cbChannelList.SelectedIndex);
            gridControl1.DataSource = selectedVariables;
            gridControl1.EndUpdate();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            int[] selected = gridView1.GetSelectedRows();

            for (int i = 0; i < selected.Length; i++)
            {
                constantVariables.Add(selectedVariables[selected[i]]);
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}