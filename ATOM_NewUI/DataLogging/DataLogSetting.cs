﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using ATOM_Class;

namespace ATOM_NewUI.DataLogging
{
    public partial class DataLogSetting : DevExpress.XtraEditors.XtraForm
    {
        DataLogger dataLogger;
        ConstantLogging constantLogging;
        public DataLogSetting(DataLogger dataLogger)
        {
            InitializeComponent();
            this.dataLogger = dataLogger;

            constantLogging = dataLogger.GetConstantLoggingInstance();

            gridControl1.DataSource = constantLogging.ConstantDataLogging_Variables;

            if (constantLogging.FilePath != string.Empty)
            {
                tbFilePath.Text = constantLogging.FilePath;
            }
            else
            {
                tbFilePath.Text = Application.StartupPath;
            }
            tbFilePrefix.Text = constantLogging.DataFilePrefix;
            tbLoggingInterval.Text = constantLogging.LoggingInterval.ToString();
            tbMaxRowCount.Text = constantLogging.MaxRowsPerFileConstant.ToString();
        }

        private void btnUp_Click(object sender, EventArgs e)
        {
            int[] selected = gridView1.GetSelectedRows();
            if (selected.Length > 1)
            {
                XtraMessageBox.Show("<size=14>한 줄만 선택해주세요.</size>", "<size=14>에러</size>", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            constantLogging.VariableUp(gridView1.FocusedRowHandle);

            gridView1.FocusedRowHandle--;

            gridControl1.BeginUpdate();
            gridControl1.EndUpdate();
        }

        private void btnDown_Click(object sender, EventArgs e)
        {
            int[] selected = gridView1.GetSelectedRows();
            if (selected.Length > 1)
            {
                XtraMessageBox.Show("<size=14>한 줄만 선택해주세요.</size>", "<size=14>에러</size>", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            constantLogging.VariableDown(gridView1.FocusedRowHandle);

            gridView1.FocusedRowHandle++;

            gridControl1.BeginUpdate();
            gridControl1.EndUpdate();
        }

        private void btnChangePath_Click(object sender, EventArgs e)
        {
            xtraFolderBrowserDialog1.ShowDialog();

            if (xtraFolderBrowserDialog1.SelectedPath == "xtraFolderBrowserDialog1")
            {
                tbFilePath.Text = constantLogging.FilePath;
            }
            else
            {
                tbFilePath.Text = xtraFolderBrowserDialog1.SelectedPath;
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            DataSelect newform = new DataSelect(constantLogging.ConstantDataLogging_Variables);
            newform.ShowDialog();

            gridControl1.BeginUpdate();
            gridControl1.EndUpdate();
        }

        private void btnRemove_Click(object sender, EventArgs e)
        {
            int[] selected = gridView1.GetSelectedRows();

            if (selected[0] == 0)
            {
                return;
            }

            constantLogging.RemoveVariables(selected);

            gridControl1.BeginUpdate();
            gridControl1.EndUpdate();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {

            constantLogging.FilePath = tbFilePath.Text;
            constantLogging.DataFilePrefix = tbFilePrefix.Text;
            try
            {
                constantLogging.LoggingInterval = int.Parse(tbLoggingInterval.Text);
            }
            catch (Exception)
            {
                constantLogging.LoggingInterval = 1000;
            }
            try
            {
                constantLogging.MaxRowsPerFileConstant = int.Parse(tbMaxRowCount.Text);
            }
            catch (Exception)
            {
                constantLogging.MaxRowsPerFileConstant = 10000;
            }

            this.Close();
        }
    }
}