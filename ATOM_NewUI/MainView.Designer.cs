﻿namespace ATOM_NewUI
{
    partial class MainView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainView));
            DevExpress.XtraBars.Docking2010.Views.Tabbed.DockingContainer dockingContainer1 = new DevExpress.XtraBars.Docking2010.Views.Tabbed.DockingContainer();
            this.documentGroup1 = new DevExpress.XtraBars.Docking2010.Views.Tabbed.DocumentGroup(this.components);
            this.documentFixed = new DevExpress.XtraBars.Docking2010.Views.Tabbed.Document(this.components);
            this.document4 = new DevExpress.XtraBars.Docking2010.Views.Tabbed.Document(this.components);
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.barQuickButton = new DevExpress.XtraBars.Bar();
            this.barButtonItemConnect_Quick = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemDisconnect_Quick = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemHWConfig_Quick = new DevExpress.XtraBars.BarButtonItem();
            this.barTopStripMenu = new DevExpress.XtraBars.Bar();
            this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            this.popupMenuSetting = new DevExpress.XtraBars.PopupMenu(this.components);
            this.barButtonItemCommunicationConfig = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemACSMapping = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemDataLoggingSetting = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem2 = new DevExpress.XtraBars.BarButtonItem();
            this.popupMenuMonitoring = new DevExpress.XtraBars.PopupMenu(this.components);
            this.barButtonItemAddMonitoring = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemMonitoringVisible = new DevExpress.XtraBars.BarButtonItem();
            this.popupMenuMonitoringVisible = new DevExpress.XtraBars.PopupMenu(this.components);
            this.barButtonItemMonitoringRemove = new DevExpress.XtraBars.BarButtonItem();
            this.popupMenuMonitoringRemove = new DevExpress.XtraBars.PopupMenu(this.components);
            this.barButtonItem3 = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.barButtonItemNew = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemLoad = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemSave = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemSaveAs = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemExitApplication = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemVariableSettings = new DevExpress.XtraBars.BarButtonItem();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryItemTextEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.splitContainerControl2 = new DevExpress.XtraEditors.SplitContainerControl();
            this.xtraTabControlMain = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPageMain = new DevExpress.XtraTab.XtraTabPage();
            this.treeListMain = new DevExpress.XtraTreeList.TreeList();
            this.xtraTabPageModules = new DevExpress.XtraTab.XtraTabPage();
            this.xtraUserControlMain = new DevExpress.XtraEditors.XtraUserControl();
            this.xtraTabControBottom = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPageLog = new DevExpress.XtraTab.XtraTabPage();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.ColType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemPictureEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit();
            this.ColTime = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTimeEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit();
            this.ColMessage = new DevExpress.XtraGrid.Columns.GridColumn();
            this.xtraTabPageControlPanel = new DevExpress.XtraTab.XtraTabPage();
            this.pnABBDriveControlPad = new System.Windows.Forms.Panel();
            this.documentManagerMain = new DevExpress.XtraBars.Docking2010.DocumentManager(this.components);
            this.tabbedView1 = new DevExpress.XtraBars.Docking2010.Views.Tabbed.TabbedView(this.components);
            this.tmrUpdateUI = new System.Windows.Forms.Timer(this.components);
            this.svgImageCollection1 = new DevExpress.Utils.SvgImageCollection(this.components);
            this.popupMenuVariables = new DevExpress.XtraBars.PopupMenu(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.documentGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.documentFixed)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.document4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenuSetting)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenuMonitoring)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenuMonitoringVisible)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenuMonitoringRemove)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).BeginInit();
            this.splitContainerControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControlMain)).BeginInit();
            this.xtraTabControlMain.SuspendLayout();
            this.xtraTabPageMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.treeListMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControBottom)).BeginInit();
            this.xtraTabControBottom.SuspendLayout();
            this.xtraTabPageLog.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPictureEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTimeEdit1)).BeginInit();
            this.xtraTabPageControlPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.documentManagerMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.svgImageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenuVariables)).BeginInit();
            this.SuspendLayout();
            // 
            // documentGroup1
            // 
            this.documentGroup1.Items.AddRange(new DevExpress.XtraBars.Docking2010.Views.Tabbed.Document[] {
            this.documentFixed,
            this.document4});
            // 
            // documentFixed
            // 
            this.documentFixed.Caption = "";
            this.documentFixed.ControlName = "documentFixed";
            this.documentFixed.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("documentFixed.ImageOptions.Image")));
            this.documentFixed.ImageOptions.SvgImageSize = new System.Drawing.Size(16, 16);
            this.documentFixed.Properties.AllowActivate = DevExpress.Utils.DefaultBoolean.False;
            this.documentFixed.Properties.AllowAnimation = DevExpress.Utils.DefaultBoolean.False;
            this.documentFixed.Properties.AllowClose = DevExpress.Utils.DefaultBoolean.False;
            this.documentFixed.Properties.AllowDock = DevExpress.Utils.DefaultBoolean.False;
            this.documentFixed.Properties.AllowDockFill = DevExpress.Utils.DefaultBoolean.False;
            this.documentFixed.Properties.AllowFloat = DevExpress.Utils.DefaultBoolean.False;
            this.documentFixed.Properties.AllowFloatOnDoubleClick = DevExpress.Utils.DefaultBoolean.False;
            this.documentFixed.Properties.AllowGlyphSkinning = DevExpress.Utils.DefaultBoolean.False;
            this.documentFixed.Properties.AllowPin = DevExpress.Utils.DefaultBoolean.False;
            this.documentFixed.Properties.AllowTabReordering = DevExpress.Utils.DefaultBoolean.False;
            this.documentFixed.Properties.ShowInDocumentSelector = DevExpress.Utils.DefaultBoolean.False;
            this.documentFixed.Properties.ShowPinButton = DevExpress.Utils.DefaultBoolean.False;
            // 
            // document4
            // 
            this.document4.Caption = "document4";
            this.document4.ControlName = "document4";
            // 
            // bar2
            // 
            this.bar2.BarName = "Main menu";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar2.OptionsBar.MultiLine = true;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Main menu";
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.barQuickButton,
            this.barTopStripMenu});
            this.barManager1.Categories.AddRange(new DevExpress.XtraBars.BarManagerCategory[] {
            new DevExpress.XtraBars.BarManagerCategory("Control", new System.Guid("70ed722d-8730-4538-86b9-e2820241633d"))});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barButtonItemConnect_Quick,
            this.barButtonItemNew,
            this.barButtonItemLoad,
            this.barButtonItemSave,
            this.barButtonItemDisconnect_Quick,
            this.barButtonItemSaveAs,
            this.barButtonItemExitApplication,
            this.barButtonItem1,
            this.barButtonItemCommunicationConfig,
            this.barButtonItemHWConfig_Quick,
            this.barButtonItemACSMapping,
            this.barButtonItemDataLoggingSetting,
            this.barButtonItem2,
            this.barButtonItemAddMonitoring,
            this.barButtonItemMonitoringVisible,
            this.barButtonItemMonitoringRemove,
            this.barButtonItem3,
            this.barButtonItemVariableSettings});
            this.barManager1.MainMenu = this.barTopStripMenu;
            this.barManager1.MaxItemId = 33;
            this.barManager1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit1,
            this.repositoryItemTextEdit2});
            // 
            // barQuickButton
            // 
            this.barQuickButton.BarAppearance.Normal.BackColor = System.Drawing.Color.Transparent;
            this.barQuickButton.BarAppearance.Normal.Options.UseBackColor = true;
            this.barQuickButton.BarName = "Tools";
            this.barQuickButton.DockCol = 0;
            this.barQuickButton.DockRow = 1;
            this.barQuickButton.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.barQuickButton.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItemConnect_Quick),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItemDisconnect_Quick),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItemHWConfig_Quick)});
            this.barQuickButton.OptionsBar.AllowQuickCustomization = false;
            this.barQuickButton.OptionsBar.DrawDragBorder = false;
            this.barQuickButton.Text = "Tools";
            // 
            // barButtonItemConnect_Quick
            // 
            this.barButtonItemConnect_Quick.Caption = "연결";
            this.barButtonItemConnect_Quick.CategoryGuid = new System.Guid("70ed722d-8730-4538-86b9-e2820241633d");
            this.barButtonItemConnect_Quick.Id = 0;
            this.barButtonItemConnect_Quick.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("barButtonItemConnect_Quick.ImageOptions.SvgImage")));
            this.barButtonItemConnect_Quick.Name = "barButtonItemConnect_Quick";
            this.barButtonItemConnect_Quick.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemConnect_Quick_ItemClick);
            // 
            // barButtonItemDisconnect_Quick
            // 
            this.barButtonItemDisconnect_Quick.Caption = "중단";
            this.barButtonItemDisconnect_Quick.CategoryGuid = new System.Guid("70ed722d-8730-4538-86b9-e2820241633d");
            this.barButtonItemDisconnect_Quick.Id = 7;
            this.barButtonItemDisconnect_Quick.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("barButtonItemDisconnect_Quick.ImageOptions.SvgImage")));
            this.barButtonItemDisconnect_Quick.Name = "barButtonItemDisconnect_Quick";
            this.barButtonItemDisconnect_Quick.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemDisconnect_Quick_ItemClick);
            // 
            // barButtonItemHWConfig_Quick
            // 
            this.barButtonItemHWConfig_Quick.Caption = "통신 연결 설정";
            this.barButtonItemHWConfig_Quick.Id = 19;
            this.barButtonItemHWConfig_Quick.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("barButtonItemHWConfig_Quick.ImageOptions.SvgImage")));
            this.barButtonItemHWConfig_Quick.Name = "barButtonItemHWConfig_Quick";
            this.barButtonItemHWConfig_Quick.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemCommunicationConfig_ItemClick);
            // 
            // barTopStripMenu
            // 
            this.barTopStripMenu.BarName = "Main menu";
            this.barTopStripMenu.DockCol = 0;
            this.barTopStripMenu.DockRow = 0;
            this.barTopStripMenu.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.barTopStripMenu.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem2),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem3)});
            this.barTopStripMenu.OptionsBar.AllowQuickCustomization = false;
            this.barTopStripMenu.OptionsBar.DrawDragBorder = false;
            this.barTopStripMenu.OptionsBar.MultiLine = true;
            this.barTopStripMenu.OptionsBar.UseWholeRow = true;
            this.barTopStripMenu.Text = "Main menu";
            // 
            // barButtonItem1
            // 
            this.barButtonItem1.ActAsDropDown = true;
            this.barButtonItem1.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.DropDown;
            this.barButtonItem1.Caption = "설정";
            this.barButtonItem1.DropDownControl = this.popupMenuSetting;
            this.barButtonItem1.Id = 17;
            this.barButtonItem1.Name = "barButtonItem1";
            // 
            // popupMenuSetting
            // 
            this.popupMenuSetting.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItemCommunicationConfig),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItemACSMapping),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItemDataLoggingSetting)});
            this.popupMenuSetting.Manager = this.barManager1;
            this.popupMenuSetting.Name = "popupMenuSetting";
            // 
            // barButtonItemCommunicationConfig
            // 
            this.barButtonItemCommunicationConfig.Caption = "통신 연결 설정";
            this.barButtonItemCommunicationConfig.Id = 18;
            this.barButtonItemCommunicationConfig.Name = "barButtonItemCommunicationConfig";
            this.barButtonItemCommunicationConfig.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemCommunicationConfig_ItemClick);
            // 
            // barButtonItemACSMapping
            // 
            this.barButtonItemACSMapping.Caption = "ACS 맵핑";
            this.barButtonItemACSMapping.Id = 25;
            this.barButtonItemACSMapping.Name = "barButtonItemACSMapping";
            this.barButtonItemACSMapping.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemACSMapping_ItemClick);
            // 
            // barButtonItemDataLoggingSetting
            // 
            this.barButtonItemDataLoggingSetting.Caption = "데이터 로깅 설정";
            this.barButtonItemDataLoggingSetting.Id = 26;
            this.barButtonItemDataLoggingSetting.Name = "barButtonItemDataLoggingSetting";
            this.barButtonItemDataLoggingSetting.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemDataLoggingSetting_ItemClick);
            // 
            // barButtonItem2
            // 
            this.barButtonItem2.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.DropDown;
            this.barButtonItem2.Caption = "모니터링";
            this.barButtonItem2.DropDownControl = this.popupMenuMonitoring;
            this.barButtonItem2.Id = 27;
            this.barButtonItem2.Name = "barButtonItem2";
            // 
            // popupMenuMonitoring
            // 
            this.popupMenuMonitoring.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItemAddMonitoring),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItemMonitoringVisible),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItemMonitoringRemove)});
            this.popupMenuMonitoring.Manager = this.barManager1;
            this.popupMenuMonitoring.Name = "popupMenuMonitoring";
            // 
            // barButtonItemAddMonitoring
            // 
            this.barButtonItemAddMonitoring.Caption = "모니터링 추가";
            this.barButtonItemAddMonitoring.Id = 28;
            this.barButtonItemAddMonitoring.Name = "barButtonItemAddMonitoring";
            this.barButtonItemAddMonitoring.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemAddMonitoring_ItemClick);
            // 
            // barButtonItemMonitoringVisible
            // 
            this.barButtonItemMonitoringVisible.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.DropDown;
            this.barButtonItemMonitoringVisible.Caption = "모니터링 표시";
            this.barButtonItemMonitoringVisible.DropDownControl = this.popupMenuMonitoringVisible;
            this.barButtonItemMonitoringVisible.Id = 29;
            this.barButtonItemMonitoringVisible.Name = "barButtonItemMonitoringVisible";
            // 
            // popupMenuMonitoringVisible
            // 
            this.popupMenuMonitoringVisible.Manager = this.barManager1;
            this.popupMenuMonitoringVisible.Name = "popupMenuMonitoringVisible";
            // 
            // barButtonItemMonitoringRemove
            // 
            this.barButtonItemMonitoringRemove.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.DropDown;
            this.barButtonItemMonitoringRemove.Caption = "모니터링 제거";
            this.barButtonItemMonitoringRemove.DropDownControl = this.popupMenuMonitoringRemove;
            this.barButtonItemMonitoringRemove.Id = 30;
            this.barButtonItemMonitoringRemove.Name = "barButtonItemMonitoringRemove";
            // 
            // popupMenuMonitoringRemove
            // 
            this.popupMenuMonitoringRemove.Manager = this.barManager1;
            this.popupMenuMonitoringRemove.Name = "popupMenuMonitoringRemove";
            // 
            // barButtonItem3
            // 
            this.barButtonItem3.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.DropDown;
            this.barButtonItem3.Caption = "변수";
            this.barButtonItem3.DropDownControl = this.popupMenuVariables;
            this.barButtonItem3.Id = 31;
            this.barButtonItem3.Name = "barButtonItem3";
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Manager = this.barManager1;
            this.barDockControlTop.Size = new System.Drawing.Size(1364, 54);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 624);
            this.barDockControlBottom.Manager = this.barManager1;
            this.barDockControlBottom.Size = new System.Drawing.Size(1364, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 54);
            this.barDockControlLeft.Manager = this.barManager1;
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 570);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(1364, 54);
            this.barDockControlRight.Manager = this.barManager1;
            this.barDockControlRight.Size = new System.Drawing.Size(0, 570);
            // 
            // barButtonItemNew
            // 
            this.barButtonItemNew.Id = 20;
            this.barButtonItemNew.Name = "barButtonItemNew";
            // 
            // barButtonItemLoad
            // 
            this.barButtonItemLoad.Id = 21;
            this.barButtonItemLoad.Name = "barButtonItemLoad";
            // 
            // barButtonItemSave
            // 
            this.barButtonItemSave.Id = 22;
            this.barButtonItemSave.Name = "barButtonItemSave";
            // 
            // barButtonItemSaveAs
            // 
            this.barButtonItemSaveAs.Id = 23;
            this.barButtonItemSaveAs.Name = "barButtonItemSaveAs";
            // 
            // barButtonItemExitApplication
            // 
            this.barButtonItemExitApplication.Id = 24;
            this.barButtonItemExitApplication.Name = "barButtonItemExitApplication";
            // 
            // barButtonItemVariableSettings
            // 
            this.barButtonItemVariableSettings.Caption = "변수 설정";
            this.barButtonItemVariableSettings.Id = 32;
            this.barButtonItemVariableSettings.Name = "barButtonItemVariableSettings";
            this.barButtonItemVariableSettings.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemVariableSettings_ItemClick);
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // repositoryItemTextEdit2
            // 
            this.repositoryItemTextEdit2.AutoHeight = false;
            this.repositoryItemTextEdit2.Name = "repositoryItemTextEdit2";
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.Cursor = System.Windows.Forms.Cursors.Default;
            this.splitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl1.Horizontal = false;
            this.splitContainerControl1.Location = new System.Drawing.Point(0, 54);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.Controls.Add(this.splitContainerControl2);
            this.splitContainerControl1.Panel1.Text = "Panel1";
            this.splitContainerControl1.Panel2.Controls.Add(this.xtraTabControBottom);
            this.splitContainerControl1.Panel2.Text = "Panel2";
            this.splitContainerControl1.ShowSplitGlyph = DevExpress.Utils.DefaultBoolean.True;
            this.splitContainerControl1.Size = new System.Drawing.Size(1364, 570);
            this.splitContainerControl1.SplitterPosition = 401;
            this.splitContainerControl1.TabIndex = 10;
            // 
            // splitContainerControl2
            // 
            this.splitContainerControl2.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.splitContainerControl2.Appearance.Options.UseBackColor = true;
            this.splitContainerControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl2.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl2.LookAndFeel.UseDefaultLookAndFeel = false;
            this.splitContainerControl2.Name = "splitContainerControl2";
            this.splitContainerControl2.Panel1.Controls.Add(this.xtraTabControlMain);
            this.splitContainerControl2.Panel1.Text = "Panel1";
            this.splitContainerControl2.Panel2.Controls.Add(this.xtraUserControlMain);
            this.splitContainerControl2.Panel2.Text = "Panel2";
            this.splitContainerControl2.ShowSplitGlyph = DevExpress.Utils.DefaultBoolean.True;
            this.splitContainerControl2.Size = new System.Drawing.Size(1364, 401);
            this.splitContainerControl2.SplitterPosition = 282;
            this.splitContainerControl2.TabIndex = 0;
            // 
            // xtraTabControlMain
            // 
            this.xtraTabControlMain.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.xtraTabControlMain.Appearance.Options.UseBackColor = true;
            this.xtraTabControlMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraTabControlMain.HeaderLocation = DevExpress.XtraTab.TabHeaderLocation.Bottom;
            this.xtraTabControlMain.Location = new System.Drawing.Point(0, 0);
            this.xtraTabControlMain.LookAndFeel.SkinName = "DevExpress Dark Style";
            this.xtraTabControlMain.LookAndFeel.UseDefaultLookAndFeel = false;
            this.xtraTabControlMain.Name = "xtraTabControlMain";
            this.xtraTabControlMain.SelectedTabPage = this.xtraTabPageMain;
            this.xtraTabControlMain.Size = new System.Drawing.Size(282, 401);
            this.xtraTabControlMain.TabIndex = 0;
            this.xtraTabControlMain.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPageMain,
            this.xtraTabPageModules});
            // 
            // xtraTabPageMain
            // 
            this.xtraTabPageMain.Controls.Add(this.treeListMain);
            this.xtraTabPageMain.Name = "xtraTabPageMain";
            this.xtraTabPageMain.Size = new System.Drawing.Size(276, 371);
            this.xtraTabPageMain.Text = "메인";
            // 
            // treeListMain
            // 
            this.treeListMain.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.treeListMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treeListMain.Location = new System.Drawing.Point(0, 0);
            this.treeListMain.LookAndFeel.SkinName = "Office 2010 Black";
            this.treeListMain.LookAndFeel.UseDefaultLookAndFeel = false;
            this.treeListMain.Name = "treeListMain";
            this.treeListMain.OptionsBehavior.Editable = false;
            this.treeListMain.Size = new System.Drawing.Size(276, 371);
            this.treeListMain.TabIndex = 0;
            // 
            // xtraTabPageModules
            // 
            this.xtraTabPageModules.Name = "xtraTabPageModules";
            this.xtraTabPageModules.Size = new System.Drawing.Size(276, 371);
            this.xtraTabPageModules.Text = "모듈";
            // 
            // xtraUserControlMain
            // 
            this.xtraUserControlMain.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(80)))), ((int)(((byte)(80)))));
            this.xtraUserControlMain.Appearance.Options.UseForeColor = true;
            this.xtraUserControlMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraUserControlMain.Location = new System.Drawing.Point(0, 0);
            this.xtraUserControlMain.LookAndFeel.UseDefaultLookAndFeel = false;
            this.xtraUserControlMain.Name = "xtraUserControlMain";
            this.xtraUserControlMain.Size = new System.Drawing.Size(1077, 401);
            this.xtraUserControlMain.TabIndex = 0;
            // 
            // xtraTabControBottom
            // 
            this.xtraTabControBottom.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.xtraTabControBottom.Appearance.Options.UseBackColor = true;
            this.xtraTabControBottom.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraTabControBottom.Location = new System.Drawing.Point(0, 0);
            this.xtraTabControBottom.LookAndFeel.SkinName = "DevExpress Dark Style";
            this.xtraTabControBottom.LookAndFeel.UseDefaultLookAndFeel = false;
            this.xtraTabControBottom.Name = "xtraTabControBottom";
            this.xtraTabControBottom.SelectedTabPage = this.xtraTabPageLog;
            this.xtraTabControBottom.Size = new System.Drawing.Size(1364, 159);
            this.xtraTabControBottom.TabIndex = 1;
            this.xtraTabControBottom.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPageLog,
            this.xtraTabPageControlPanel});
            // 
            // xtraTabPageLog
            // 
            this.xtraTabPageLog.Controls.Add(this.gridControl1);
            this.xtraTabPageLog.Name = "xtraTabPageLog";
            this.xtraTabPageLog.Size = new System.Drawing.Size(1358, 129);
            this.xtraTabPageLog.Text = "로그";
            // 
            // gridControl1
            // 
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.Location = new System.Drawing.Point(0, 0);
            this.gridControl1.LookAndFeel.SkinName = "Office 2010 Black";
            this.gridControl1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.MenuManager = this.barManager1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemPictureEdit1,
            this.repositoryItemTimeEdit1});
            this.gridControl1.Size = new System.Drawing.Size(1358, 129);
            this.gridControl1.TabIndex = 0;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.ColType,
            this.ColTime,
            this.ColMessage});
            this.gridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFullFocus;
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsBehavior.Editable = false;
            this.gridView1.OptionsCustomization.AllowColumnMoving = false;
            this.gridView1.OptionsCustomization.AllowSort = false;
            this.gridView1.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.OptionsView.ShowIndicator = false;
            // 
            // ColType
            // 
            this.ColType.Caption = " ";
            this.ColType.ColumnEdit = this.repositoryItemPictureEdit1;
            this.ColType.FieldName = "IconImage";
            this.ColType.Name = "ColType";
            this.ColType.OptionsFilter.AllowAutoFilter = false;
            this.ColType.OptionsFilter.AllowFilter = false;
            this.ColType.Visible = true;
            this.ColType.VisibleIndex = 0;
            this.ColType.Width = 31;
            // 
            // repositoryItemPictureEdit1
            // 
            this.repositoryItemPictureEdit1.Name = "repositoryItemPictureEdit1";
            this.repositoryItemPictureEdit1.SvgImageSize = new System.Drawing.Size(14, 14);
            // 
            // ColTime
            // 
            this.ColTime.Caption = "시간";
            this.ColTime.ColumnEdit = this.repositoryItemTimeEdit1;
            this.ColTime.DisplayFormat.FormatString = "yyyy-MM-dd HH:mm:ss";
            this.ColTime.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.ColTime.FieldName = "Time";
            this.ColTime.Name = "ColTime";
            this.ColTime.OptionsFilter.AllowAutoFilter = false;
            this.ColTime.OptionsFilter.AllowFilter = false;
            this.ColTime.Visible = true;
            this.ColTime.VisibleIndex = 1;
            this.ColTime.Width = 330;
            // 
            // repositoryItemTimeEdit1
            // 
            this.repositoryItemTimeEdit1.AutoHeight = false;
            this.repositoryItemTimeEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemTimeEdit1.Name = "repositoryItemTimeEdit1";
            // 
            // ColMessage
            // 
            this.ColMessage.Caption = "메시지";
            this.ColMessage.FieldName = "Message";
            this.ColMessage.Name = "ColMessage";
            this.ColMessage.OptionsFilter.AllowAutoFilter = false;
            this.ColMessage.OptionsFilter.AllowFilter = false;
            this.ColMessage.Visible = true;
            this.ColMessage.VisibleIndex = 2;
            this.ColMessage.Width = 981;
            // 
            // xtraTabPageControlPanel
            // 
            this.xtraTabPageControlPanel.Controls.Add(this.pnABBDriveControlPad);
            this.xtraTabPageControlPanel.Name = "xtraTabPageControlPanel";
            this.xtraTabPageControlPanel.Size = new System.Drawing.Size(1358, 129);
            this.xtraTabPageControlPanel.Text = "제어 패널";
            // 
            // pnABBDriveControlPad
            // 
            this.pnABBDriveControlPad.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnABBDriveControlPad.Location = new System.Drawing.Point(0, 0);
            this.pnABBDriveControlPad.Name = "pnABBDriveControlPad";
            this.pnABBDriveControlPad.Size = new System.Drawing.Size(1358, 129);
            this.pnABBDriveControlPad.TabIndex = 0;
            // 
            // documentManagerMain
            // 
            this.documentManagerMain.ContainerControl = this.xtraUserControlMain;
            this.documentManagerMain.MenuManager = this.barManager1;
            this.documentManagerMain.View = this.tabbedView1;
            this.documentManagerMain.ViewCollection.AddRange(new DevExpress.XtraBars.Docking2010.Views.BaseView[] {
            this.tabbedView1});
            // 
            // tabbedView1
            // 
            this.tabbedView1.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(230)))));
            this.tabbedView1.Appearance.Options.UseBackColor = true;
            this.tabbedView1.DocumentGroupProperties.ClosePageButtonShowMode = DevExpress.XtraTab.ClosePageButtonShowMode.InTabControlHeader;
            this.tabbedView1.DocumentGroups.AddRange(new DevExpress.XtraBars.Docking2010.Views.Tabbed.DocumentGroup[] {
            this.documentGroup1});
            this.tabbedView1.DocumentProperties.AllowGlyphSkinning = true;
            this.tabbedView1.DocumentProperties.ShowPinButton = false;
            this.tabbedView1.Documents.AddRange(new DevExpress.XtraBars.Docking2010.Views.BaseDocument[] {
            this.documentFixed,
            this.document4});
            dockingContainer1.Element = this.documentGroup1;
            this.tabbedView1.RootContainer.Nodes.AddRange(new DevExpress.XtraBars.Docking2010.Views.Tabbed.DockingContainer[] {
            dockingContainer1});
            this.tabbedView1.DocumentClosed += new DevExpress.XtraBars.Docking2010.Views.DocumentEventHandler(this.tabbedView1_DocumentClosed);
            this.tabbedView1.QueryControl += new DevExpress.XtraBars.Docking2010.Views.QueryControlEventHandler(this.tabbedView1_QueryControl);
            // 
            // tmrUpdateUI
            // 
            this.tmrUpdateUI.Enabled = true;
            this.tmrUpdateUI.Interval = 500;
            this.tmrUpdateUI.Tick += new System.EventHandler(this.tmrUpdateUI_Tick);
            // 
            // svgImageCollection1
            // 
            this.svgImageCollection1.ImageSize = new System.Drawing.Size(13, 13);
            this.svgImageCollection1.Add("actions_checkcircled", "image://svgimages/icon builder/actions_checkcircled.svg");
            this.svgImageCollection1.Add("actions_deletecircled", "image://svgimages/icon builder/actions_deletecircled.svg");
            // 
            // popupMenuVariables
            // 
            this.popupMenuVariables.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItemVariableSettings)});
            this.popupMenuVariables.Manager = this.barManager1;
            this.popupMenuVariables.Name = "popupMenuVariables";
            // 
            // MainView
            // 
            this.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.Appearance.Options.UseForeColor = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1364, 624);
            this.Controls.Add(this.splitContainerControl1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.Name = "MainView";
            this.Text = "ATOM";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MainView_FormClosed);
            this.Load += new System.EventHandler(this.MainForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.documentGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.documentFixed)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.document4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenuSetting)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenuMonitoring)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenuMonitoringVisible)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenuMonitoringRemove)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).EndInit();
            this.splitContainerControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControlMain)).EndInit();
            this.xtraTabControlMain.ResumeLayout(false);
            this.xtraTabPageMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.treeListMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControBottom)).EndInit();
            this.xtraTabControBottom.ResumeLayout(false);
            this.xtraTabPageLog.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPictureEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTimeEdit1)).EndInit();
            this.xtraTabPageControlPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.documentManagerMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.svgImageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenuVariables)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar barQuickButton;
        private DevExpress.XtraBars.BarButtonItem barButtonItemConnect_Quick;
        private DevExpress.XtraBars.Bar barTopStripMenu;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarButtonItem barButtonItemNew;
        private DevExpress.XtraBars.BarButtonItem barButtonItemLoad;
        private DevExpress.XtraBars.BarButtonItem barButtonItemSave;
        private DevExpress.XtraBars.BarButtonItem barButtonItemDisconnect_Quick;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit2;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private DevExpress.XtraTab.XtraTabControl xtraTabControBottom;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageLog;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageControlPanel;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl2;
        private DevExpress.XtraTab.XtraTabControl xtraTabControlMain;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageMain;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageModules;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraBars.BarButtonItem barButtonItemSaveAs;
        private DevExpress.XtraBars.BarButtonItem barButtonItemExitApplication;
        private DevExpress.XtraBars.BarButtonItem barButtonItem1;
        private DevExpress.XtraBars.PopupMenu popupMenuSetting;
        private DevExpress.XtraBars.BarButtonItem barButtonItemCommunicationConfig;
        private DevExpress.XtraEditors.XtraUserControl xtraUserControlMain;
        private DevExpress.XtraBars.Docking2010.Views.Tabbed.TabbedView tabbedView1;
        private DevExpress.XtraBars.Docking2010.Views.Tabbed.DocumentGroup documentGroup1;
        private DevExpress.XtraBars.Docking2010.Views.Tabbed.Document documentFixed;
        private DevExpress.XtraBars.Docking2010.Views.Tabbed.Document document4;
        private DevExpress.XtraGrid.Columns.GridColumn ColType;
        private DevExpress.XtraGrid.Columns.GridColumn ColTime;
        private DevExpress.XtraTreeList.TreeList treeListMain;
        private DevExpress.XtraBars.Docking2010.DocumentManager documentManagerMain;
        private DevExpress.XtraBars.BarButtonItem barButtonItemHWConfig_Quick;
        private DevExpress.XtraBars.BarButtonItem barButtonItemACSMapping;
        private DevExpress.XtraBars.BarButtonItem barButtonItemDataLoggingSetting;
        private DevExpress.XtraBars.BarButtonItem barButtonItem2;
        private DevExpress.XtraBars.PopupMenu popupMenuMonitoring;
        private DevExpress.XtraBars.BarButtonItem barButtonItemAddMonitoring;
        private DevExpress.XtraBars.BarButtonItem barButtonItemMonitoringVisible;
        private DevExpress.XtraBars.BarButtonItem barButtonItemMonitoringRemove;
        private DevExpress.XtraBars.PopupMenu popupMenuMonitoringRemove;
        private DevExpress.XtraBars.PopupMenu popupMenuMonitoringVisible;
        private System.Windows.Forms.Panel pnABBDriveControlPad;
        private System.Windows.Forms.Timer tmrUpdateUI;
        private DevExpress.Utils.SvgImageCollection svgImageCollection1;
        private DevExpress.XtraBars.BarButtonItem barButtonItem3;
        private DevExpress.XtraBars.PopupMenu popupMenuVariables;
        private DevExpress.XtraGrid.Columns.GridColumn ColMessage;
        private DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit repositoryItemPictureEdit1;
        private DevExpress.XtraBars.BarButtonItem barButtonItemVariableSettings;
        private DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit repositoryItemTimeEdit1;
    }
}

