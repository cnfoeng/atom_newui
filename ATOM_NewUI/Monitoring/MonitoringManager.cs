﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using ATOM_Class;

namespace ATOM_NewUI.Monitoring
{
    [Serializable]
    class MonitoringManager
    {
        CommunicationConnector communicationConnector;
        public List<Monitoring> monitorings = new List<Monitoring>();
        public MonitoringManager()
        {
            
        }
        public void ReAssignVariables()
        {
            communicationConnector = CommunicationConnector.GetInstance();

            foreach (Monitoring mon in monitorings)
            {
                foreach(MonitorItem item in mon.monitoringItems)
                {
                    if(communicationConnector.FindVariable(item.si.Name)!=null)
                    {
                        item.si.assignedVar = communicationConnector.FindVariable(item.si.assignedVar.Name);
                    }
                }
            }
        }
        public void AddMonitoring(string monitoringName)
        {
            monitorings.Add(new Monitoring(monitoringName));
        }
        public void RemoveMonitoring(string monitoringName)
        {
            int index = -1;

            for(int i=0; i < monitorings.Count; i++)
            {
                if(monitorings[i].MonitoringName == monitoringName)
                {
                    index = i;
                    break;
                }
            }

            if(index != -1)
            {
                monitorings.RemoveAt(index);
            }
        }
        public MonitoringManager Load(string loadFilePath)
        {
            try
            {
                FileStream fileStreamObject;
                string FileName = loadFilePath;
                fileStreamObject = new FileStream(FileName, FileMode.Open);
                BinaryFormatter binaryFormatter = new BinaryFormatter();
                object dd = binaryFormatter.Deserialize(fileStreamObject);
                fileStreamObject.Close();

                return (MonitoringManager)dd;
            }
            catch (Exception)
            {
                return new MonitoringManager();
            }
        }

        public void Save(string saveFilePath)
        {


            FileStream fileStreamObject;
            string FileName = saveFilePath;
            fileStreamObject = new FileStream(FileName, FileMode.Create);
            BinaryFormatter binaryFormatter = new BinaryFormatter();
            binaryFormatter.Serialize(fileStreamObject, this);
            fileStreamObject.Close();
        }
    }

    [Serializable]
    public class Monitoring
    {
        string monitoringName = string.Empty;
        bool visible = true;
        public List<MonitorItem> monitoringItems = new List<MonitorItem>();

        public Monitoring(string monitoringName)
        {
            this.MonitoringName = monitoringName;
        }

        public bool Visible { get => visible; set => visible = value; }
        public string MonitoringName { get => monitoringName; set => monitoringName = value; }
    }
}
