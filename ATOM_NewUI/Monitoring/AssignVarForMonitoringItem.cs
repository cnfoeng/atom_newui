﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using ATOM_Class;

namespace ATOM_NewUI.Monitoring
{
    public partial class AssignVarForMonitoringItem : DevExpress.XtraEditors.XtraForm
    {
        CommunicationConnector communicationConnector;

        List<VariableInfo> Variables = null;
        MonitorItem searchedMI = null;

        public AssignVarForMonitoringItem(MonitorItem searchedMI)
        {
            InitializeComponent();

            communicationConnector = CommunicationConnector.GetInstance();

            this.searchedMI = searchedMI;
            teAssignedVarName.Text = searchedMI.si.Name;

            cbChannelList.Properties.Items.Clear();
            foreach (CommunicationType variableType in (CommunicationType[])Enum.GetValues(typeof(CommunicationType)))
            {
                cbChannelList.Properties.Items.Add(variableType.ToString());
            }


            if (searchedMI.si.assignedVar == null)
            {
                teAssignedVarName.Text = "Unassigned";

                Variables = communicationConnector.FindCommunication(0).Variables;
                ClearCheck(Variables);
                gridList.DataSource = Variables;
                return;
            }

            CommunicationType commType = communicationConnector.GetCommTypeByVariable(searchedMI.si.assignedVar);

            Variables = communicationConnector.GetVariables(commType);
            cbChannelList.SelectedIndex = (int)commType;

            ClearCheck(Variables);

            CheckAssignedVariable(Variables, searchedMI.si.assignedVar);

            gridList.BeginUpdate();
            gridList.DataSource = Variables;
            gridList.EndUpdate();
        }

        private void CheckAssignedVariable(List<VariableInfo> Variables, VariableInfo AssingedVariable)
        {
            foreach (VariableInfo var in Variables)
            {
                if (var == AssingedVariable)
                    var.Checked = true;
            }

            foreach (VariableInfo var in Variables)
            {
                if (var.Checked)
                {

                }
            }
        }
        private void ClearCheck(List<VariableInfo> Variables)
        {
            foreach (VariableInfo var in Variables)
            {
                var.Checked = false;
            }

            gridList.BeginUpdate();
            gridList.DataSource = Variables;
            gridList.EndUpdate();
        }

        private void repositoryItemCheckEdit1_CheckedChanged(object sender, EventArgs e)
        {
            ClearCheck(Variables);

            Variables[gridView1.FocusedRowHandle].Checked = true;

            searchedMI.si.assignedVar = (VariableInfo)Variables[gridView1.FocusedRowHandle];
            teAssignedVarName.Text = searchedMI.si.assignedVar.Name;

            gridList.BeginUpdate();
            gridList.EndUpdate();
        }

        private void cbChannelList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Variables = communicationConnector.FindCommunication((CommunicationType)cbChannelList.SelectedIndex).Variables;
            }
            catch (NullReferenceException)
            {

                gridList.BeginUpdate();
                gridList.DataSource = null;
                gridList.EndUpdate();
                return;
            }

            ClearCheck(Variables);

            CheckAssignedVariable(Variables, searchedMI.si.assignedVar);

            gridList.BeginUpdate();
            gridList.DataSource = Variables;
            gridList.EndUpdate();
        }
    }
}