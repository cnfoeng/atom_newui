﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ATOM_NewUI.Monitoring.Controls
{
    public partial class Empty : UserControl
    {
        public Empty()
        {
            InitializeComponent();
            this.Dock = DockStyle.Fill;
        }
    }
}
