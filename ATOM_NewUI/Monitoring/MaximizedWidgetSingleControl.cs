﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraBars.Docking2010.Views.Widget;
using ATOM_Class;

namespace ATOM_NewUI.Monitoring
{
    public partial class MaximizedWidgetSingleControl : UserControl
    {
        MonitorItem searchedMI;
        public MaximizedWidgetSingleControl(MonitorItem searchedMI)
        {
            InitializeComponent();

            this.searchedMI = searchedMI;
            UpdateControls();
        }

       

        private void UpdateControls()
        {
            if (this.searchedMI.si.assignedVar == null)
            {
                btnEditVar.Enabled = false;
                btnAppearanceChange.Enabled = false;
            }
            else
            {
                btnEditVar.Enabled = true;
                btnAppearanceChange.Enabled = true;
            }
        }
        private void btnAssignVar_Click(object sender, EventArgs e)
        {
            using (AssignVarForMonitoringItem newForm = new AssignVarForMonitoringItem(searchedMI))
            {
                newForm.ShowDialog();
            }

            UpdateControls();

            
        }

        private void btnAppearanceChange_Click(object sender, EventArgs e)
        {
            using (ChangeAppearance_MonitoringItem newform = new ChangeAppearance_MonitoringItem(searchedMI))
            {
                newform.ShowDialog();
            }

            UpdateControls();
        }

        private void btnEditVar_Click(object sender, EventArgs e)
        {
            CommunicationConnector communicationConnector = CommunicationConnector.GetInstance();
            VariableInfo var = communicationConnector.FindVariable(searchedMI.si.assignedVar.Name);
            CommunicationType commType = communicationConnector.GetCommTypeByVariable(var);
            if (var == null)
            {
                return;
            }
            using (VariableEdit newform = new VariableEdit(var, true, commType))//Edit은 CommType 상관 없긴함.
            {
                newform.ShowDialog();
            }

            UpdateControls();
        }
    }
}
