﻿namespace ATOM_NewUI.SettingForm.ACS
{
    partial class ACSMapping
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ACSMapping));
            this.pnTop = new System.Windows.Forms.Panel();
            this.pnTopRight = new System.Windows.Forms.Panel();
            this.gridControlModuleChannels = new DevExpress.XtraGrid.GridControl();
            this.gridViewModuleChannels = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.ColModuleName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ColModuleChannelNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ColValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ColChannel = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panel2 = new System.Windows.Forms.Panel();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.panel3 = new System.Windows.Forms.Panel();
            this.pnTopLeft = new System.Windows.Forms.Panel();
            this.gridControlACSVar = new DevExpress.XtraGrid.GridControl();
            this.gridViewACSVar = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.ColVariableName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ColAssignedChannel = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panel1 = new System.Windows.Forms.Panel();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.pnBot = new System.Windows.Forms.Panel();
            this.btnClose = new DevExpress.XtraEditors.SimpleButton();
            this.btnAssignNull = new DevExpress.XtraEditors.SimpleButton();
            this.btnAssignAutomatically = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.cbIOType = new DevExpress.XtraEditors.ComboBoxEdit();
            this.behaviorManager1 = new DevExpress.Utils.Behaviors.BehaviorManager(this.components);
            this.dragDropEvents1 = new DevExpress.Utils.DragDrop.DragDropEvents(this.components);
            this.dragDropEvents2 = new DevExpress.Utils.DragDrop.DragDropEvents(this.components);
            this.pnTop.SuspendLayout();
            this.pnTopRight.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlModuleChannels)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewModuleChannels)).BeginInit();
            this.panel2.SuspendLayout();
            this.pnTopLeft.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlACSVar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewACSVar)).BeginInit();
            this.panel1.SuspendLayout();
            this.pnBot.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbIOType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.behaviorManager1)).BeginInit();
            this.SuspendLayout();
            // 
            // pnTop
            // 
            this.pnTop.Controls.Add(this.pnTopRight);
            this.pnTop.Controls.Add(this.panel3);
            this.pnTop.Controls.Add(this.pnTopLeft);
            this.pnTop.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnTop.Location = new System.Drawing.Point(0, 0);
            this.pnTop.Name = "pnTop";
            this.pnTop.Size = new System.Drawing.Size(1292, 510);
            this.pnTop.TabIndex = 0;
            // 
            // pnTopRight
            // 
            this.pnTopRight.Controls.Add(this.gridControlModuleChannels);
            this.pnTopRight.Controls.Add(this.panel2);
            this.pnTopRight.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnTopRight.Location = new System.Drawing.Point(394, 0);
            this.pnTopRight.Name = "pnTopRight";
            this.pnTopRight.Size = new System.Drawing.Size(898, 510);
            this.pnTopRight.TabIndex = 2;
            // 
            // gridControlModuleChannels
            // 
            this.gridControlModuleChannels.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlModuleChannels.Location = new System.Drawing.Point(0, 45);
            this.gridControlModuleChannels.MainView = this.gridViewModuleChannels;
            this.gridControlModuleChannels.Name = "gridControlModuleChannels";
            this.gridControlModuleChannels.Size = new System.Drawing.Size(898, 465);
            this.gridControlModuleChannels.TabIndex = 3;
            this.gridControlModuleChannels.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewModuleChannels});
            // 
            // gridViewModuleChannels
            // 
            this.behaviorManager1.SetBehaviors(this.gridViewModuleChannels, new DevExpress.Utils.Behaviors.Behavior[] {
            ((DevExpress.Utils.Behaviors.Behavior)(DevExpress.Utils.DragDrop.DragDropBehavior.Create(typeof(DevExpress.XtraGrid.Extensions.ColumnViewDragDropSource), true, true, true, true, this.dragDropEvents1)))});
            this.gridViewModuleChannels.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.ColModuleName,
            this.ColModuleChannelNumber,
            this.ColValue,
            this.ColChannel});
            this.gridViewModuleChannels.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFullFocus;
            this.gridViewModuleChannels.GridControl = this.gridControlModuleChannels;
            this.gridViewModuleChannels.Name = "gridViewModuleChannels";
            this.gridViewModuleChannels.OptionsBehavior.Editable = false;
            this.gridViewModuleChannels.OptionsCustomization.AllowColumnMoving = false;
            this.gridViewModuleChannels.OptionsCustomization.AllowSort = false;
            this.gridViewModuleChannels.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridViewModuleChannels.OptionsView.ShowGroupPanel = false;
            this.gridViewModuleChannels.OptionsView.ShowIndicator = false;
            // 
            // ColModuleName
            // 
            this.ColModuleName.Caption = "모듈 이름";
            this.ColModuleName.FieldName = "ModuleName";
            this.ColModuleName.Name = "ColModuleName";
            this.ColModuleName.OptionsFilter.AllowAutoFilter = false;
            this.ColModuleName.OptionsFilter.AllowFilter = false;
            this.ColModuleName.Visible = true;
            this.ColModuleName.VisibleIndex = 1;
            this.ColModuleName.Width = 601;
            // 
            // ColModuleChannelNumber
            // 
            this.ColModuleChannelNumber.Caption = "모듈 채널";
            this.ColModuleChannelNumber.FieldName = "ModuleChannel";
            this.ColModuleChannelNumber.Name = "ColModuleChannelNumber";
            this.ColModuleChannelNumber.OptionsFilter.AllowAutoFilter = false;
            this.ColModuleChannelNumber.OptionsFilter.AllowFilter = false;
            this.ColModuleChannelNumber.Visible = true;
            this.ColModuleChannelNumber.VisibleIndex = 2;
            this.ColModuleChannelNumber.Width = 83;
            // 
            // ColValue
            // 
            this.ColValue.Caption = "값";
            this.ColValue.FieldName = "Value";
            this.ColValue.Name = "ColValue";
            this.ColValue.OptionsFilter.AllowAutoFilter = false;
            this.ColValue.OptionsFilter.AllowFilter = false;
            this.ColValue.Visible = true;
            this.ColValue.VisibleIndex = 3;
            this.ColValue.Width = 113;
            // 
            // ColChannel
            // 
            this.ColChannel.Caption = "채널";
            this.ColChannel.FieldName = "Channel";
            this.ColChannel.Name = "ColChannel";
            this.ColChannel.OptionsFilter.AllowAutoFilter = false;
            this.ColChannel.OptionsFilter.AllowFilter = false;
            this.ColChannel.Visible = true;
            this.ColChannel.VisibleIndex = 0;
            this.ColChannel.Width = 99;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.labelControl3);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(898, 45);
            this.panel2.TabIndex = 2;
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl3.Appearance.Options.UseFont = true;
            this.labelControl3.Location = new System.Drawing.Point(8, 14);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(119, 15);
            this.labelControl3.TabIndex = 1;
            this.labelControl3.Text = "ACS Module Channels";
            // 
            // panel3
            // 
            this.panel3.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel3.Location = new System.Drawing.Point(384, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(10, 510);
            this.panel3.TabIndex = 4;
            // 
            // pnTopLeft
            // 
            this.pnTopLeft.Controls.Add(this.gridControlACSVar);
            this.pnTopLeft.Controls.Add(this.panel1);
            this.pnTopLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnTopLeft.Location = new System.Drawing.Point(0, 0);
            this.pnTopLeft.Name = "pnTopLeft";
            this.pnTopLeft.Size = new System.Drawing.Size(384, 510);
            this.pnTopLeft.TabIndex = 3;
            // 
            // gridControlACSVar
            // 
            this.gridControlACSVar.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlACSVar.Location = new System.Drawing.Point(0, 45);
            this.gridControlACSVar.MainView = this.gridViewACSVar;
            this.gridControlACSVar.Name = "gridControlACSVar";
            this.gridControlACSVar.Size = new System.Drawing.Size(384, 465);
            this.gridControlACSVar.TabIndex = 2;
            this.gridControlACSVar.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewACSVar});
            // 
            // gridViewACSVar
            // 
            this.behaviorManager1.SetBehaviors(this.gridViewACSVar, new DevExpress.Utils.Behaviors.Behavior[] {
            ((DevExpress.Utils.Behaviors.Behavior)(DevExpress.Utils.DragDrop.DragDropBehavior.Create(typeof(DevExpress.XtraGrid.Extensions.ColumnViewDragDropSource), true, true, true, true, this.dragDropEvents2)))});
            this.gridViewACSVar.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.ColVariableName,
            this.ColAssignedChannel});
            this.gridViewACSVar.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFullFocus;
            this.gridViewACSVar.GridControl = this.gridControlACSVar;
            this.gridViewACSVar.Name = "gridViewACSVar";
            this.gridViewACSVar.OptionsBehavior.Editable = false;
            this.gridViewACSVar.OptionsCustomization.AllowColumnMoving = false;
            this.gridViewACSVar.OptionsCustomization.AllowSort = false;
            this.gridViewACSVar.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridViewACSVar.OptionsView.ShowGroupPanel = false;
            this.gridViewACSVar.OptionsView.ShowIndicator = false;
            this.gridViewACSVar.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.gridViewACSVar_PopupMenuShowing);
            // 
            // ColVariableName
            // 
            this.ColVariableName.Caption = "변수 이름";
            this.ColVariableName.FieldName = "VariableName";
            this.ColVariableName.Name = "ColVariableName";
            this.ColVariableName.OptionsFilter.AllowAutoFilter = false;
            this.ColVariableName.OptionsFilter.AllowFilter = false;
            this.ColVariableName.Visible = true;
            this.ColVariableName.VisibleIndex = 0;
            this.ColVariableName.Width = 132;
            // 
            // ColAssignedChannel
            // 
            this.ColAssignedChannel.Caption = "맵핑 채널";
            this.ColAssignedChannel.FieldName = "AssignedChannel";
            this.ColAssignedChannel.Name = "ColAssignedChannel";
            this.ColAssignedChannel.OptionsFilter.AllowAutoFilter = false;
            this.ColAssignedChannel.OptionsFilter.AllowFilter = false;
            this.ColAssignedChannel.Visible = true;
            this.ColAssignedChannel.VisibleIndex = 1;
            this.ColAssignedChannel.Width = 250;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.labelControl2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(384, 45);
            this.panel1.TabIndex = 1;
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl2.Appearance.Options.UseFont = true;
            this.labelControl2.Location = new System.Drawing.Point(8, 14);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(75, 15);
            this.labelControl2.TabIndex = 0;
            this.labelControl2.Text = "ACS Variables";
            // 
            // pnBot
            // 
            this.pnBot.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnBot.Controls.Add(this.btnClose);
            this.pnBot.Controls.Add(this.btnAssignNull);
            this.pnBot.Controls.Add(this.btnAssignAutomatically);
            this.pnBot.Controls.Add(this.labelControl1);
            this.pnBot.Controls.Add(this.cbIOType);
            this.pnBot.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnBot.Location = new System.Drawing.Point(0, 510);
            this.pnBot.Name = "pnBot";
            this.pnBot.Size = new System.Drawing.Size(1292, 59);
            this.pnBot.TabIndex = 1;
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.Location = new System.Drawing.Point(1147, 13);
            this.btnClose.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(132, 33);
            this.btnClose.TabIndex = 4;
            this.btnClose.Text = "닫기";
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnAssignNull
            // 
            this.btnAssignNull.Location = new System.Drawing.Point(426, 13);
            this.btnAssignNull.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btnAssignNull.Name = "btnAssignNull";
            this.btnAssignNull.Size = new System.Drawing.Size(132, 33);
            this.btnAssignNull.TabIndex = 3;
            this.btnAssignNull.Text = "전 변수 맵핑 제거";
            this.btnAssignNull.Click += new System.EventHandler(this.btnAssignNull_Click);
            // 
            // btnAssignAutomatically
            // 
            this.btnAssignAutomatically.Location = new System.Drawing.Point(263, 13);
            this.btnAssignAutomatically.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btnAssignAutomatically.Name = "btnAssignAutomatically";
            this.btnAssignAutomatically.Size = new System.Drawing.Size(132, 33);
            this.btnAssignAutomatically.TabIndex = 2;
            this.btnAssignAutomatically.Text = "전 변수 자동 순차 맵핑";
            this.btnAssignAutomatically.Click += new System.EventHandler(this.btnAssignAutomatically_Click);
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(16, 21);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(46, 15);
            this.labelControl1.TabIndex = 1;
            this.labelControl1.Text = "IO Type :";
            // 
            // cbIOType
            // 
            this.cbIOType.Location = new System.Drawing.Point(76, 18);
            this.cbIOType.Name = "cbIOType";
            this.cbIOType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbIOType.Properties.Items.AddRange(new object[] {
            "AI",
            "AO",
            "DI",
            "DO"});
            this.cbIOType.Size = new System.Drawing.Size(152, 22);
            this.cbIOType.TabIndex = 0;
            this.cbIOType.SelectedIndexChanged += new System.EventHandler(this.cbIOType_SelectedIndexChanged);
            // 
            // dragDropEvents2
            // 
            this.dragDropEvents2.DragOver += new DevExpress.Utils.DragDrop.DragOverEventHandler(this.dragDropEvents2_DragOver);
            this.dragDropEvents2.DragDrop += new DevExpress.Utils.DragDrop.DragDropEventHandler(this.dragDropEvents2_DragDrop);
            // 
            // ACSMapping
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1292, 569);
            this.Controls.Add(this.pnTop);
            this.Controls.Add(this.pnBot);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ACSMapping";
            this.Text = "ACS Mapping";
            this.pnTop.ResumeLayout(false);
            this.pnTopRight.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlModuleChannels)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewModuleChannels)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.pnTopLeft.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlACSVar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewACSVar)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.pnBot.ResumeLayout(false);
            this.pnBot.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbIOType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.behaviorManager1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnTop;
        private System.Windows.Forms.Panel pnBot;
        private DevExpress.XtraEditors.ComboBoxEdit cbIOType;
        private DevExpress.XtraEditors.SimpleButton btnClose;
        private DevExpress.XtraEditors.SimpleButton btnAssignNull;
        private DevExpress.XtraEditors.SimpleButton btnAssignAutomatically;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private System.Windows.Forms.Panel pnTopRight;
        private DevExpress.XtraGrid.GridControl gridControlModuleChannels;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewModuleChannels;
        private DevExpress.XtraGrid.Columns.GridColumn ColModuleName;
        private System.Windows.Forms.Panel panel2;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel pnTopLeft;
        private DevExpress.XtraGrid.GridControl gridControlACSVar;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewACSVar;
        private DevExpress.XtraGrid.Columns.GridColumn ColVariableName;
        private DevExpress.XtraGrid.Columns.GridColumn ColAssignedChannel;
        private System.Windows.Forms.Panel panel1;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraGrid.Columns.GridColumn ColModuleChannelNumber;
        private DevExpress.XtraGrid.Columns.GridColumn ColValue;
        private DevExpress.XtraGrid.Columns.GridColumn ColChannel;
        private DevExpress.Utils.Behaviors.BehaviorManager behaviorManager1;
        private DevExpress.Utils.DragDrop.DragDropEvents dragDropEvents1;
        private DevExpress.Utils.DragDrop.DragDropEvents dragDropEvents2;
    }
}