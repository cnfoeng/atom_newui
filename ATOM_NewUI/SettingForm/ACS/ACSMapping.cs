﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using ATOM_Class;
namespace ATOM_NewUI.SettingForm.ACS
{
    public partial class ACSMapping : DevExpress.XtraEditors.XtraForm
    {
        CommunicationConnector communicationConnector;
        ACSConnector acsConnector;

        public ACSMapping()
        {
            InitializeComponent();
            communicationConnector = CommunicationConnector.GetInstance();
            try
            {
                ICommunication com = communicationConnector.FindCommunication(CommunicationType.ACS);
                acsConnector = (ACSConnector)com.ReturnBaseObject();
                cbIOType.SelectedIndex = 0;

            }
            catch (NullReferenceException)
            {
                XtraMessageBox.Show("<size=14>ACS 통신 연결 설정 또는 연결 상태 확인</size>", "<size=14>에러</size>", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            dragDropEvents1.DragDrop += DragDropEvents1_DragDrop;
            dragDropEvents1.DragOver += DragDropEvents1_DragOver;
        }

        private void DragDropEvents1_DragOver(object sender, DevExpress.Utils.DragDrop.DragOverEventArgs e)
        {
            DragOverGridEventArgs args = DragOverGridEventArgs.GetDragOverGridEventArgs(e);
            e.InsertType = args.InsertType;
            e.InsertIndicatorLocation = args.InsertIndicatorLocation;
            e.Action = args.Action;
            args.Cursor = Cursors.Arrow;
            Cursor.Current = args.Cursor;
            args.Handled = true;

        }

        private void DragDropEvents1_DragDrop(object sender, DevExpress.Utils.DragDrop.DragDropEventArgs e)
        {
            e.Action = DevExpress.Utils.DragDrop.DragDropActions.None;
        }
        private void dragDropEvents2_DragOver(object sender, DevExpress.Utils.DragDrop.DragOverEventArgs e)
        {
            DragOverGridEventArgs args = DragOverGridEventArgs.GetDragOverGridEventArgs(e);
            e.InsertType = args.InsertType;
            e.InsertIndicatorLocation = args.InsertIndicatorLocation;
            e.Action = args.Action;
            args.Cursor = Cursors.Arrow;
            Cursor.Current = args.Cursor;
            args.Handled = true;

        }

        private void dragDropEvents2_DragDrop(object sender, DevExpress.Utils.DragDrop.DragDropEventArgs e)
        {
            e.Action = DevExpress.Utils.DragDrop.DragDropActions.None;

            GridView targetView = (GridView)e.Target;
            Point point = targetView.GridControl.PointToClient(e.Location);
            GridHitInfo hitInfo = targetView.CalcHitInfo(point);
            int targetRowHandle = hitInfo.RowHandle;


            IOType type = (IOType)cbIOType.SelectedIndex;

            DataTable dt = acsConnector.MappingList.ReturnDataTableByType(type);
            string varName = dt.Rows[targetRowHandle][0].ToString();

            acsConnector.MappingList.AssignVariable(varName, gridViewModuleChannels.FocusedRowHandle, type);
            acsConnector.VariableModuleIOMapping();



            //communicationConnector.FindVariable(varName).CalibrationTable[0].IOValue =
            //acsConnector.Modules[gridViewModuleChannels.FocusedRowHandle].ModuleIOMin;
            //communicationConnector.FindVariable(varName).CalibrationTable[1].IOValue =
            //acsConnector.Modules[gridViewModuleChannels.FocusedRowHandle].ModuleIOMax;

            gridControlACSVar.DataSource = null;
            gridControlACSVar.DataSource = acsConnector.MappingList.ReturnDataTableByType(type);
        }
        private void cbIOType_SelectedIndexChanged(object sender, EventArgs e)
        {
            IOType type = (IOType)cbIOType.SelectedIndex;
            gridControlACSVar.DataSource = null;
            gridControlACSVar.DataSource = acsConnector.MappingList.ReturnDataTableByType(type);
            gridControlModuleChannels.DataSource = null;
            gridControlModuleChannels.DataSource = acsConnector.Modules.ReturnDataTableByType(type, acsConnector.ReturnRealValueArray(type));
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnAssignAutomatically_Click(object sender, EventArgs e)
        {
            acsConnector.MappingList.AssignAllMappingsSequentially();
            acsConnector.VariableModuleIOMapping();

            IOType type = (IOType)cbIOType.SelectedIndex;
            gridControlACSVar.DataSource = null;
            gridControlACSVar.DataSource = acsConnector.MappingList.ReturnDataTableByType(type);
        }

        private void btnAssignNull_Click(object sender, EventArgs e)
        {
            acsConnector.MappingList.EmptyAllMappings();

            IOType type = (IOType)cbIOType.SelectedIndex;
            gridControlACSVar.DataSource = null;
            gridControlACSVar.DataSource = acsConnector.MappingList.ReturnDataTableByType(type);
        }

        private void gridViewACSVar_PopupMenuShowing(object sender, PopupMenuShowingEventArgs e)
        {
            if (e.MenuType == GridMenuType.Row)
            {
                ContextMenu cm = new ContextMenu();
                cm.MenuItems.Add("할당 제거", new EventHandler(cm_Click));
                if (e.HitInfo.InRow)
                {
                    gridViewACSVar.FocusedRowHandle = e.HitInfo.RowHandle;
                    cm.Show(gridViewACSVar.GridControl, e.Point);
                }


            }
        }

        private void cm_Click(object sender, EventArgs e)
        {
            IOType type = (IOType)cbIOType.SelectedIndex;

            DataTable dt = acsConnector.MappingList.ReturnDataTableByType(type);
            string varName = dt.Rows[gridViewACSVar.FocusedRowHandle][0].ToString();

            acsConnector.MappingList.AssignVariable(varName, -1, type);

            gridControlACSVar.DataSource = null;
            gridControlACSVar.DataSource = acsConnector.MappingList.ReturnDataTableByType(type);
        }
    }

}