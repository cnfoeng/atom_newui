﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using DevExpress.XtraEditors;
using ATOM_Class;

namespace ATOM_NewUI.Settings.ACS
{
    public partial class AddACS : DevExpress.XtraEditors.XtraForm
    {
        CommunicationConnector communicationConnector;
        ICommunication communication;

        bool IsEdit = false;

        public AddACS(bool isEdit)
        {
            InitializeComponent();
            communicationConnector = CommunicationConnector.GetInstance();

            tbIP.Text = "10.0.0.100";
            tbIP.ReadOnly = true;

            IsEdit = isEdit;

            if (isEdit)
            {
                communication = communicationConnector.FindCommunication(CommunicationType.ACS);
                tbIP.Text = communication.ConnectionString;
                cbSimulator.Checked = communication.IsSimulator;
                this.Text = "Edit ACS";
                cbUseHMI.Checked = (communication.Remark == "WithHMI") ? true : false;
            }
            else
            {
                communication = new ACSConnector();
            }

        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            communication.ConnectionString = tbIP.Text;
            communication.IsSimulator = cbSimulator.Checked;
            communication.Remark = cbUseHMI.Checked ? "WithHMI" : "WithoutHMI";
            if (IsEdit == false)
            {
                communicationConnector.AddCommunication(communication);
            }
            this.Close();
        }

        private void AddACS_FormClosed(object sender, FormClosedEventArgs e)
        {

        }

        private void btnImportVariableFile_Click(object sender, EventArgs e)
        {
            xtraOpenFileDialog1.FileName = "";
            xtraOpenFileDialog1.Filter = "csv File Format|*.csv";
            xtraOpenFileDialog1.Title = "Load ACS Variable File";

            xtraOpenFileDialog1.ShowDialog();


            string filePath = xtraOpenFileDialog1.FileName;
            tbVariableFilePath.Text = filePath;


            communication.VariableFilePath = filePath;

            if (communication.MakeVariableList() == false)
            {
                XtraMessageBox.Show("<size=12>변수 파일을 불러오는 중 문제가 발생했습니다.</size>", "<size=12>에러</size>", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            
        }

        private void btnExportVariableFile_Click(object sender, EventArgs e)
        {
            xtraSaveFileDialog1.DefaultExt = "csv";
            xtraSaveFileDialog1.Filter = "csv File Format|*.csv";
            xtraSaveFileDialog1.Title = "Save ACS Variable File";
            xtraSaveFileDialog1.FileName = "ACSVariableFile_" + DateTime.Now.ToString("yyyyMMddHHmm");
            xtraSaveFileDialog1.ShowDialog();

            if(xtraSaveFileDialog1.FileName != "")
            {
                string fileName = xtraSaveFileDialog1.FileName;
                communication.ExportVariableFile(fileName);
            }
        }
        
    }
}