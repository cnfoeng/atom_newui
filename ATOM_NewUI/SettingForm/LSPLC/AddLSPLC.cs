﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using DevExpress.XtraEditors;
using ATOM_Class;

namespace ATOM_NewUI.Settings.LSPLC
{
    public partial class AddLSPLC : DevExpress.XtraEditors.XtraForm
    {
        CommunicationConnector communicationConnector;
        ICommunication communication;

        string plcName = string.Empty;
        bool IsEdit = false;

        public AddLSPLC(bool isEdit, int plcNumber)
        {
            InitializeComponent();
            communicationConnector = CommunicationConnector.GetInstance();

            
            tbIP.ReadOnly = true;

            IsEdit = isEdit;
            if (plcNumber == 1)
            {
                tbIP.Text = "192.168.0.100";
                this.plcName = "WTC";
            }
            else if(plcNumber == 2)
            {
                tbIP.Text = "192.168.0.120";
                this.plcName = "FTC";
            }
            else if(plcNumber == 3)
            {
                tbIP.Text = "192.168.0.101";
                this.plcName = "OTC";
            }

            if (isEdit)
            {
                communication = communicationConnector.FindCommunication(this.communication.CommType);
                tbIP.Text = communication.ConnectionString;
                this.Text = "Edit "+ plcName;
            }
            else
            {
                this.Text = "Add " + plcName;
                communication = new ATOM_Class.LSPLC.LS_FEnetClass(plcNumber);
            }

        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            communication.ConnectionString = tbIP.Text;

            if (IsEdit == false)
            {
                communicationConnector.AddCommunication(communication);
            }
            this.Close();
        }

        private void btnImportVariableFile_Click(object sender, EventArgs e)
        {
            xtraOpenFileDialog1.FileName = "";
            xtraOpenFileDialog1.Filter = "csv File Format|*.csv";
            xtraOpenFileDialog1.Title = "Load "+ plcName + " Variable File";

            xtraOpenFileDialog1.ShowDialog();


            string filePath = xtraOpenFileDialog1.FileName;
            tbVariableFilePath.Text = filePath;


            communication.VariableFilePath = filePath;

            if (communication.MakeVariableList() == false)
            {
                XtraMessageBox.Show("<size=12>변수 파일을 불러오는 중 문제가 발생했습니다.</size>", "<size=12>에러</size>", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            
        }

        private void btnExportVariableFile_Click(object sender, EventArgs e)
        {
            xtraSaveFileDialog1.DefaultExt = "csv";
            xtraSaveFileDialog1.Filter = "csv File Format|*.csv";
            xtraSaveFileDialog1.Title = "Save "+plcName+" Variable File";
            xtraSaveFileDialog1.FileName = plcName+"VariableFile_" + DateTime.Now.ToString("yyyyMMddHHmm");
            xtraSaveFileDialog1.ShowDialog();

            if(xtraSaveFileDialog1.FileName != "")
            {
                string fileName = xtraSaveFileDialog1.FileName;
                communication.ExportVariableFile(fileName);
            }
        }
        
    }
}