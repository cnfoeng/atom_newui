﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using ATOM_Class;

namespace ATOM_NewUI.Settings
{
    public partial class CommunicationConfig : DevExpress.XtraEditors.XtraForm
    {
        #region Member
        CommunicationConnector communicationConnector;
        #endregion
        #region Functions

        public CommunicationConfig()
        {
            InitializeComponent();
            communicationConnector = CommunicationConnector.GetInstance();

            MakeGridview();
        }

        private void MakeGridview()
        {
            gridControl1.DataSource = communicationConnector.GetCommunications();
        }
        #endregion

        #region Events
        private void btnAdd_Click(object sender, EventArgs e)
        {
            SelectCommType newform = new SelectCommType();
            newform.ShowDialog();

            gridControl1.BeginUpdate();
            gridControl1.EndUpdate();
        }
        private void btnRemove_Click(object sender, EventArgs e)
        {
            if (gridView1.FocusedRowHandle < 0)
            {
                return;
            }

            CommunicationType type = (CommunicationType)gridView1.FocusedValue;
            communicationConnector.RemoveCommunication(type);

            gridControl1.BeginUpdate();
            gridControl1.EndUpdate();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            //acs말고 abbdrive는?
            if (gridView1.FocusedRowHandle < 0)
            {
                return;
            }

            bool isEdit = true;
            ACS.AddACS newform = new ACS.AddACS(isEdit);
            newform.ShowDialog();

            gridControl1.BeginUpdate();
            gridControl1.EndUpdate();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion


    }
}