﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using ATOM_Class;

namespace ATOM_NewUI.Settings
{
    public partial class SelectCommType : DevExpress.XtraEditors.XtraForm
    {
        CommunicationType selectedCommType;
        CommunicationConnector communicationConnector;

        public SelectCommType()
        {
            InitializeComponent();
            communicationConnector = CommunicationConnector.GetInstance();

            EnumCommunicationTypeToComboEditItem();
        }
        private void EnumCommunicationTypeToComboEditItem()
        {
            foreach (CommunicationType commType in (CommunicationType[])Enum.GetValues(typeof(CommunicationType)))
            {
                comboBoxEdit1.Properties.Items.Add(commType.ToString());
            }

            if (comboBoxEdit1.Properties.Items.Count > 0)
                comboBoxEdit1.SelectedIndex = 0;
        }
        private void btnAdd_Click(object sender, EventArgs e)
        {
            var communications = communicationConnector.GetCommunications();

            foreach (ICommunication commConfig in communications)
            {
                if (commConfig.CommType == selectedCommType)
                {
                    XtraMessageBox.Show("<size=12>이미 같은 통신 타입이 존재합니다.</size>", "<size=12>에러</size>", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
            }

            switch (selectedCommType)
            {

                case CommunicationType.ACS:
                    ACS.AddACS addACS = new ACS.AddACS(false);
                    addACS.ShowDialog();
                    break;
                case CommunicationType.CAN:
                    break;
                case CommunicationType.ABBDrive:
                    ABBDrive.AddABBDrive addABBDrive = new ABBDrive.AddABBDrive(false);
                    addABBDrive.ShowDialog();
                    break;
                case CommunicationType.WTC:
                    LSPLC.AddLSPLC AddLSPLC1 = new LSPLC.AddLSPLC(false, 1);
                    AddLSPLC1.ShowDialog();
                    break;
                case CommunicationType.FTC:
                    LSPLC.AddLSPLC AddLSPLC2 = new LSPLC.AddLSPLC(false, 2);
                    AddLSPLC2.ShowDialog();
                    break;
                case CommunicationType.OTC:
                    LSPLC.AddLSPLC AddLSPLC3 = new LSPLC.AddLSPLC(false,3);
                    AddLSPLC3.ShowDialog();
                    break;


            }

            this.Close();
        }


        private void comboBoxEdit1_SelectedIndexChanged(object sender, EventArgs e)
        {
            selectedCommType = (CommunicationType)comboBoxEdit1.SelectedIndex;
        }
    }
}