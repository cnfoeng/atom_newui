﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using ATOM_Class;

namespace ATOM_NewUI.Settings.ABBDrive
{
    public partial class AddABBDrive : DevExpress.XtraEditors.XtraForm
    {
        CommunicationConnector communicationConnector;
        ICommunication communication;

        bool IsEdit = false;

        public AddABBDrive(bool isEdit)
        {
            InitializeComponent();

            communicationConnector = CommunicationConnector.GetInstance();

            tbIP.Text = "192.168.0.10";
            tbIP.ReadOnly = true;

            IsEdit = isEdit;

            if (isEdit)
            {
                communication = communicationConnector.FindCommunication(CommunicationType.ACS);
                tbIP.Text = communication.ConnectionString;
                this.Text = "Edit ABB Drive";
            }
            else
            {
                communication = new ABBDriverClass();
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            communication.ConnectionString = tbIP.Text;
            if (IsEdit == false)
            {
                communication.MakeVariableList();
                communicationConnector.AddCommunication(communication);
            }
            this.Close();
        }
    }
}