﻿namespace ATOM_NewUI
{
    partial class ABBDriveControlPad
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraGauges.Core.Model.IndicatorState indicatorState1 = new DevExpress.XtraGauges.Core.Model.IndicatorState();
            DevExpress.XtraGauges.Core.Model.IndicatorState indicatorState2 = new DevExpress.XtraGauges.Core.Model.IndicatorState();
            DevExpress.XtraGauges.Core.Model.IndicatorState indicatorState3 = new DevExpress.XtraGauges.Core.Model.IndicatorState();
            DevExpress.XtraGauges.Core.Model.IndicatorState indicatorState4 = new DevExpress.XtraGauges.Core.Model.IndicatorState();
            this.panel1 = new System.Windows.Forms.Panel();
            this.gaugeControl1 = new DevExpress.XtraGauges.Win.GaugeControl();
            this.stateIndicatorGauge1 = new DevExpress.XtraGauges.Win.Gauges.State.StateIndicatorGauge();
            this.stateIndicatorComponent1 = new DevExpress.XtraGauges.Win.Gauges.State.StateIndicatorComponent();
            this.btnDisconnect = new DevExpress.XtraEditors.SimpleButton();
            this.btnConnect = new DevExpress.XtraEditors.SimpleButton();
            this.lbConnection = new System.Windows.Forms.Label();
            this.btnReset = new DevExpress.XtraEditors.SimpleButton();
            this.lbError = new System.Windows.Forms.Label();
            this.lbTorqueFeedback = new System.Windows.Forms.Label();
            this.lbSpeedFeedback = new System.Windows.Forms.Label();
            this.lbModeFeedback = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btnSetTorque = new DevExpress.XtraEditors.SimpleButton();
            this.btnSpeedMode = new DevExpress.XtraEditors.SimpleButton();
            this.tbSpeedReference = new DevExpress.XtraEditors.TextEdit();
            this.btnSetSpeed = new DevExpress.XtraEditors.SimpleButton();
            this.label1 = new System.Windows.Forms.Label();
            this.btnOff = new DevExpress.XtraEditors.SimpleButton();
            this.btnOn = new DevExpress.XtraEditors.SimpleButton();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.stateIndicatorGauge1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.stateIndicatorComponent1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbSpeedReference.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.gaugeControl1);
            this.panel1.Controls.Add(this.btnDisconnect);
            this.panel1.Controls.Add(this.btnConnect);
            this.panel1.Controls.Add(this.lbConnection);
            this.panel1.Controls.Add(this.btnReset);
            this.panel1.Controls.Add(this.lbError);
            this.panel1.Controls.Add(this.lbTorqueFeedback);
            this.panel1.Controls.Add(this.lbSpeedFeedback);
            this.panel1.Controls.Add(this.lbModeFeedback);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.btnSetTorque);
            this.panel1.Controls.Add(this.btnSpeedMode);
            this.panel1.Controls.Add(this.tbSpeedReference);
            this.panel1.Controls.Add(this.btnSetSpeed);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.btnOff);
            this.panel1.Controls.Add(this.btnOn);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(661, 268);
            this.panel1.TabIndex = 0;
            // 
            // gaugeControl1
            // 
            this.gaugeControl1.BackColor = System.Drawing.Color.Transparent;
            this.gaugeControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.gaugeControl1.Gauges.AddRange(new DevExpress.XtraGauges.Base.IGauge[] {
            this.stateIndicatorGauge1});
            this.gaugeControl1.Location = new System.Drawing.Point(376, 31);
            this.gaugeControl1.Name = "gaugeControl1";
            this.gaugeControl1.Size = new System.Drawing.Size(33, 39);
            this.gaugeControl1.TabIndex = 35;
            // 
            // stateIndicatorGauge1
            // 
            this.stateIndicatorGauge1.Bounds = new System.Drawing.Rectangle(6, 6, 21, 27);
            this.stateIndicatorGauge1.Indicators.AddRange(new DevExpress.XtraGauges.Win.Gauges.State.StateIndicatorComponent[] {
            this.stateIndicatorComponent1});
            this.stateIndicatorGauge1.Name = "stateIndicatorGauge1";
            // 
            // stateIndicatorComponent1
            // 
            this.stateIndicatorComponent1.Center = new DevExpress.XtraGauges.Core.Base.PointF2D(124F, 124F);
            this.stateIndicatorComponent1.Name = "stateIndicatorComponent1";
            this.stateIndicatorComponent1.Size = new System.Drawing.SizeF(200F, 200F);
            this.stateIndicatorComponent1.StateIndex = 0;
            indicatorState1.Name = "State1";
            indicatorState1.ShapeType = DevExpress.XtraGauges.Core.Model.StateIndicatorShapeType.ElectricLight1;
            indicatorState2.Name = "State2";
            indicatorState2.ShapeType = DevExpress.XtraGauges.Core.Model.StateIndicatorShapeType.ElectricLight2;
            indicatorState3.Name = "State3";
            indicatorState3.ShapeType = DevExpress.XtraGauges.Core.Model.StateIndicatorShapeType.ElectricLight3;
            indicatorState4.Name = "State4";
            indicatorState4.ShapeType = DevExpress.XtraGauges.Core.Model.StateIndicatorShapeType.ElectricLight4;
            this.stateIndicatorComponent1.States.AddRange(new DevExpress.XtraGauges.Core.Model.IIndicatorState[] {
            indicatorState1,
            indicatorState2,
            indicatorState3,
            indicatorState4});
            // 
            // btnDisconnect
            // 
            this.btnDisconnect.Location = new System.Drawing.Point(510, 34);
            this.btnDisconnect.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btnDisconnect.Name = "btnDisconnect";
            this.btnDisconnect.Size = new System.Drawing.Size(72, 32);
            this.btnDisconnect.TabIndex = 34;
            this.btnDisconnect.Text = "Disconnect";
            this.btnDisconnect.Visible = false;
            // 
            // btnConnect
            // 
            this.btnConnect.Location = new System.Drawing.Point(428, 34);
            this.btnConnect.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btnConnect.Name = "btnConnect";
            this.btnConnect.Size = new System.Drawing.Size(72, 32);
            this.btnConnect.TabIndex = 33;
            this.btnConnect.Text = "Connect";
            this.btnConnect.Visible = false;
            // 
            // lbConnection
            // 
            this.lbConnection.AutoSize = true;
            this.lbConnection.Location = new System.Drawing.Point(304, 44);
            this.lbConnection.Name = "lbConnection";
            this.lbConnection.Size = new System.Drawing.Size(78, 15);
            this.lbConnection.TabIndex = 32;
            this.lbConnection.Text = "Connection : ";
            // 
            // btnReset
            // 
            this.btnReset.Location = new System.Drawing.Point(527, 84);
            this.btnReset.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(55, 59);
            this.btnReset.TabIndex = 31;
            this.btnReset.Text = "Reset";
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
            // 
            // lbError
            // 
            this.lbError.AutoSize = true;
            this.lbError.Location = new System.Drawing.Point(307, 123);
            this.lbError.Name = "lbError";
            this.lbError.Size = new System.Drawing.Size(41, 15);
            this.lbError.TabIndex = 30;
            this.lbError.Text = "Error : ";
            // 
            // lbTorqueFeedback
            // 
            this.lbTorqueFeedback.AutoSize = true;
            this.lbTorqueFeedback.Location = new System.Drawing.Point(411, 123);
            this.lbTorqueFeedback.Name = "lbTorqueFeedback";
            this.lbTorqueFeedback.Size = new System.Drawing.Size(49, 15);
            this.lbTorqueFeedback.TabIndex = 29;
            this.lbTorqueFeedback.Text = "Torque :";
            // 
            // lbSpeedFeedback
            // 
            this.lbSpeedFeedback.AutoSize = true;
            this.lbSpeedFeedback.Location = new System.Drawing.Point(411, 92);
            this.lbSpeedFeedback.Name = "lbSpeedFeedback";
            this.lbSpeedFeedback.Size = new System.Drawing.Size(45, 15);
            this.lbSpeedFeedback.TabIndex = 28;
            this.lbSpeedFeedback.Text = "Speed :";
            // 
            // lbModeFeedback
            // 
            this.lbModeFeedback.AutoSize = true;
            this.lbModeFeedback.Location = new System.Drawing.Point(307, 92);
            this.lbModeFeedback.Name = "lbModeFeedback";
            this.lbModeFeedback.Size = new System.Drawing.Size(47, 15);
            this.lbModeFeedback.TabIndex = 27;
            this.lbModeFeedback.Text = "Mode : ";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label3.Location = new System.Drawing.Point(22, 161);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(65, 14);
            this.label3.TabIndex = 26;
            this.label3.Text = "Set Value";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label2.Location = new System.Drawing.Point(22, 86);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 14);
            this.label2.TabIndex = 25;
            this.label2.Text = "Mode";
            // 
            // btnSetTorque
            // 
            this.btnSetTorque.Location = new System.Drawing.Point(145, 109);
            this.btnSetTorque.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btnSetTorque.Name = "btnSetTorque";
            this.btnSetTorque.Size = new System.Drawing.Size(103, 33);
            this.btnSetTorque.TabIndex = 24;
            this.btnSetTorque.Text = "Torque";
            this.btnSetTorque.Click += new System.EventHandler(this.btnSetTorque_Click);
            // 
            // btnSpeedMode
            // 
            this.btnSpeedMode.Location = new System.Drawing.Point(21, 109);
            this.btnSpeedMode.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btnSpeedMode.Name = "btnSpeedMode";
            this.btnSpeedMode.Size = new System.Drawing.Size(104, 32);
            this.btnSpeedMode.TabIndex = 23;
            this.btnSpeedMode.Text = "Speed";
            this.btnSpeedMode.Click += new System.EventHandler(this.btnSpeedMode_Click);
            // 
            // tbSpeedReference
            // 
            this.tbSpeedReference.EditValue = "0";
            this.tbSpeedReference.Location = new System.Drawing.Point(24, 188);
            this.tbSpeedReference.Name = "tbSpeedReference";
            this.tbSpeedReference.Size = new System.Drawing.Size(100, 22);
            this.tbSpeedReference.TabIndex = 22;
            // 
            // btnSetSpeed
            // 
            this.btnSetSpeed.Location = new System.Drawing.Point(158, 182);
            this.btnSetSpeed.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btnSetSpeed.Name = "btnSetSpeed";
            this.btnSetSpeed.Size = new System.Drawing.Size(90, 32);
            this.btnSetSpeed.TabIndex = 21;
            this.btnSetSpeed.Text = "Set";
            this.btnSetSpeed.Click += new System.EventHandler(this.btnSetSpeed_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(22, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(45, 14);
            this.label1.TabIndex = 20;
            this.label1.Text = "Motor";
            // 
            // btnOff
            // 
            this.btnOff.Location = new System.Drawing.Point(145, 34);
            this.btnOff.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btnOff.Name = "btnOff";
            this.btnOff.Size = new System.Drawing.Size(103, 37);
            this.btnOff.TabIndex = 19;
            this.btnOff.Text = "Off";
            this.btnOff.Click += new System.EventHandler(this.btnOff_Click);
            // 
            // btnOn
            // 
            this.btnOn.Location = new System.Drawing.Point(21, 34);
            this.btnOn.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btnOn.Name = "btnOn";
            this.btnOn.Size = new System.Drawing.Size(103, 37);
            this.btnOn.TabIndex = 18;
            this.btnOn.Text = "On";
            this.btnOn.Click += new System.EventHandler(this.btnOn_Click);
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 500;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // ABBDriveControlPad
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panel1);
            this.Name = "ABBDriveControlPad";
            this.Size = new System.Drawing.Size(661, 268);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.stateIndicatorGauge1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.stateIndicatorComponent1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbSpeedReference.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private DevExpress.XtraGauges.Win.GaugeControl gaugeControl1;
        private DevExpress.XtraGauges.Win.Gauges.State.StateIndicatorGauge stateIndicatorGauge1;
        private DevExpress.XtraGauges.Win.Gauges.State.StateIndicatorComponent stateIndicatorComponent1;
        private DevExpress.XtraEditors.SimpleButton btnDisconnect;
        private DevExpress.XtraEditors.SimpleButton btnConnect;
        private System.Windows.Forms.Label lbConnection;
        private DevExpress.XtraEditors.SimpleButton btnReset;
        private System.Windows.Forms.Label lbError;
        private System.Windows.Forms.Label lbTorqueFeedback;
        private System.Windows.Forms.Label lbSpeedFeedback;
        private System.Windows.Forms.Label lbModeFeedback;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private DevExpress.XtraEditors.SimpleButton btnSetTorque;
        private DevExpress.XtraEditors.SimpleButton btnSpeedMode;
        private DevExpress.XtraEditors.TextEdit tbSpeedReference;
        private DevExpress.XtraEditors.SimpleButton btnSetSpeed;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.SimpleButton btnOff;
        private DevExpress.XtraEditors.SimpleButton btnOn;
        private System.Windows.Forms.Timer timer1;
    }
}
