﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using ATOM_Class;
using DevExpress.XtraVerticalGrid.Rows;

namespace ATOM_NewUI
{
    public partial class VariableEdit : DevExpress.XtraEditors.XtraForm
    {
        VariableInfo var;
        bool edit;
        bool calculation;
        CommunicationType commType;

        CommunicationConnector communicationConnector;

        public VariableEdit(VariableInfo var, bool edit, CommunicationType commType)
        {
            InitializeComponent();

            communicationConnector = CommunicationConnector.GetInstance();

            this.var = var;
            if(this.var == null)
            {
                this.var = new VariableInfo();
            }
            this.edit = edit;
            this.commType = commType;

            propertyGridControl1.SelectedObject = this.var;
            gridControl1.DataSource = this.var.CalibrationTable;
            if (edit)
            {
                btnAdd.Visible = false;
                this.Text = "Edit " + var.Name;
            }
            else
            {
                btnClose.Text = "취소";
                this.Text = "Add New Variable";
            }

            BaseRow alarmRow = propertyGridControl1.GetRowByCaption("Alarm");
            BaseRow basicRow = propertyGridControl1.GetRowByCaption("Basic");
            BaseRow calibrationFomulaRow = propertyGridControl1.GetRowByCaption("Calibration - Formula");
            BaseRow calibrationValueRow = propertyGridControl1.GetRowByCaption("Calibration - Value");
            BaseRow valueRow = propertyGridControl1.GetRowByCaption("Value");
            BaseRow valueProcessingRow = propertyGridControl1.GetRowByCaption("Value Processing");

            BaseRow useAlarmRow = propertyGridControl1.GetRowByCaption("Use Alarm");

            BaseRow useAveragingRow = propertyGridControl1.GetRowByCaption("Use Averaging");

            if (valueRow != null)
            {
                propertyGridControl1.OptionsBehavior.PropertySort = DevExpress.XtraVerticalGrid.PropertySort.NoSort;
                
                //Sort Categories
                propertyGridControl1.MoveRow(basicRow, null, false);
                propertyGridControl1.MoveRow(valueProcessingRow, null, false);
                propertyGridControl1.MoveRow(calibrationValueRow, null, false);
                propertyGridControl1.MoveRow(calibrationFomulaRow, null, false);
                propertyGridControl1.MoveRow(valueRow, null, false);
                propertyGridControl1.MoveRow(alarmRow, null, false);

                //Sort Properties
                propertyGridControl1.MoveRow(useAveragingRow, valueProcessingRow, false);
                propertyGridControl1.MoveRow(useAlarmRow, alarmRow, false);
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            communicationConnector.FindCommunication(commType).Variables.Add(this.var);
            this.Close();
        }
        private void btnClose_Click(object sender, EventArgs e)
        {
            if (edit)
            {
                this.Close();
            }
            else
            {
                this.var = null;
                this.Close();
            }
        }

        private void btnAddCalibrationTableRow_Click(object sender, EventArgs e)
        {
            try
            {
                var.CalibrationTable.Add(new ValuePair(double.Parse(tbIOValue.Text), double.Parse(tbRealValue.Text)));

                gridControl1.BeginUpdate();
                gridControl1.EndUpdate();
            }
            catch (Exception)
            {

            }
        }

        private void btnRemoveCalibrationTableRow_Click(object sender, EventArgs e)
        {
            if (gridView1.FocusedRowHandle > 0)
            {
                var.CalibrationTable.RemoveAt(gridView1.FocusedRowHandle);

                gridControl1.BeginUpdate();
                gridControl1.EndUpdate();
            }
        }

    }
}