﻿namespace ATOM_NewUI
{
    partial class VariableEdit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VariableEdit));
            this.pnTop = new System.Windows.Forms.Panel();
            this.pnFill = new System.Windows.Forms.Panel();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.pnFillLeftFill = new System.Windows.Forms.Panel();
            this.propertyGridControl1 = new DevExpress.XtraVerticalGrid.PropertyGridControl();
            this.pnFillLeftTop = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.pnFillRightFill = new System.Windows.Forms.Panel();
            this.pnFillRightFillFill = new System.Windows.Forms.Panel();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.ColIOValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ColRealValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.pnFillRightFillBottom = new System.Windows.Forms.Panel();
            this.dataLayoutControl1 = new DevExpress.XtraDataLayout.DataLayoutControl();
            this.tbRealValue = new DevExpress.XtraEditors.TextEdit();
            this.tbIOValue = new DevExpress.XtraEditors.TextEdit();
            this.btnRemoveCalibrationTableRow = new DevExpress.XtraEditors.SimpleButton();
            this.btnAddCalibrationTableRow = new DevExpress.XtraEditors.SimpleButton();
            this.Root = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.pnFillRightFillTop = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.pnFillRightBottom = new System.Windows.Forms.Panel();
            this.pnFillRightBottomFill = new System.Windows.Forms.Panel();
            this.propertyDescriptionControl1 = new DevExpress.XtraVerticalGrid.PropertyDescriptionControl();
            this.pnFillRightBottomTop = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.pnBottom = new System.Windows.Forms.Panel();
            this.btnAdd = new DevExpress.XtraEditors.SimpleButton();
            this.btnClose = new DevExpress.XtraEditors.SimpleButton();
            this.pnFill.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            this.pnFillLeftFill.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.propertyGridControl1)).BeginInit();
            this.pnFillLeftTop.SuspendLayout();
            this.pnFillRightFill.SuspendLayout();
            this.pnFillRightFillFill.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            this.pnFillRightFillBottom.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).BeginInit();
            this.dataLayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbRealValue.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbIOValue.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Root)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            this.pnFillRightFillTop.SuspendLayout();
            this.pnFillRightBottom.SuspendLayout();
            this.pnFillRightBottomFill.SuspendLayout();
            this.pnFillRightBottomTop.SuspendLayout();
            this.pnBottom.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnTop
            // 
            this.pnTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnTop.Location = new System.Drawing.Point(0, 0);
            this.pnTop.Name = "pnTop";
            this.pnTop.Size = new System.Drawing.Size(831, 10);
            this.pnTop.TabIndex = 0;
            // 
            // pnFill
            // 
            this.pnFill.Controls.Add(this.splitContainerControl1);
            this.pnFill.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnFill.Location = new System.Drawing.Point(0, 10);
            this.pnFill.Name = "pnFill";
            this.pnFill.Size = new System.Drawing.Size(831, 532);
            this.pnFill.TabIndex = 1;
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl1.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.Controls.Add(this.pnFillLeftFill);
            this.splitContainerControl1.Panel1.Controls.Add(this.pnFillLeftTop);
            this.splitContainerControl1.Panel1.Text = "Panel1";
            this.splitContainerControl1.Panel2.Controls.Add(this.pnFillRightFill);
            this.splitContainerControl1.Panel2.Controls.Add(this.pnFillRightBottom);
            this.splitContainerControl1.Panel2.Text = "Panel2";
            this.splitContainerControl1.Size = new System.Drawing.Size(831, 532);
            this.splitContainerControl1.SplitterPosition = 564;
            this.splitContainerControl1.TabIndex = 0;
            // 
            // pnFillLeftFill
            // 
            this.pnFillLeftFill.Controls.Add(this.propertyGridControl1);
            this.pnFillLeftFill.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnFillLeftFill.Location = new System.Drawing.Point(0, 34);
            this.pnFillLeftFill.Name = "pnFillLeftFill";
            this.pnFillLeftFill.Size = new System.Drawing.Size(564, 498);
            this.pnFillLeftFill.TabIndex = 1;
            // 
            // propertyGridControl1
            // 
            this.propertyGridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.propertyGridControl1.Location = new System.Drawing.Point(0, 0);
            this.propertyGridControl1.Name = "propertyGridControl1";
            this.propertyGridControl1.Size = new System.Drawing.Size(564, 498);
            this.propertyGridControl1.TabIndex = 0;
            // 
            // pnFillLeftTop
            // 
            this.pnFillLeftTop.Controls.Add(this.label1);
            this.pnFillLeftTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnFillLeftTop.Location = new System.Drawing.Point(0, 0);
            this.pnFillLeftTop.Name = "pnFillLeftTop";
            this.pnFillLeftTop.Size = new System.Drawing.Size(564, 34);
            this.pnFillLeftTop.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(5, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(58, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "변수 설정";
            // 
            // pnFillRightFill
            // 
            this.pnFillRightFill.Controls.Add(this.pnFillRightFillFill);
            this.pnFillRightFill.Controls.Add(this.pnFillRightFillBottom);
            this.pnFillRightFill.Controls.Add(this.pnFillRightFillTop);
            this.pnFillRightFill.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnFillRightFill.Location = new System.Drawing.Point(0, 0);
            this.pnFillRightFill.Name = "pnFillRightFill";
            this.pnFillRightFill.Size = new System.Drawing.Size(257, 328);
            this.pnFillRightFill.TabIndex = 2;
            // 
            // pnFillRightFillFill
            // 
            this.pnFillRightFillFill.Controls.Add(this.gridControl1);
            this.pnFillRightFillFill.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnFillRightFillFill.Location = new System.Drawing.Point(0, 34);
            this.pnFillRightFillFill.Name = "pnFillRightFillFill";
            this.pnFillRightFillFill.Size = new System.Drawing.Size(257, 213);
            this.pnFillRightFillFill.TabIndex = 2;
            // 
            // gridControl1
            // 
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.Location = new System.Drawing.Point(0, 0);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(257, 213);
            this.gridControl1.TabIndex = 0;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.ColIOValue,
            this.ColRealValue});
            this.gridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFullFocus;
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsCustomization.AllowColumnMoving = false;
            this.gridView1.OptionsCustomization.AllowSort = false;
            this.gridView1.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.OptionsView.ShowIndicator = false;
            // 
            // ColIOValue
            // 
            this.ColIOValue.Caption = "IO Value";
            this.ColIOValue.FieldName = "IOValue";
            this.ColIOValue.Name = "ColIOValue";
            this.ColIOValue.OptionsFilter.AllowAutoFilter = false;
            this.ColIOValue.OptionsFilter.AllowFilter = false;
            this.ColIOValue.Visible = true;
            this.ColIOValue.VisibleIndex = 0;
            // 
            // ColRealValue
            // 
            this.ColRealValue.Caption = "Real Value";
            this.ColRealValue.FieldName = "RealValue";
            this.ColRealValue.Name = "ColRealValue";
            this.ColRealValue.OptionsFilter.AllowAutoFilter = false;
            this.ColRealValue.OptionsFilter.AllowFilter = false;
            this.ColRealValue.Visible = true;
            this.ColRealValue.VisibleIndex = 1;
            // 
            // pnFillRightFillBottom
            // 
            this.pnFillRightFillBottom.Controls.Add(this.dataLayoutControl1);
            this.pnFillRightFillBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnFillRightFillBottom.Location = new System.Drawing.Point(0, 247);
            this.pnFillRightFillBottom.Name = "pnFillRightFillBottom";
            this.pnFillRightFillBottom.Size = new System.Drawing.Size(257, 81);
            this.pnFillRightFillBottom.TabIndex = 3;
            // 
            // dataLayoutControl1
            // 
            this.dataLayoutControl1.Controls.Add(this.tbRealValue);
            this.dataLayoutControl1.Controls.Add(this.tbIOValue);
            this.dataLayoutControl1.Controls.Add(this.btnRemoveCalibrationTableRow);
            this.dataLayoutControl1.Controls.Add(this.btnAddCalibrationTableRow);
            this.dataLayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataLayoutControl1.Location = new System.Drawing.Point(0, 0);
            this.dataLayoutControl1.Name = "dataLayoutControl1";
            this.dataLayoutControl1.OptionsView.ShareLookAndFeelWithChildren = false;
            this.dataLayoutControl1.Root = this.Root;
            this.dataLayoutControl1.Size = new System.Drawing.Size(257, 81);
            this.dataLayoutControl1.TabIndex = 0;
            this.dataLayoutControl1.Text = "dataLayoutControl1";
            // 
            // tbRealValue
            // 
            this.tbRealValue.Location = new System.Drawing.Point(189, 30);
            this.tbRealValue.Name = "tbRealValue";
            this.tbRealValue.Size = new System.Drawing.Size(64, 22);
            this.tbRealValue.TabIndex = 7;
            // 
            // tbIOValue
            // 
            this.tbIOValue.Location = new System.Drawing.Point(61, 30);
            this.tbIOValue.Name = "tbIOValue";
            this.tbIOValue.Size = new System.Drawing.Size(67, 22);
            this.tbIOValue.TabIndex = 6;
            // 
            // btnRemoveCalibrationTableRow
            // 
            this.btnRemoveCalibrationTableRow.Location = new System.Drawing.Point(132, 4);
            this.btnRemoveCalibrationTableRow.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btnRemoveCalibrationTableRow.Name = "btnRemoveCalibrationTableRow";
            this.btnRemoveCalibrationTableRow.Size = new System.Drawing.Size(121, 22);
            this.btnRemoveCalibrationTableRow.TabIndex = 5;
            this.btnRemoveCalibrationTableRow.Text = "제거";
            this.btnRemoveCalibrationTableRow.Click += new System.EventHandler(this.btnRemoveCalibrationTableRow_Click);
            // 
            // btnAddCalibrationTableRow
            // 
            this.btnAddCalibrationTableRow.Location = new System.Drawing.Point(4, 4);
            this.btnAddCalibrationTableRow.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btnAddCalibrationTableRow.Name = "btnAddCalibrationTableRow";
            this.btnAddCalibrationTableRow.Size = new System.Drawing.Size(124, 22);
            this.btnAddCalibrationTableRow.TabIndex = 4;
            this.btnAddCalibrationTableRow.Text = "추가";
            this.btnAddCalibrationTableRow.Click += new System.EventHandler(this.btnAddCalibrationTableRow_Click);
            // 
            // Root
            // 
            this.Root.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.Root.GroupBordersVisible = false;
            this.Root.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem2,
            this.layoutControlItem3,
            this.layoutControlItem4});
            this.Root.Name = "Root";
            this.Root.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.Root.Size = new System.Drawing.Size(257, 81);
            this.Root.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.btnAddCalibrationTableRow;
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(128, 26);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.btnRemoveCalibrationTableRow;
            this.layoutControlItem2.Location = new System.Drawing.Point(128, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(125, 26);
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextVisible = false;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.tbIOValue;
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 26);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(128, 51);
            this.layoutControlItem3.Text = "IO Value";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(54, 15);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.tbRealValue;
            this.layoutControlItem4.Location = new System.Drawing.Point(128, 26);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(125, 51);
            this.layoutControlItem4.Text = "Real Value";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(54, 15);
            // 
            // pnFillRightFillTop
            // 
            this.pnFillRightFillTop.Controls.Add(this.label2);
            this.pnFillRightFillTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnFillRightFillTop.Location = new System.Drawing.Point(0, 0);
            this.pnFillRightFillTop.Name = "pnFillRightFillTop";
            this.pnFillRightFillTop.Size = new System.Drawing.Size(257, 34);
            this.pnFillRightFillTop.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(4, 10);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(104, 15);
            this.label2.TabIndex = 1;
            this.label2.Text = "Calibration 테이블";
            // 
            // pnFillRightBottom
            // 
            this.pnFillRightBottom.Controls.Add(this.pnFillRightBottomFill);
            this.pnFillRightBottom.Controls.Add(this.pnFillRightBottomTop);
            this.pnFillRightBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnFillRightBottom.Location = new System.Drawing.Point(0, 328);
            this.pnFillRightBottom.Name = "pnFillRightBottom";
            this.pnFillRightBottom.Size = new System.Drawing.Size(257, 204);
            this.pnFillRightBottom.TabIndex = 3;
            // 
            // pnFillRightBottomFill
            // 
            this.pnFillRightBottomFill.Controls.Add(this.propertyDescriptionControl1);
            this.pnFillRightBottomFill.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnFillRightBottomFill.Location = new System.Drawing.Point(0, 40);
            this.pnFillRightBottomFill.Name = "pnFillRightBottomFill";
            this.pnFillRightBottomFill.Size = new System.Drawing.Size(257, 164);
            this.pnFillRightBottomFill.TabIndex = 1;
            // 
            // propertyDescriptionControl1
            // 
            this.propertyDescriptionControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.propertyDescriptionControl1.Location = new System.Drawing.Point(0, 0);
            this.propertyDescriptionControl1.Name = "propertyDescriptionControl1";
            this.propertyDescriptionControl1.PropertyGrid = this.propertyGridControl1;
            this.propertyDescriptionControl1.Size = new System.Drawing.Size(257, 164);
            this.propertyDescriptionControl1.TabIndex = 0;
            this.propertyDescriptionControl1.TabStop = false;
            // 
            // pnFillRightBottomTop
            // 
            this.pnFillRightBottomTop.Controls.Add(this.label3);
            this.pnFillRightBottomTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnFillRightBottomTop.Location = new System.Drawing.Point(0, 0);
            this.pnFillRightBottomTop.Name = "pnFillRightBottomTop";
            this.pnFillRightBottomTop.Size = new System.Drawing.Size(257, 40);
            this.pnFillRightBottomTop.TabIndex = 0;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(5, 12);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(31, 15);
            this.label3.TabIndex = 2;
            this.label3.Text = "설명";
            // 
            // pnBottom
            // 
            this.pnBottom.Controls.Add(this.btnAdd);
            this.pnBottom.Controls.Add(this.btnClose);
            this.pnBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnBottom.Location = new System.Drawing.Point(0, 542);
            this.pnBottom.Name = "pnBottom";
            this.pnBottom.Size = new System.Drawing.Size(831, 54);
            this.pnBottom.TabIndex = 2;
            // 
            // btnAdd
            // 
            this.btnAdd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAdd.Location = new System.Drawing.Point(588, 9);
            this.btnAdd.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(105, 35);
            this.btnAdd.TabIndex = 1;
            this.btnAdd.Text = "추가";
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.Location = new System.Drawing.Point(712, 9);
            this.btnClose.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(105, 35);
            this.btnClose.TabIndex = 0;
            this.btnClose.Text = "닫기";
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // VariableEdit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(831, 596);
            this.Controls.Add(this.pnFill);
            this.Controls.Add(this.pnBottom);
            this.Controls.Add(this.pnTop);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "VariableEdit";
            this.Text = "Edit Variable";
            this.pnFill.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            this.pnFillLeftFill.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.propertyGridControl1)).EndInit();
            this.pnFillLeftTop.ResumeLayout(false);
            this.pnFillLeftTop.PerformLayout();
            this.pnFillRightFill.ResumeLayout(false);
            this.pnFillRightFillFill.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            this.pnFillRightFillBottom.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).EndInit();
            this.dataLayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tbRealValue.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbIOValue.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Root)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            this.pnFillRightFillTop.ResumeLayout(false);
            this.pnFillRightFillTop.PerformLayout();
            this.pnFillRightBottom.ResumeLayout(false);
            this.pnFillRightBottomFill.ResumeLayout(false);
            this.pnFillRightBottomTop.ResumeLayout(false);
            this.pnFillRightBottomTop.PerformLayout();
            this.pnBottom.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnTop;
        private System.Windows.Forms.Panel pnFill;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private System.Windows.Forms.Panel pnBottom;
        private System.Windows.Forms.Panel pnFillLeftFill;
        private DevExpress.XtraVerticalGrid.PropertyGridControl propertyGridControl1;
        private System.Windows.Forms.Panel pnFillLeftTop;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel pnFillRightFill;
        private System.Windows.Forms.Panel pnFillRightFillTop;
        private System.Windows.Forms.Label label2;
        private DevExpress.XtraEditors.SimpleButton btnClose;
        private System.Windows.Forms.Panel pnFillRightFillBottom;
        private System.Windows.Forms.Panel pnFillRightFillFill;
        private System.Windows.Forms.Panel pnFillRightBottom;
        private System.Windows.Forms.Panel pnFillRightBottomFill;
        private DevExpress.XtraVerticalGrid.PropertyDescriptionControl propertyDescriptionControl1;
        private System.Windows.Forms.Panel pnFillRightBottomTop;
        private System.Windows.Forms.Label label3;
        private DevExpress.XtraEditors.SimpleButton btnAdd;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn ColIOValue;
        private DevExpress.XtraGrid.Columns.GridColumn ColRealValue;
        private DevExpress.XtraDataLayout.DataLayoutControl dataLayoutControl1;
        private DevExpress.XtraEditors.TextEdit tbRealValue;
        private DevExpress.XtraEditors.TextEdit tbIOValue;
        private DevExpress.XtraEditors.SimpleButton btnRemoveCalibrationTableRow;
        private DevExpress.XtraEditors.SimpleButton btnAddCalibrationTableRow;
        private DevExpress.XtraLayout.LayoutControlGroup Root;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
    }
}