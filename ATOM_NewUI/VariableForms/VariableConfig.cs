﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using ATOM_Class;

namespace ATOM_NewUI
{
    public partial class VariableConfig : DevExpress.XtraEditors.XtraForm
    {
        CommunicationConnector communicationConnector;

        public VariableConfig()
        {
            InitializeComponent();
            Init();
        }

        private void Init()
        {
            communicationConnector = CommunicationConnector.GetInstance();

            foreach (ICommunication com in communicationConnector.GetCommunications())
            {
                cbCommType.Properties.Items.Add(com.CommType.ToString());
            }

            if (cbCommType.Properties.Items.Count != 0)
            {
                cbCommType.SelectedIndex = 0;
            }

        }

        private void cbCommType_SelectedIndexChanged(object sender, EventArgs e)
        {
            CommunicationType commType;
            if (Enum.TryParse(cbCommType.Text, out commType))
            {
                gridView1.BeginDataUpdate();
                gridControl1.DataSource = communicationConnector.GetVariables(commType);
                gridView1.EndDataUpdate();

                ICommunication com = communicationConnector.FindCommunication(commType);
                if (com.VariableEditable)
                {
                    btnAddVariable.Enabled = true;
                    btnRemoveVariable.Enabled = true;
                }
                else
                {
                    btnAddVariable.Enabled = false;
                    btnRemoveVariable.Enabled = false;
                }
            }

        }

        private void btnAddVariable_Click(object sender, EventArgs e)
        {
            CommunicationType commType;
            if (Enum.TryParse(cbCommType.Text, out commType))
            {

                VariableEdit newform = new VariableEdit(null, false, commType);
                newform.ShowDialog();

                gridControl1.BeginUpdate();
                gridControl1.EndUpdate();
            }
        }
        private void btnRemoveVariable_Click(object sender, EventArgs e)
        {
            CommunicationType commType;
            if (Enum.TryParse(cbCommType.Text, out commType))
            {

                if (XtraMessageBox.Show(communicationConnector.GetVariables(commType)[gridView1.FocusedRowHandle].Name + "변수를 정말 삭제하시겠습니까?</size>", "변수 삭제", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                {
                    communicationConnector.GetVariables(commType).RemoveAt(gridView1.FocusedRowHandle);
                    
                    gridControl1.BeginUpdate();
                    gridControl1.EndUpdate();
                }
            }
        }

        private void btnEditVariable_Click(object sender, EventArgs e)
        {
            CommunicationType commType;
            if (Enum.TryParse(cbCommType.Text, out commType))
            {
                VariableInfo var = communicationConnector.GetVariables(commType)[gridView1.FocusedRowHandle];

                VariableEdit newform = new VariableEdit(var, true, commType);
                newform.ShowDialog();

                gridControl1.BeginUpdate();
                gridControl1.EndUpdate();
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}