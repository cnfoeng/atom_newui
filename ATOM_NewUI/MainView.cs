﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using log4net;
using DevExpress.XtraEditors;
using ATOM_Class;
using DevExpress.XtraRichEdit;
using DevExpress.XtraBars.Docking2010.Views.Tabbed;
using ATOM_NewUI.Monitoring;
using DevExpress.XtraBars;
using System.Diagnostics;
using DevExpress.XtraTreeList.Nodes;
using DevExpress.XtraTreeList;
using DevExpress.XtraTreeList.Columns;

namespace ATOM_NewUI
{
    public partial class MainView : DevExpress.XtraEditors.XtraForm
    {
        static ILog logger = LogManager.GetLogger("MainForm");
        CommunicationConnector communicationConnector;
        DataLogger dataLogger = new DataLogger();
        MonitoringManager monitoringManager = new MonitoringManager();
        SystemLog systemLog;
        TreeListNode parentForRootNodes = null;
        TreeListNode RootNode = null;
        TreeListNode level1Node = null;
        public MainView()
        {
            InitializeComponent();
            InitButtons();
            systemLog = SystemLog.GetInstance();

            gridControl1.DataSource = systemLog.GetLogs();

            systemLog.AddLog("프로그램 시작", LogType.Information);
        }
        private void InitButtons()
        {
            barButtonItemConnect_Quick.Enabled = true;
            barButtonItemDisconnect_Quick.Enabled = false;
        }
        private void InitTreeListColumn()
        {

            treeListMain.ViewStyle = TreeListViewStyle.TreeView;

            treeListMain.BeginUpdate();
            TreeListColumn col1 = treeListMain.Columns.Add();
            col1.Caption = "Connection";
            col1.VisibleIndex = 0;
            treeListMain.EndUpdate();

            treeListMain.StateImageList = svgImageCollection1;

            UpdateTreeList();
        }

        private void UpdateTreeList()
        {
            treeListMain.ClearNodes();
            treeListMain.BeginUnboundLoad();
            parentForRootNodes = null;
            RootNode = treeListMain.AppendNode(new object[] { "Connection" }, parentForRootNodes);

            foreach (ICommunication com in communicationConnector.GetCommunications())
            {
                level1Node = treeListMain.AppendNode(new object[] { com.CommType.ToString() }, RootNode);
                treeListMain.AppendNode(new object[] { "Connection Information : "+com.ConnectionString }, level1Node);
                treeListMain.MoveLast();
            }

            treeListMain.MoveLast();
            
            treeListMain.EndUnboundLoad();
        }
        private void item_ItemVisibleClick(object sender, ItemClickEventArgs e)
        {
            foreach (Monitoring.Monitoring mon in monitoringManager.monitorings)
            {
                if(mon.MonitoringName == e.Item.Caption)
                {
                    mon.Visible = mon.Visible ? false : true;
                }
            }


            RefreshMonitoringPopupMenu();
            RefreshTabbedViewDocuments();
        }
        private void RefreshMonitoringPopupMenu()
        {
            popupMenuMonitoringVisible.ItemLinks.Clear();
            popupMenuMonitoringRemove.ItemLinks.Clear();

            foreach (Monitoring.Monitoring mon in monitoringManager.monitorings)
            {
                BarButtonItem itemVisible = new BarButtonItem() { Caption = mon.MonitoringName, Id = 0, ImageIndex = 0, Name = mon.MonitoringName };
                itemVisible.ButtonStyle = BarButtonStyle.Check;
                if (mon.Visible)
                {
                    itemVisible.Down = true;
                }
                itemVisible.ItemClick += new ItemClickEventHandler(item_ItemVisibleClick);

                popupMenuMonitoringVisible.ItemLinks.Add(itemVisible);


                BarButtonItem itemRemove = new BarButtonItem() { Caption = mon.MonitoringName, Id = 0, ImageIndex = 0, Name = mon.MonitoringName };

                itemRemove.ItemClick += new ItemClickEventHandler(item_ItemRemoveClick);

                popupMenuMonitoringRemove.ItemLinks.Add(itemRemove);
            }
        }

        private void item_ItemRemoveClick(object sender, ItemClickEventArgs e)
        {

            DialogResult result = XtraMessageBox.Show(e.Item.Caption +" 모니터링 화면을 정말 삭제하시겠습니까?", "모니터링 삭제", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
            
            if(result != DialogResult.Yes)
            {
                return;
            }

            //삭제 전에 Visible = false로 변경해 우선 Document에서 먼저 지워주고 그 후에 삭제해야 함. UI 표시문제때문.
            foreach (Monitoring.Monitoring mon in monitoringManager.monitorings)
            {
                if (mon.MonitoringName == e.Item.Caption)
                {
                    mon.Visible = false;
                }
            }

            RefreshTabbedViewDocuments();

            monitoringManager.RemoveMonitoring(e.Item.Caption);
            
            RefreshMonitoringPopupMenu();
        }

        private void RefreshTabbedViewDocuments()
        {
            
            Document document = new Document();
            foreach (Monitoring.Monitoring mon in monitoringManager.monitorings)
            {
                bool flag = true;
                foreach (Document doc in tabbedView1.Documents)
                {
                    if(doc.Caption == mon.MonitoringName)
                    {
                        document = doc;
                        flag = false;
                        break;
                    }
                }

                if (mon.Visible && flag)
                {
                    tabbedView1.AddDocument(mon.MonitoringName, "Monitoring");
                }
                else if (!mon.Visible && !flag)
                {
                    tabbedView1.Controller.Close(document);
                }
            }
        }
        private void MainForm_Load(object sender, EventArgs e)
        {
            communicationConnector = CommunicationConnector.GetInstance();
            dataLogger = dataLogger.Load(Application.StartupPath + "\\Config\\DataLogger.bin");
            monitoringManager = monitoringManager.Load(Application.StartupPath + "\\Config\\MonitoringManager.bin");
            monitoringManager.ReAssignVariables();

            communicationConnector.ResetVariableValues();
            communicationConnector.ResetCommunication();

            tabbedView1.Documents.Clear();

            RefreshTabbedViewDocuments();
            RefreshMonitoringPopupMenu();

            pnABBDriveControlPad.Controls.Add(new ABBDriveControlPad());


            InitTreeListColumn();
        }

        internal void onIdle()
        {

        }

        #region Events

        private void barButtonItemCommunicationConfig_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Settings.CommunicationConfig form = new Settings.CommunicationConfig();
            form.ShowDialog();

            UpdateTreeList();
        }
        private void barButtonItemConnect_Quick_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            communicationConnector.Connect();

            barButtonItemConnect_Quick.Enabled = false;
            barButtonItemDisconnect_Quick.Enabled = true;
        }


        private void barButtonItemDisconnect_Quick_ItemClick(object sender, ItemClickEventArgs e)
        {
            
            barButtonItemConnect_Quick.Enabled = true;
            barButtonItemDisconnect_Quick.Enabled = false;
        }
        private void tabbedView1_QueryControl(object sender, DevExpress.XtraBars.Docking2010.Views.QueryControlEventArgs e)
        {
            tabbedView1.BeginUpdate();
            e.Control = new Control();

            if (e.Document.ControlName == "Monitoring")
            {
                List<MonitorItem> monitorItems = new List<MonitorItem>();

                foreach (Monitoring.Monitoring mon in monitoringManager.monitorings)
                {
                    if (mon.MonitoringName == e.Document.Caption)
                    {
                        monitorItems = mon.monitoringItems;
                    }
                }

                e.Control = new MonitoringForm(e.Document.Caption, monitorItems);
            }
            tabbedView1.EndUpdate();
        }

        private void MainView_FormClosed(object sender, FormClosedEventArgs e)
        {
            communicationConnector.Save(Application.StartupPath + Properties.Resources.ConfigDirectoryPath + Properties.Resources.CommunicationConfigFileName);
            dataLogger.Save(Application.StartupPath + "\\Config\\DataLogger.bin");
            monitoringManager.Save(Application.StartupPath + "\\Config\\MonitoringManager.bin");

        }

        private void barButtonItemACSMapping_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            SettingForm.ACS.ACSMapping newform = new SettingForm.ACS.ACSMapping();
            newform.Show();
        }


        #endregion

        private void barButtonItemDataLoggingSetting_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            DataLogging.DataLogSetting newform = new DataLogging.DataLogSetting(dataLogger);
            newform.Show();
        }

        private void barButtonItemAddMonitoring_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            string result = XtraInputBox.Show("모니터링 이름을 입력해주세요.", "모니터링 추가", "");

            if (result != "")
            {
                monitoringManager.AddMonitoring(result);
            }

            RefreshMonitoringPopupMenu();
            RefreshTabbedViewDocuments();
        }

        private void tabbedView1_DocumentClosed(object sender, DevExpress.XtraBars.Docking2010.Views.DocumentEventArgs e)
        {
            //Todo : Close 버튼으로 닫아도 visible 변화되게.
        }

        private void tmrUpdateUI_Tick(object sender, EventArgs e)
        {
            foreach (TreeListNode node in treeListMain.Nodes[0].Nodes)
            {
                string value = treeListMain.GetRowCellValue(node, treeListMain.Columns[0]).ToString();
                CommunicationType commtype = (CommunicationType)Enum.Parse(typeof(CommunicationType), value);
                ICommunication com = communicationConnector.FindCommunication(commtype);

                if(com == null)
                {
                    UpdateTreeList();
                    return;
                }

                if (com.IsConnected)
                {
                    node.StateImageIndex = 0;
                }
                else
                {
                    node.StateImageIndex = 1;
                }
                treeListMain.RefreshNode(node);
            }
            
        }

        private void barButtonItemVariableSettings_ItemClick(object sender, ItemClickEventArgs e)
        {
            VariableConfig newform = new VariableConfig();
            newform.Show();
        }
    }
}
