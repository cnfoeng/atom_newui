﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using DevExpress.UserSkins;
using DevExpress.Skins;
using DevExpress.LookAndFeel;
using log4net;
using log4net.Config;
using log4net.Appender;
using log4net.Layout;
using log4net.Repository.Hierarchy;
using ATOM_NewUI.Properties;
using System.Threading;
using DevExpress.XtraEditors;
using System.Diagnostics;

namespace ATOM_NewUI
{
    static class Program
    {
        internal static MainView MainForm { get; private set; }

        static ILog logger = LogManager.GetLogger("Program");
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        ///
        [STAThread]
        static void Main()
        {
            DevExpress.XtraEditors.XtraMessageBox.AllowHtmlText = true;

            bool bnew;
            Mutex mutex = new Mutex(true, "MutexName", out bnew);
            if (bnew)
            {
                Process.GetCurrentProcess().PriorityBoostEnabled = true;
                Process.GetCurrentProcess().PriorityClass = ProcessPriorityClass.RealTime;

                string deploymentKey = "lgCAAMOOmPwMBNYBJABVcGRhdGVhYmxlVGlsbD0yMDIxLTAzLTI3I1JldmlzaW9uPTABDUH9IhH+QhhoFGxHCqWnYJHuG6HG+8ducBal1exGGiVPXE76goHVAp6ToPLJmfXHykjmT/8QitrHrxXqNy5Thmbn64Q1osEvzjeof3VahIKOjwJ/tbhWxooOYH0F44ZW6qWDacAyMV/755bKlEdzFgZio5RTcuBeMu3z6kUSddQOCuT/3Cqkh43RerMz2D+HvqRU07Sl+/yZqznhZWcBKCn2CZqp7cV4t9Y6J3YSSAu8OsyAycYlMHIcfF3y/3qfXs7XLFHyERaz0wbfsO4W3pnDP31fJfmyTE/LUuHs5SE/nwF1Cnb0KXlaUieSwxYCvxu2M6liYDY23Aoi3juKzPZdSydKrvM4zvnline7bE7T/sQZFQCFtnKHAPBE1KYUqlEZrCQPfNVgxOoKC47K1ZT/FqxEuW4RrCkh4fgC+uF2v04n9icNuZ9YkArJ525SbwwjYgup+TLv5Gwd6RRboaNhyJ/ZOQttc+pX7IJb/b2uASKReWh78uWPg45Ouj4qiw==";

                Arction.WinForms.Charting.LightningChartUltimate.SetDeploymentKey(deploymentKey);

                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Application.Run(new MainView());
                mutex.ReleaseMutex();
            }
            else
            {
                XtraMessageBox.Show("<size=14>프로그램이 이미 실행 중입니다.</size>", "<size=14>에러</size>", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Application.Exit();

            }
        }

        static void Log4NetConfiguration()
        {
            var repository = LogManager.GetRepository();
            repository.Configured = true;


            // 파일 로그 패턴 설정
            var rollingAppender = new RollingFileAppender();
            rollingAppender.Name = "RollingFile";
            // 시스템이 기동되면 파일을 추가해서 할 것인가? 새로 작성할 것인가?
            rollingAppender.AppendToFile = true;
            rollingAppender.DatePattern = "-yyyy-MM-dd";
            // 로그 파일 설정
            DateTime dateTime = DateTime.Now;
            rollingAppender.File = Application.StartupPath + "\\Log\\" + dateTime.Year + dateTime.Month + dateTime.Day + ".log";
            // 파일 단위는 날짜 단위인 것인가, 파일 사이즈인가?
            rollingAppender.RollingStyle = RollingFileAppender.RollingMode.Date;
            // 로그 패턴
            rollingAppender.Layout = new PatternLayout("%d [%t] %-5p %c - %m%n");
            var hierarchy = (Hierarchy)repository;
            hierarchy.Root.AddAppender(rollingAppender);
            rollingAppender.ActivateOptions();
            // 로그 출력 설정 All 이면 모든 설정이 되고 Info 이면 최하 레벨 Info 위가 설정됩니다.
            hierarchy.Root.Level = log4net.Core.Level.All;

        }
    }
}
