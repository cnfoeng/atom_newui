﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using ATOM_Class;

namespace ATOM_NewUI
{
    public partial class ABBDriveControlPad : DevExpress.XtraEditors.XtraUserControl
    {
        CommunicationConnector communicationConnector;
        ABBDriverClass abbDriver;

        public ABBDriveControlPad()
        {
            InitializeComponent();

            communicationConnector = CommunicationConnector.GetInstance();

            ICommunication com = communicationConnector.FindCommunication(CommunicationType.ABBDrive);

            if (com == null)
            {
                return;
            }

            abbDriver = (ABBDriverClass)com.ReturnBaseObject();
        }

        private void btnOn_Click(object sender, EventArgs e)
        {
            abbDriver.On();
        }

        private void btnOff_Click(object sender, EventArgs e)
        {
            abbDriver.Off();
        }

        private void btnSpeedMode_Click(object sender, EventArgs e)
        {
            abbDriver.SetSpeedMode();
        }

        private void btnSetTorque_Click(object sender, EventArgs e)
        {
            abbDriver.SetTorqueMode();
        }

        private void btnSetSpeed_Click(object sender, EventArgs e)
        {
            abbDriver.SetSpeed(int.Parse(tbSpeedReference.Text));
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            abbDriver.Reset();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if(abbDriver == null)
            {
                return;
            }

            if (abbDriver.IsConnected == false)
            {
                panel1.Enabled = false;
                return;
            }
            else
            {
                if (panel1.Enabled == false)
                    panel1.Enabled = true;
            }

            lbModeFeedback.Text = "Mode : " + abbDriver.DriverMode.ToString();
            lbSpeedFeedback.Text = "Speed : " + abbDriver.ActualSpeed.ToString();
            lbTorqueFeedback.Text = "Torque : " + abbDriver.ActualTorque.ToString();

            if (abbDriver.IsConnected)
            {
                stateIndicatorComponent1.StateIndex = 3;
                btnConnect.Enabled = false;
                btnDisconnect.Enabled = true;
            }
            else
            {
                stateIndicatorComponent1.StateIndex = 0;
                btnConnect.Enabled = true;
                btnDisconnect.Enabled = false;
            }
        }
    }
}
