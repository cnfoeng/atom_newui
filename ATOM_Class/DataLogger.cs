﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Diagnostics;
using System.Threading;

namespace ATOM_Class
{
    [Serializable]
    public class DataLogger
    {
        ConstantLogging constantLogging = new ConstantLogging(string.Empty, "Data", 1000, 10000);

        public DataLogger Load(string loadFilePath)
        {
            try
            {
                FileStream fileStreamObject;
                string FileName = loadFilePath;
                fileStreamObject = new FileStream(FileName, FileMode.Open);
                BinaryFormatter binaryFormatter = new BinaryFormatter();
                object dd = binaryFormatter.Deserialize(fileStreamObject);
                fileStreamObject.Close();

                return (DataLogger)dd;
            }
            catch (Exception)
            {
                return new DataLogger();
            }
        }

        public void Save(string saveFilePath)
        {
            FileStream fileStreamObject;
            string FileName = saveFilePath;
            fileStreamObject = new FileStream(FileName, FileMode.Create);
            BinaryFormatter binaryFormatter = new BinaryFormatter();
            binaryFormatter.Serialize(fileStreamObject, this);
            fileStreamObject.Close();
        }

        public void StartConstantLogging()
        {
            constantLogging.StartLogging();
        }

        public ConstantLogging GetConstantLoggingInstance()
        {
            return constantLogging;
        }

        public void SetConstantVariables(List<VariableInfo> variables)
        {
            constantLogging.ConstantDataLogging_Variables = variables;
        }
    }
    [Serializable]
    public class ConstantLogging
    {
        private string filePath = string.Empty;
        private bool dataLoggingConstant = false;
        private string strConstantFileName = string.Empty;
        private string strConstantFileNameOnly = string.Empty;
        private int fileCountConstant = 0;
        private long prevStopWatchTickConstant = 0;
        private long nowStopWatchTickConstant = 0;
        private string dataFilePrefix = "Data";
        private int loggingInterval = 1000;
        private int rowCountConstant = 0;
        private int maxRowsPerFileConstant = 10000;

        [NonSerialized]
        private Stopwatch swConstant = new Stopwatch();
        [NonSerialized]
        Thread thConstantLog;

        public List<VariableInfo> ConstantDataLogging_Variables = new List<VariableInfo>();
        
        public int LoggingInterval { get => loggingInterval; set => loggingInterval = value; }
        public string DataFilePrefix { get => dataFilePrefix; set => dataFilePrefix = value; }
        public string FilePath { get => filePath; set => filePath = value; }
        public int MaxRowsPerFileConstant { get => maxRowsPerFileConstant; set => maxRowsPerFileConstant = value; }

        public ConstantLogging(string filepath, string prefix, int interval, int maxrowcount)
        {
            this.filePath = filepath;
            dataFilePrefix = prefix;
            loggingInterval = interval;
            MaxRowsPerFileConstant = maxrowcount;
        }
        public void StartLogging()
        {
            thConstantLog = new Thread(new ThreadStart(StartConstantLog));
            thConstantLog.Start();
        }

        public void StopLogging()
        {
            swConstant.Reset();
            dataLoggingConstant = false;
            rowCountConstant = 0;
            fileCountConstant = 0;
            prevStopWatchTickConstant = 0;

            if (thConstantLog != null)
            {
                thConstantLog.Abort();
                thConstantLog.Join();
            }
        }

        public void VariableUp(int index)
        {
            if (index > 0)
            {
                VariableInfo var = ConstantDataLogging_Variables[index];
                ConstantDataLogging_Variables.RemoveAt(index);
                ConstantDataLogging_Variables.Insert(index - 1, var);
            }
        }

        public void VariableDown(int index)
        {
            if (index >= 0 && index < ConstantDataLogging_Variables.Count - 1)
            {
                VariableInfo var = ConstantDataLogging_Variables[index];
                ConstantDataLogging_Variables.RemoveAt(index);
                ConstantDataLogging_Variables.Insert(index + 1, var);
            }
        }

        public void RemoveVariables(int[] arr)
        {
            for (int i = 0; i < arr.Length; i++)
            {
                ConstantDataLogging_Variables.RemoveAt(i);
            }
        }

        private void StartConstantLog()
        {
            DataSaveStart();
            Tick();
        }
        private void DataSaveStart()
        {
            dataLoggingConstant = true;

            if ((filePath == string.Empty))
            {
                filePath = AppDomain.CurrentDomain.BaseDirectory;
            }

            string Header;
            ///////////////////////
            Header = "StartTime";
            ///////////////////////
            ///

            foreach (VariableInfo var in ConstantDataLogging_Variables)
                Header = Header + "," + var.Name;
            strConstantFileNameOnly = DataFilePrefix + "_Constant_" + DateTime.Now.ToString("yyyyMMddHHmm") + "_" + fileCountConstant + ".csv";
            strConstantFileName = filePath + "\\Constant\\" + strConstantFileNameOnly;
            try
            {
                System.IO.File.AppendAllText(strConstantFileName, Header + "\r\n", Encoding.Default);
            }
            catch (DirectoryNotFoundException e)
            {
                Directory.CreateDirectory(filePath + "\\Constant");
                System.IO.File.AppendAllText(strConstantFileName, Header + "\r\n", Encoding.Default);
            }
        }

        private void DataSave()
        {
            if (dataLoggingConstant)
            {
                if (nowStopWatchTickConstant - prevStopWatchTickConstant > LoggingInterval)
                {
                    string Value = DateTime.Now.ToString("yyyy_MM_dd HH:mm:ss") + "." + DateTime.Now.Millisecond.ToString("000");
                    prevStopWatchTickConstant = nowStopWatchTickConstant;

                    foreach (VariableInfo var in ConstantDataLogging_Variables)
                    {
                        Value = Value + "," + var.RealValue.ToString();
                    }

                    System.IO.File.AppendAllText(strConstantFileName, Value + "\r\n", Encoding.Default);
                    rowCountConstant++;
                }
            }
        }
        
        private void Tick()
        {
            if (dataLoggingConstant == true)
            {

                swConstant.Start();

                while (swConstant.IsRunning)
                {

                    if (rowCountConstant < MaxRowsPerFileConstant)
                    {
                        if (swConstant.ElapsedMilliseconds < 100000000)
                        {
                            nowStopWatchTickConstant = swConstant.ElapsedMilliseconds;

                            DataSave();
                        }
                        else
                        {
                            swConstant.Restart();
                            prevStopWatchTickConstant = 0;
                        }
                    }
                    else
                    {
                        swConstant.Reset();
                        rowCountConstant = 0;
                        prevStopWatchTickConstant = 0;
                        fileCountConstant++;
                        DataSaveStart();

                        swConstant.Start();
                        Thread.Sleep(1);
                    }
                    Thread.Sleep(10);
                }
            }
        }
    }
}
