﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ATOM_Class
{
    public interface ICommunication
    {
        CommunicationType CommType { get; }
        List<VariableInfo> Variables { get; set; }//getvariables()로 바꿔보면?
        bool IsConnected { get; set; }
        bool IsSimulator { get; set; }
        string VariableFilePath { get; set; }
        string ConnectionString { get; set; }
        string Remark { get; set; }
        bool VariableEditable { get; }
        bool Connect();
        void Disconnect();
        bool MakeVariableList();
        bool ExportVariableFile(string filePath);
        object ReturnBaseObject();
    }



}
