﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EasyModbus;
using System.Diagnostics;
using System.Timers;
namespace ATOM_Class.Oval
{
    class OvalFC200Class : ICommunication
    {
        public CommunicationType CommType => CommunicationType.OvalFC200;

        public List<VariableInfo> Variables { get; set; } = new List<VariableInfo>();
        public bool VariableEditable { get=>false;}
        public bool IsConnected { get; set; }
        public bool IsSimulator { get; set; }
        public string VariableFilePath { get; set; }
        public string ConnectionString { get; set; }
        public string Remark { get; set; }
        public double Temperature { get => temperature; set => temperature = value; }
        public double Pressure { get => pressure; set => pressure = value; }
        public double FlowVelocity { get => flowVelocity; set => flowVelocity = value; }
        public double RateMass { get => rateMass; set => rateMass = value; }

        double temperature = 0;
        double pressure = 0;
        double flowVelocity = 0;
        double rateMass = 0;
        double status = 0;

        [NonSerialized]
        ModbusClient modbusClient;
        [NonSerialized]
        Timer tmr = new Timer();
        int timeTimer = 500;
        public bool Connect()
        {
            modbusClient = new ModbusClient();
            modbusClient.SerialPort = ConnectionString;
            modbusClient.Baudrate = 9600;
            modbusClient.Parity = System.IO.Ports.Parity.None;
            modbusClient.StopBits = System.IO.Ports.StopBits.One;
            modbusClient.UnitIdentifier = 1;

            try
            {
                modbusClient.Connect();
                System.Threading.Thread.Sleep(200);
                IsConnected = true;
            }
            catch (Exception)
            {
                //Connection Error
                Debug.Print("Oval FC200 Connection Error");
                IsConnected = false;
                return false;
            }

            if (modbusClient.Connected)
            {

                tmr = new Timer(timeTimer);
                tmr.Elapsed += Tmr_Elapsed;
                tmr.Enabled = true;

                return true;
            }

            return false;
        }
        private void Tmr_Elapsed(object sender, ElapsedEventArgs e)
        {
            ReadData();
        }
        private void ReadData()
        {
            if(IsConnected == false)
            {
                return;
            }
            try
            {
                int[] readData = modbusClient.ReadInputRegisters(3502 - 1, 29);

                status = readData[0];
                temperature = FloatingPointConvert(readData[21], readData[22]);
                pressure = FloatingPointConvert(readData[23], readData[24]);
                rateMass = FloatingPointConvert(readData[9], readData[10]);
                flowVelocity = FloatingPointConvert(readData[15], readData[16]);

                Variables[0].IOValue = temperature;
                Variables[1].IOValue = pressure;
                Variables[2].IOValue = rateMass;
                Variables[3].IOValue = flowVelocity;
            }
            catch (Exception)
            {
                Debug.Print("ModbusRTU Read Error");
                IsConnected = false;
                tmr.Stop();
                tmr.Enabled = false;

                return;
            }
        }
        public void Disconnect()
        {
            if (IsConnected)
            {
                modbusClient.Disconnect();
                IsConnected = false;
                tmr.Stop();
                tmr.Enabled = false;

            }
        }

        public bool ExportVariableFile(string filePath)
        {
            //미사용
            throw new NotImplementedException();
        }

        public bool MakeVariableList()
        {
            Variables.Clear();

            VariableInfo var = new VariableInfo();
            var.Name = "Temperature";
            var.Unit = "℃";
            var.Use_Alarm = true;
            var.Warning_Low = 0;
            var.Warning_High = 100;
            var.Alarm_Low = 0;
            var.Alarm_High = 100;
            var.AlarmAllowableTime = 5;
            var.Use_Averaging = true;
            var.AveragingCount = 5;
            var.CalType = CalibrationType.None;
            var.IOType = IOType.None;
            Variables.Add(var);


            var = new VariableInfo();
            var.Name = "Pressure";
            var.Unit = "";
            var.Use_Alarm = true;
            var.Warning_Low = 0;
            var.Warning_High = 100;
            var.Alarm_Low = 0;
            var.Alarm_High = 100;
            var.AlarmAllowableTime = 5;
            var.Use_Averaging = true;
            var.AveragingCount = 5;
            var.CalType = CalibrationType.None;
            var.IOType = IOType.None;
            Variables.Add(var);

            var = new VariableInfo();
            var.Name = "Rate Mass";
            var.Unit = "";
            var.Use_Alarm = true;
            var.Warning_Low = 0;
            var.Warning_High = 100;
            var.Alarm_Low = 0;
            var.Alarm_High = 100;
            var.AlarmAllowableTime = 5;
            var.Use_Averaging = true;
            var.AveragingCount = 5;
            var.CalType = CalibrationType.None;
            var.IOType = IOType.None;
            Variables.Add(var);

            var = new VariableInfo();
            var.Name = "Flow Velocity";
            var.Unit = "";
            var.Use_Alarm = true;
            var.Warning_Low = 0;
            var.Warning_High = 100;
            var.Alarm_Low = 0;
            var.Alarm_High = 100;
            var.AlarmAllowableTime = 5;
            var.Use_Averaging = true;
            var.AveragingCount = 5;
            var.CalType = CalibrationType.None;
            var.IOType = IOType.None;
            Variables.Add(var);
            return true;
        }

        public object ReturnBaseObject()
        {
            return this;
        }

        private double FloatingPointConvert(int num1, int num2) // num1 = 앞자리, num2 = 뒷자리
        {
            int MSW = num1;
            int LSW = num2;
            
            byte byte1MSW = (byte)(MSW >> 8);
            byte byte2MSW = (byte)MSW;
            byte byte1LSW = (byte)(LSW >> 8);
            byte byte2LSW = (byte)LSW;

            string strByte1MSW = Convert.ToString(byte1MSW, 2).PadLeft(8, '0');
            string strByte2MSW = Convert.ToString(byte2MSW, 2).PadLeft(8, '0');
            string strByte1LSW = Convert.ToString(byte1LSW, 2).PadLeft(8, '0');
            string strByte2LSW = Convert.ToString(byte2LSW, 2).PadLeft(8, '0');

            char firstBit_MSW = strByte1MSW.First();

            int sign;
            if (firstBit_MSW == '0')
            {
                sign = 1;
            }
            else
            {
                sign = -1;
            }


            string strValue1 = strByte2MSW.Substring(1, 7);
            byte value1 = Convert.ToByte(strValue1, 2);
            byte value2 = byte1LSW;
            byte value3 = byte2LSW;

            int temp = value1 * 256 * 256 + value2 * 256 + value3;

            int const1 = Convert.ToInt32("800000", 16);
            int const2 = Convert.ToInt32("400000", 16);

            float temp1 = ((float)temp / (float)const1) + 1;
            float temp2 = ((float)temp / (float)const2);

            string strTemp = strByte1MSW.Substring(1, 7) + strByte2MSW.First();
            int value4 = Convert.ToInt32(strTemp, 2);
            int value5 = value4 - 127;

            float result1;
            if (value4 > 0)
            {
                result1 = temp1;
            }
            else
            {
                result1 = temp2;
            }

            double FloatingPointResult = sign * result1 * Math.Pow(2, value5);

            return FloatingPointResult;
        }
    }
}
