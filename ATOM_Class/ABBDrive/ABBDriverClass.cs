﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EasyModbus;
using System.IO;
using System.Diagnostics;
using System.Timers;

namespace ATOM_Class
{
    public enum ABBDriverMode
    {
        Speed,
        Torque,
    }
    [Serializable]
    public class ABBDriverClass : ICommunication
    {
        public CommunicationType CommType { get => CommunicationType.ABBDrive; }
        public bool IsConnected { get; set; } = false;
        public bool IsSimulator { get; set; } = false;
        public string VariableFilePath { get; set; } = string.Empty;
        public string ConnectionString { get; set; } = string.Empty;
        public string Remark { get; set; } = string.Empty;
        
        public List<VariableInfo> Variables { get; set; } = new List<VariableInfo>();
        public bool VariableEditable { get => false;}

        public object ReturnBaseObject()
        {
            return this;
        }

        public bool MakeVariableList()//고정식
        {
            Variables.Clear();

            VariableInfo var = new VariableInfo();
            var.Name = "스피드";
            var.Unit = "rpm";
            var.Use_Alarm = true;
            var.Warning_Low = 0;
            var.Warning_High = 10000;
            var.Alarm_Low = 0;
            var.Alarm_High = 10000;
            var.AlarmAllowableTime = 5;
            var.Use_Averaging = true;
            var.AveragingCount = 5;
            var.CalType = CalibrationType.None;
            var.IOType = IOType.None;
            Variables.Add(var);
            

            var = new VariableInfo();
            var.Name = "토크";
            var.Unit = "%";
            var.Use_Alarm = true;
            var.Warning_Low = 0;
            var.Warning_High = 100;
            var.Alarm_Low = 0;
            var.Alarm_High = 100;
            var.AlarmAllowableTime = 5;
            var.Use_Averaging = true;
            var.AveragingCount = 5;
            var.CalType = CalibrationType.None;
            var.IOType = IOType.None;
            Variables.Add(var);

            return true;
        }
        public bool ExportVariableFile(string filePath)
        {
            //사용 안함
            throw new NotImplementedException();
        }

        [NonSerialized]
        ModbusClient modbusClient = new ModbusClient();

        [NonSerialized]
        Timer tmr = new Timer();
        int timeTimer = 500;
        int writeCW = 1150;

        int[] readData;
        int readCW = 0;
        int readReferenceValue = 0;
        int readSW = 0;
        int readActualValue1 = 0;
        int readActualValue2 = 0;
        
        bool bOn = false;
        bool bSetValue = false;
        int setValue = 0;
        int modeValue = 0;
        int speedScailingValue = 10000;
        int torqueScailingValue = 100;

        double actualSpeed = 0;
        double actualTorque = 0;

        int errorCode = 0;

        ABBDriverMode driverMode = ABBDriverMode.Speed;

        public double ActualSpeed { get => actualSpeed; }
        public double ActualTorque { get => actualTorque; }
        public ABBDriverMode DriverMode { get => driverMode; }

        public bool Connect()
        {
            //Embedded Fieldbus - ModbusRTU로 연결하는 경우
            //modbusClient.Baudrate = 19200;
            //modbusClient.Parity = System.IO.Ports.Parity.Even;
            //modbusClient.StopBits = System.IO.Ports.StopBits.One;
            //modbusClient.UnitIdentifier = 1;
            //modbusClient.SerialPort = "COM4";

            modbusClient = new ModbusClient();

            modbusClient.IPAddress = ConnectionString;

            try
            {
                modbusClient.Connect();
                IsConnected = true;
            }
            catch (Exception)
            {
                IsConnected = false;
                return false;
            }

            Reset();

            if (modbusClient.Connected)
            {

                tmr = new Timer(timeTimer);
                tmr.Elapsed += Tmr_Elapsed;
                tmr.Enabled = true;

                return true;
            }

            return false;
        }
        public void Disconnect()
        {
            modbusClient.Disconnect();
            IsConnected = false;
            tmr.Stop();
            tmr.Enabled = false;
        }
        public void Reset()
        {
            writeCW = 1150 + 128;
            bOn = false;

            modbusClient.WriteSingleRegister(0, writeCW);

            System.Threading.Thread.Sleep(10);
        }
        public void On()
        {
            writeCW = 1151 + modeValue;
            bOn = true;
        }
        public void Off()
        {
            writeCW = 1150 + modeValue;
            bOn = false;
        }
        public void SetSpeedMode()
        {
            modeValue = 0;
            
            if (bOn)
            {
                writeCW = 1151 + modeValue;
            }
            else
            {
                writeCW = 1150 + modeValue;
            }
        }
        public void SetTorqueMode()
        {
            modeValue = 2048;

            if (bOn)
            {
                writeCW = 1151 + modeValue;
            }
            else
            {
                writeCW = 1150 + modeValue;
            }
        }
        public void SetSpeed(int value)
        {
            setValue = value;
            bSetValue = true;
        }
        private void Tmr_Elapsed(object sender, ElapsedEventArgs e)
        {
            ReadAndWriteState();
        }
        private void ReadAndWriteState()
        {

            if(IsConnected == false)
            {
                return;
            }
            try
            {
                readData = modbusClient.ReadHoldingRegisters(0, 6);
                readCW = readData[0];
                readReferenceValue = readData[2];
                readSW = readData[3];
                readActualValue1 = readData[4];
                readActualValue2 = readData[5];

                readData = modbusClient.ReadHoldingRegisters(89, 4);

                errorCode = readData[0];

                modbusClient.WriteSingleRegister(0, writeCW);
            }
            catch (Exception)
            {
                Debug.Print("ModbusTCP Read Error");
                IsConnected = false;
                tmr.Stop();
                tmr.Enabled = false;

                return;
            }
            actualSpeed = Math.Round((double)readActualValue1 / 20000 * speedScailingValue,1);
            actualTorque = Math.Round((double)readActualValue2 / 10000 * torqueScailingValue,1);

            this.Variables[0].IOValue = actualSpeed;
            this.Variables[1].IOValue = actualTorque;

            string strCW = Convert.ToString(readCW, 2);
            string strSW = Convert.ToString(readSW, 2);
            strSW = strSW.PadLeft(16, '0');

            if (strSW.ElementAt(4) == '1')
            {
                driverMode = ABBDriverMode.Torque;
            }
            else
            {
                driverMode = ABBDriverMode.Speed;
            }

            Debug.Print("CW");
            Debug.Print(strCW);
            Debug.Print("SW");
            Debug.Print(strSW);
            Debug.Print("SW");
            Debug.Print(ActualSpeed.ToString());
            Debug.Print(ActualTorque.ToString());
            Debug.Print(DriverMode.ToString());
            Debug.Print(errorCode.ToString());

            if (bSetValue)
            {
                if (DriverMode == ABBDriverMode.Speed)
                {
                    modbusClient.WriteSingleRegister(1, setValue);
                }
                else
                {
                    modbusClient.WriteSingleRegister(2, setValue);
                }
                bSetValue = false;

            }
        }
        
    }
}
