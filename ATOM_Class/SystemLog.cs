﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DevExpress.Utils.Svg;

namespace ATOM_Class
{
    public enum LogType
    {
        None = 0,
        Information = 1,
        Warning = 2,
        Error = 3,
        OK = 4,
    }

    public class SystemLog
    {

        private static SystemLog systemLog = null;
        private List<LogItem> Logs = new List<LogItem>();
        public static SystemLog GetInstance()
        {
            if (systemLog == null)
            {
                systemLog = new SystemLog();
            }

            return systemLog;
        }

        public void AddLog(string message, LogType logType)
        {
            Logs.Add(new LogItem(message, logType, DateTime.Now));
        }

        public List<LogItem> GetLogs()
        {
            return Logs;
        }

    }
    public class LogItem
    {
        DateTime time;
        string message;
        SvgImage iconImage;

        public DateTime Time { get => time; set => time = value; }
        public string Message { get => message; set => message = value; }
        public SvgImage IconImage
        {
            get
            {
                SvgImage img = null;

                switch (Logtype)
                {
                    case LogType.Error:
                        img = Properties.Resources.Security_WarningCircled1;
                        break;
                    case LogType.Warning:
                        img = Properties.Resources.Warning;
                        break;
                    case LogType.Information:
                        img = Properties.Resources.About;
                        break;
                    case LogType.OK:
                        img = Properties.Resources.Actions_CheckCircled;
                        break;
                    case LogType.None:
                        break;
                }

                return img;
            }
            set => iconImage = value;
        }

        public LogType Logtype { get; set; }

        public LogItem()
        {

        }
        public LogItem(string message, LogType logType, DateTime time)
        {
            this.message = message;
            this.Logtype = logType;
            this.time = time;
        }


    }
}
