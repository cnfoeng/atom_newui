﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;
using System.Diagnostics;
using System.Timers;
using System.IO;

namespace ATOM_Class.LSPLC
{
    public enum CMDType
    {
        Read = 0,
        Write = 1
    }
    [Serializable]
    public class LS_FEnetClass : ICommunication
    {
        #region Members
        [NonSerialized]
        SystemLog systemLog;
        public CommunicationType CommType { get; set; }

        public List<VariableInfo> Variables { get; set; } = new List<VariableInfo>();
        public bool VariableEditable { get => false;}
        public bool IsConnected { get; set; }
        public bool IsSimulator { get; set; }
        public string VariableFilePath { get; set; }
        public string ConnectionString { get; set; }
        public string Remark { get; set; }

        [NonSerialized]
        TcpClient tcpClient;
        [NonSerialized]
        public NetworkStream ns;
        [NonSerialized]
        Timer tmr = new Timer();
        int timeTimer = 500;

        int headerLength = 20;
        int bodyLength = 17;

        #endregion

        #region Methods
        public LS_FEnetClass(int plcNumber)
        {
            switch (plcNumber)
            {
                case 1:
                    CommType = CommunicationType.WTC;
                    break;
                case 2:
                    CommType = CommunicationType.FTC;
                    break;
                case 3:
                    CommType = CommunicationType.OTC;
                    break;
            }
        }

        public bool Connect()
        {
            int portNumber = 2004;
            TimeSpan timeout = new TimeSpan(0, 0, 1);
            try
            {
                tcpClient = new TcpClient();
                tcpClient.ReceiveTimeout = 100;
                //tcpClient.Connect(ConnectionString, portNumber);
                var result = tcpClient.BeginConnect(ConnectionString, portNumber, null, null);
                bool success = result.AsyncWaitHandle.WaitOne(timeout, true);

                if (success)
                {
                    tcpClient.EndConnect(result);
                }
                else
                {
                    tcpClient.Close();
                    Debug.Print(CommType + " TCP 연결 실패");
                    return false;
                }

                IsConnected = true;
            }
            catch (SocketException e)
            {
                Debug.Print(CommType + " TCP 연결 실패");
                return false;
            }

            if (tcpClient.Connected)
            {

                tmr = new Timer(timeTimer);
                tmr.Elapsed += Tmr_Elapsed;
                tmr.Enabled = true;

                return true;
            }

            return false;
        }
        private void Tmr_Elapsed(object sender, ElapsedEventArgs e)
        {
            ReadAndWrite();
            SetValues();
            TestSetValue();
        }
        private void TestSetValue()
        {
            int controlon = 0;
            int reset = 0;
            int setvalue = 21;

            foreach(VariableInfo var in Variables)
            {
                if(var.Name == "FTC_HOST_Control_On" && var.RealValue != controlon)
                {
                    var.Set(controlon);
                }
                if(var.Name == "FTC_HOST_Reset" && var.RealValue != reset)
                {
                    var.Set(reset);
                }
                //if(var.Name == "FTC_HOST_SetValue" && var.RealValue != setvalue)
                //{
                //    var.Set(setvalue);
                //}
                if (var.Name == "WTC_HOST_SetValue" && var.RealValue != setvalue)
                {
                    var.Set(setvalue);
                }
            }
        }
        private void SetValues()
        {
            foreach (VariableInfo var in Variables)
            {
                if (var.SpareInt1 == 0)//Bit
                {
                    
                    if (var.Use_SetValue && var.RealValue != var.SetValue)
                    {
                        int setValue = 0;
                        string address = var.SpareString1;
                        string startVarName = var.Name;

                        setValue += (int)(Math.Pow(2, var.SpareInt2) * var.SetValue);

                        var.Use_SetValue = false;

                        foreach (VariableInfo var2 in Variables)
                        {
                            if (var2.SpareString1 == address)
                            {
                                if(var2.Name == startVarName)
                                {
                                    continue;
                                }

                                if (var2.Use_SetValue && var2.RealValue != var2.SetValue)
                                {
                                    setValue += (int)(Math.Pow(2, var2.SpareInt2) * var2.SetValue);
                                    var2.Use_SetValue = false;
                                }
                                else
                                {
                                    setValue += (int)(Math.Pow(2, var2.SpareInt2) * var2.IOValue);
                                }
                            }

                        }

                        SendCommand(CMDType.Write, address, 255, 300, setValue);
                    }
                }
                else if (var.SpareInt1 == 1)//Word
                {
                    if (var.Use_SetValue && var.RealValue != var.SetValue)
                    {
                        SendCommand(CMDType.Write, var.SpareString1, 255, 300, (int)var.SetIOValue);
                        if(var.SpareString1 == "%DW3230")
                        {
                            Debug.Print(var.SetIOValue.ToString());
                        }
                        var.Use_SetValue = false;
                    }
                }
            }
        }
        public void Disconnect()
        {
            IsConnected = false;
            tcpClient.Close();

            tmr.Stop();
            tmr.Enabled = false;
        }

        public bool ExportVariableFile(string filePath)
        {
            string Header = string.Empty;
            string Value = string.Empty;

            Header = "Variable Name,Unit,IO Min,IO Max,Real Min,Real Max,Use Alarm,Alarm Low,Alarm High,Use Warning,Warning Low,Warning High,Use Averaging,Averaging Count,Round,Description,Address,Type,BitIndex\r\n";
            try
            {
                File.WriteAllText(filePath, Header);

                foreach (VariableInfo var in Variables)
                {
                    string type = (var.SpareInt1 == 0) ? "BIT" : "WORD";
                    Value = $"{var.Name},{var.Unit},{var.CalibrationTable[0].IOValue},{var.CalibrationTable[1].IOValue},{var.CalibrationTable[1].RealValue},{var.CalibrationTable[1].RealValue},{var.Use_Alarm},{var.Alarm_Low},{var.Alarm_High},{var.Use_Warning},{var.Warning_Low},{var.Warning_High},{var.Use_Averaging},{var.AveragingCount},{var.Round},{var.Description},{var.SpareString1},{type},{var.SpareInt2.ToString()}";
                    System.IO.File.AppendAllText(filePath, Value + "\r\n");
                }

            }
            catch (Exception)
            {
                systemLog = SystemLog.GetInstance();
                systemLog.AddLog("LS PLC Variable File Export 실패", LogType.Error);
                Debug.Print("LS PLC Variable File Export 에러");
                return false;
            }

            return true;
        }

        public bool MakeVariableList()
        {
            if (File.Exists(VariableFilePath))
            {
                bool result = ImportVariableFile(VariableFilePath);

                return result;
            }
            else
            {
                return false;
            }
        }

        public object ReturnBaseObject()
        {
            return this;
        }

        #region Private
        private bool ImportVariableFile(string filePath)
        {
            VariableInfo varItem;

            string[] Lines = System.IO.File.ReadAllLines(filePath, Encoding.Default);
            List<string> varlist = Lines.ToList<string>();
            varlist.RemoveAt(0);

            Variables?.Clear();

            if (varlist != null)
            {
                try
                {
                    foreach (string LSPLC_Data in varlist)
                    {
                        string[] arrLSPLC_Data = LSPLC_Data.Split(',');

                        string variableName = arrLSPLC_Data[0];
                        string unit = arrLSPLC_Data[1];
                        double ioValue_Min = double.Parse(arrLSPLC_Data[2]);
                        double ioValue_Max = double.Parse(arrLSPLC_Data[3]);
                        double realValue_Min = double.Parse(arrLSPLC_Data[4]);
                        double realValue_Max = double.Parse(arrLSPLC_Data[5]);
                        bool use_Alarm = (arrLSPLC_Data[6] == "TRUE") ? true : false;
                        double alarm_Low = double.Parse(arrLSPLC_Data[7]);
                        double alarm_High = double.Parse(arrLSPLC_Data[8]);
                        bool use_Warning = (arrLSPLC_Data[9] == "TRUE") ? true : false;
                        double warning_Low = double.Parse(arrLSPLC_Data[10]);
                        double warning_High = double.Parse(arrLSPLC_Data[11]);
                        bool use_Averaging = (arrLSPLC_Data[12] == "TRUE") ? true : false;
                        int averaging_Count = int.Parse(arrLSPLC_Data[13]);
                        int round = int.Parse(arrLSPLC_Data[14]);
                        string description = arrLSPLC_Data[15];
                        string address = arrLSPLC_Data[16];
                        string type = arrLSPLC_Data[17];
                        int bitIndex = 0;
                        if (arrLSPLC_Data[18] != "")
                        {
                            bitIndex = int.Parse(arrLSPLC_Data[18]);
                        }

                        varItem = new VariableInfo();
                        varItem.Name = variableName;
                        varItem.CalType = CalibrationType.Table;
                        varItem.CalibrationTable.Add(new ValuePair(ioValue_Min, realValue_Min));
                        varItem.CalibrationTable.Add(new ValuePair(ioValue_Max, realValue_Max));
                        varItem.Unit = unit;
                        varItem.Use_Alarm = use_Alarm;
                        varItem.Alarm_Low = alarm_Low;
                        varItem.Alarm_High = alarm_High;
                        varItem.Use_Warning = use_Warning;
                        varItem.Warning_Low = warning_Low;
                        varItem.Warning_High = warning_High;
                        varItem.Use_Averaging = use_Averaging;
                        varItem.AveragingCount = averaging_Count;
                        varItem.Round = round;
                        varItem.Description = description;
                        varItem.SpareString1 = address;
                        varItem.SpareInt1 = (type == "BIT") ? 0 : 1;
                        varItem.SpareInt2 = bitIndex;
                        Variables.Add(varItem);
                    }
                }
                catch (Exception)
                {
                    systemLog = SystemLog.GetInstance();
                    systemLog.AddLog("LS PLC Variable File Import 실패", LogType.Error);
                    Debug.Print("LS PLC Variable File Import 에러");
                    return false;
                }
            }

            return true;
        }

        private void ReadAndWrite()
        {
            ns = tcpClient.GetStream();
            StreamReader readerStream = new StreamReader(ns);
            //Todo : try catch로 감싸기
            if (ns.CanWrite)
            {
                for (int i = 0; i < Variables.Count; i++)
                {
                    SendCommand(CMDType.Read, Variables[i].SpareString1, i, 0, 0);//Read 명령어는 sendData 사용 안해서 의미 X

                    System.Threading.Thread.Sleep(15);
                }


            }
            if (ns.CanRead)
            {
                byte[] bytes = new byte[2048];
                int bytesRead = ns.Read(bytes, 0, bytes.Length); //비트읽을때 bytesRead[32]가 해당값. On = 1, Off = 0

                if (bytesRead >100)
                {
                    ParsingBytes(bytes);
                }
            }
        }
        private void ParsingBytes(byte[] bytes)
        {
            int startPos = 0;
            int bitPos = 0;
            string strBit = "";

            for (int i = 0; i < bytes.Length - headerLength - bodyLength; i++)
            {
                if (bytes[i] == 0x4c)
                {
                    if (bytes[i + 1] == 0x53) 
                    {
                        startPos = i;
                        int InvokeID = Convert.ToInt32(bytes[i + 14]);
                        if(InvokeID == 255)
                        {
                            continue;
                        }

                        //SpareString1 = Address
                        //SpareInt1 = Type. 0 = Bit, 1 = Word
                        //SpareInt2 = BitIndex
                        if (Variables[InvokeID].SpareInt1 == 0)//타입 = 비트
                        {
                            if (Variables[InvokeID].SpareInt2 < 8)
                            {
                                strBit = Convert.ToString(bytes[i + 32], 2).PadLeft(8, '0');
                                bitPos = (strBit.Length - 1) - Variables[InvokeID].SpareInt2;
                            }
                            else
                            {
                                strBit = Convert.ToString(bytes[i + 33], 2).PadLeft(8, '0');
                                bitPos = (strBit.Length - 1) - (Variables[InvokeID].SpareInt2 % 8);
                            }

                            Variables[InvokeID].IOValue = double.Parse(strBit[bitPos].ToString());
                        }
                        else//타입 = 워드
                        {
                            Variables[InvokeID].IOValue = BytesToDouble(bytes[i + 33], bytes[i + 32]);
                        }


                    }
                }
            }
        }
        public void SendCommand(CMDType RW, string directCmd, int invokeID, int interval, int sendData)//directCmd = PLC Var Address
        {
            string dataType = "";

            string CompanyID = "4C 53 49 53 2D 58 47 54 00 00";//LGIS-XGT NULL NULL
            string PLCinfo = " 00 00";//클라이언트 - 서버로 보내는 경우는 상관없음. 0으로 채움
            string CPUinfo = " A0";//예약 영역. A0 - XGK, A4 - XGI, A8 - XGR
            string srcFreme = " 33";//클라이언트 to 서버 - 33, 서버 to 클라이언트 - 11
            string strinvokeID = " 00 00";//프레임 순서 구별하기 위한 ID. 응답프레임에도 이 번호가 붙어서 응답이 온다
            string Length = " 15 00";//body크기. body길이와 안맞으면 통신 안됨
            string FEnetPos = " 03";//0~7bit - FEnet모듈의 슬롯번호, 4~7bit - FEnet모듈의 베이스번호
            string Reserved2 = " 40";//예약영역. Application header의 바이트썸


            string command = "";
            if (RW == CMDType.Read)
            {
                command = " 54 00";//Read - 54 00, Write - 58 00. 응답은 각각 55 00, 57 00
            }
            else if (RW == CMDType.Write)
            {
                command = " 58 00";//Read - 54 00, Write - 58 00. 응답은 각각 55 00, 57 00
            }
            if (RW == CMDType.Read)
            {
                dataType = " 02 00";//bit - 00, byte - 01, word - 02, 연속 - 14. 자세한건 매뉴얼 참고
            }
            else if (RW == CMDType.Write)
            {
                dataType = " 02 00";//bit - 00, byte - 01, word - 02, 연속 - 14. 자세한건 매뉴얼 참고
            }
            string Reserved = " 00 00";// PC to PLC 경우 00 00. Don't care
            string blockNum = " 01 00";//블록의 수. 읽고자 하는 변수 수라고 보면 됨. 1 block = 1 varLength + 1 var
            string varLength = "";//직접변수의 길이. 최대 16자. 가능하면 07 00으로 둔다.
            //string directCmd = "%DW1000";
            string var = StringtoHex(directCmd);//M메모리 비트로 접근 명령어 : %MX0000, D메모리 워드 접근 명령어 : %DW0000. 0000부분은 4자리 맞춰준다.
            varLength = " " + directCmd.Length.ToString("X2") + " 00";
            string addData = "";

            if (RW == CMDType.Read)
            {
                addData = "";
            }
            else if (RW == CMDType.Write)
            {
                
                string data =sendData.ToString("X4");//확인 필요. 20을 보낸다고 했을때 16진수인 14로 보내는지 아니면 2,0을 따로 떼서 2에 해당하는 아스키 32, 0에 해당하는 아스키 30 이런식으로 보내는지 확인이 필요. 아스키면 그대로 사용하면 되고 아니면 stringtohex함수 대신에 양식에 맞게 16진수 스트링을 리턴하는 함수 필요
                data = data.Insert(2, " ");
                string[] temp = data.Split(' ');
                string inverse = temp[1] + " " + temp[0];
                int byteLength = data.Split(' ').Length;
                string dataLength = " " + byteLength.ToString("X2") + " 00 ";

                addData = dataLength + inverse;

                //비트 입력 용. 위의 elif RW = Write일때 dataType을 00 00으로 변경
                //Console.Write("데이터 :");
                //addData = " 01 00 "+int.Parse(Console.ReadLine()).ToString("X2");
                //addData = " 01 00 01 00";
            }

            string body = command + dataType + Reserved + blockNum + varLength + var + addData;

            string[] arrForBodyLength = body.Split(' ');
            Length = " " + (arrForBodyLength.Length - 1).ToString("X2") + " 00";
            strinvokeID = " " + invokeID.ToString("X2") + " 00";

            string header = CompanyID + PLCinfo + CPUinfo + srcFreme + strinvokeID + Length + FEnetPos + Reserved2;


            string Frame = header + body;
            string[] hexValuesSplit = Frame.Split(' ');

            byte[] bytesCMD = new byte[hexValuesSplit.Length];

            for (int i = 0; i < hexValuesSplit.Length; i++)
            {
                int value = Convert.ToInt32(hexValuesSplit[i], 16);
                bytesCMD[i] = Convert.ToByte(value);
            }


            ns.Write(bytesCMD, 0, bytesCMD.Length);

            //if (RW == CMDType.Write)//Write로 Pulse신호 주기위한 부분.
            //{
            //    System.Threading.Thread.Sleep(interval);

            //    addData = " 01 00 00 00";
            //    body = command + dataType + Reserved + blockNum + varLength + var + addData;
            //    header = CompanyID + PLCinfo + CPUinfo + srcFreme + strinvokeID + Length + FEnetPos + Reserved2;
            //    Frame = header + body;

            //    hexValuesSplit = Frame.Split(' ');

            //    bytesCMD = new byte[hexValuesSplit.Length];

            //    for (int i = 0; i < hexValuesSplit.Length; i++)
            //    {
            //        int value = Convert.ToInt32(hexValuesSplit[i], 16);
            //        bytesCMD[i] = Convert.ToByte(value);
            //    }

            //    ns.Write(bytesCMD, 0, bytesCMD.Length);
            //}
        }
        private double BytesToDouble(byte byte1, byte byte2)
        {
            byte[] tempbytes = new byte[2];
            tempbytes[0] = byte2;
            tempbytes[1] = byte1;

            int result = BitConverter.ToUInt16(tempbytes, 0);
            return result;
        }
        private string StringtoHex(string hexstr)
        {
            char[] charValues = hexstr.ToCharArray();
            string hexOutput = "";
            foreach (char eachChar in charValues)
            {
                int value = Convert.ToInt32(eachChar);
                hexOutput += String.Format(" {0:X}", value);
            }

            return hexOutput;
        }
        #endregion//Private
        #endregion//Methods
    }
}
