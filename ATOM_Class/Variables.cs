﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using org.mariuszgromada.math.mxparser;
using System.ComponentModel;

namespace ATOM_Class
{
    public enum UseFor
    {
        HMI,
        DAQ
    }
    public enum IOType
    {
        AI,
        AO,
        DI,
        DO,
        None
    }
    public enum CalibrationType
    {
        Value,
        Table,
        Formula,
        Mapping,
        None
    }
    public enum AlarmState
    {
        Normal = 0,
        Warning = 1,
        Alarm = 2
    }
    public enum AlarmMethod
    {
        None = 0,
        Idle = 1,
        Stop = 2
    }
    [Serializable]
    public class VariableInfo
    {
        #region Member
        //Default
        private string name;
        private string unit;
        
        private double ioValue = 0.0;
        private double realValue = 0.0;
        private bool use_SetValue = false;
        private double setValue = 0.0;
        private string description;

        private string spareString1 = string.Empty;
        private string spareString2 = string.Empty;

        private int spareInt1 = 0;
        private int spareInt2 = 0;

        private bool use_Alarm;
        private double alarm_Low;
        private double alarm_High;
        private bool use_Warning;
        private double warning_Low;
        private double warning_High;
        private int alarmAllowableTime = 5;
        private TimeSpan alarmStartTime;
        private TimeSpan alarmElapsedTime;
        private bool alarmAlreadyError = false;
        private TimeSpan warningStartTime;
        private TimeSpan warningElapsedTime;
        private bool warningAlreadyError = false;

        private bool use_Averaging = false;
        private int averagingCount = 5;
        private List<double> IOValueList = new List<double>();
        private int round = 1;

        CalibrationType calType = CalibrationType.Value;
        IOType ioType = IOType.None;

        //Calibration - Value
        private double offset = 0;
        private double factor = 1;

        //Calibration - Formula
        private string formula;

        private bool _Checked = false; //Variable Assign 용도로만 쓰임.


        [CategoryAttribute("Basic"), DescriptionAttribute("변수 이름")]
        public string Name { get => name; set => name = value; }
        [CategoryAttribute("Basic"), DescriptionAttribute("변수 단위")]
        public string Unit { get => unit; set => unit = value; }
        [CategoryAttribute("Basic"), DescriptionAttribute("IO 타입"), ReadOnly(true)]
        public IOType IOType { get => ioType; set => ioType = value; }
        [CategoryAttribute("Basic"), DescriptionAttribute("설명")]
        public string Description { get => description; set => description = value; }
        [CategoryAttribute("Alarm"), DescriptionAttribute("알람 사용 여부 체크"), DisplayName("Use Alarm")]
        public bool Use_Alarm { get => use_Alarm; set => use_Alarm = value; }
        [CategoryAttribute("Alarm"), DescriptionAttribute("실제 값이 해당 값보다 낮아지면 알람 상태가 됩니다."), DisplayName("Alarm Low")]
        public double Alarm_Low { get => alarm_Low; set => alarm_Low = value; }
        [CategoryAttribute("Alarm"), DescriptionAttribute("실제 값이 해당 값보다 높아지면 알람 상태가 됩니다."), DisplayName("Alarm High")]
        public double Alarm_High { get => alarm_High; set => alarm_High = value; }
        [CategoryAttribute("Alarm"), DescriptionAttribute("경고 사용 여부 체크"), DisplayName("Use Warning")]
        public bool Use_Warning { get => use_Warning; set => use_Warning = value; }
        [CategoryAttribute("Alarm"), DescriptionAttribute("실제 값이 해당 값보다 낮아지면 경고 상태가 됩니다."), DisplayName("Warning Low")]
        public double Warning_Low { get => warning_Low; set => warning_Low = value; }
        [CategoryAttribute("Alarm"), DescriptionAttribute("실제 값이 해당 값보다 높아지면 경고 상태가 됩니다."), DisplayName("Warning High")]
        public double Warning_High { get => warning_High; set => warning_High = value; }
        [CategoryAttribute("Alarm"), DescriptionAttribute("실제 값이 알람 또는 경고값 범위에 포함되고 해당 시간만큼 경과했을 때 상태가 변합니다."), DisplayName("Alarm Allowable Time")]
        public int AlarmAllowableTime { get => alarmAllowableTime; set => alarmAllowableTime = value; }
        [Browsable(false)]
        public TimeSpan AlarmElapsedTime { get => alarmElapsedTime; set => alarmElapsedTime = value; }
        [Browsable(false)]
        public bool AlarmAlreadyError { get => alarmAlreadyError; set => alarmAlreadyError = value; }
        [Browsable(false)]
        public TimeSpan AlarmStartTime { get => alarmStartTime; set => alarmStartTime = value; }
        [Browsable(false)]
        public TimeSpan WarningStartTime { get => warningStartTime; set => warningStartTime = value; }
        [Browsable(false)]
        public TimeSpan WarningElapsedTime { get => warningElapsedTime; set => warningElapsedTime = value; }
        [Browsable(false)]
        public bool WarningAlreadyError { get => warningAlreadyError; set => warningAlreadyError = value; }

        [CategoryAttribute("Value Processing"), DescriptionAttribute("평균값 계산 기능 사용여부를 체크합니다."), DisplayName("Use Averaging")]
        public bool Use_Averaging { get => use_Averaging; set => use_Averaging = value; }
        [CategoryAttribute("Value Processing"), DescriptionAttribute("최근 몇 개의 값을 평균값 계산에 사용할지를 정합니다."), DisplayName("Averaging Count")]
        public int AveragingCount { get => averagingCount; set => averagingCount = value; }
        [CategoryAttribute("Value Processing"), DescriptionAttribute("실제 값이 소수점 몇 번째 자리수까지 표시될지를 정합니다.")]
        public int Round { get => round; set => round = value; }
        [CategoryAttribute("Value Processing"), DescriptionAttribute("IO 값이 실제 값으로 환산되는 방식을 정합니다.\nValue : 실제 값 : Factor * IO 값 + Offset\nTable : Calibration Table을 이용한 비례식으로 계산\nFormula : IO 값에 관계없이 Formula에 적힌 식대로 실제 값 환산\nMapping : 설정한 변수의 실제값을 그대로 가져옴"), DisplayName("Calibration Type")]
        public CalibrationType CalType { get => calType; set => calType = value; }
        [CategoryAttribute("Calibration - Value"), DescriptionAttribute("Calibration Type - Value에 사용되는 값. \nValue : 실제 값 : Factor * IO 값 + Offset")]
        public double Factor { get => factor; set => factor = value; }
        [CategoryAttribute("Calibration - Value"), DescriptionAttribute("Calibration Type - Value에 사용되는 값. \nValue : 실제 값 : Factor * IO 값 + Offset")]
        public double Offset { get => offset; set => offset = value; }
        [CategoryAttribute("Calibration - Formula"), DescriptionAttribute("")]
        public string Formula { get => formula; set => formula = value; }
        [CategoryAttribute("Value"), DescriptionAttribute("IO 값"), DisplayName("IO Value")]
        public double IOValue
        {
            get
            {
                if (!use_Averaging || IOValueList.Count == 0)
                    return ioValue;
                else
                    return IOValueList[0];
            }
            set
            {
                if (!use_Averaging)
                    ioValue = value;
                else
                {
                    try
                    {
                        lock (IOValueList)
                        {
                            if (IOValueList.Count > AveragingCount)
                                IOValueList.RemoveAt(IOValueList.Count - 1);
                            IOValueList.Insert(0, value);
                        }
                    }
                    catch (Exception) { }
                }
            }
        }
        [CategoryAttribute("Value"), DescriptionAttribute("실제 값"), DisplayName("Real Value")]
        public double RealValue
        {
            get
            {
                double sum = 0;
                if (!use_Averaging || IOValueList.Count == 0)
                    return Math.Round(GetRealValue(IOValue), Round);
                else
                {
                    int maxNum = 0;
                    lock (IOValueList)
                    {
                        maxNum = (IOValueList.Count > AveragingCount ? AveragingCount : IOValueList.Count);

                        for (int i = 0; i < maxNum; i++)
                        {
                            sum += GetRealValue(IOValueList[i]);
                        }
                    }

                    return Math.Round(sum / maxNum, Round);
                }
            }
            set
            {
                RealValue = value;
            }
        }
        [Browsable(false)]
        public bool Checked { get => _Checked; set => _Checked = value; }

        [Browsable(false)]
        public double SetValue { get => setValue; set => setValue = value; }
        [Browsable(false)]
        public double SetIOValue
        {
            get
            {
                return GetSetIOValue(SetValue);
            }
        }
        [Browsable(false)]
        public bool Use_SetValue { get => use_SetValue; set => use_SetValue = value; }
        [Browsable(false)]
        public string SpareString1 { get => spareString1; set => spareString1 = value; }
        [Browsable(false)]
        public string SpareString2 { get => spareString2; set => spareString2 = value; }
        [Browsable(false)]
        public int SpareInt1 { get => spareInt1; set => spareInt1 = value; }
        [Browsable(false)]
        public int SpareInt2 { get => spareInt2; set => spareInt2 = value; }

        public List<ValuePair> CalibrationTable = new List<ValuePair>();

        [Browsable(false)]
        public AlarmState alarmState;
        [Browsable(false)]
        public AlarmMethod alarmMethod;
        #endregion

        #region Method

        public VariableInfo()
        {

        }
        public void Set(double setValue)
        {
            use_SetValue = true;
            this.setValue = setValue;
        }
        public double GetSetIOValue(double setValue)
        {
            double IOValue = 0;

            if (CalType == CalibrationType.Value)
            {
                IOValue = (setValue - Offset) / Factor;
            }
            else if (CalType == CalibrationType.Table)
            {
                if (CalibrationTable.Count == 0)
                {
                    IOValue = setValue;
                }
                else if (CalibrationTable.Count == 1)
                {
                    IOValue = setValue * (double)CalibrationTable[0].IOValue / (double)CalibrationTable[0].RealValue;
                }
                else
                {
                    int i;
                    bool flag = false;
                    for (i = 0; i < CalibrationTable.Count - 1; i++)
                    {
                        if (setValue >= CalibrationTable[i].RealValue && setValue <= CalibrationTable[i + 1].RealValue)
                        {
                            flag = true;
                            break;
                        }
                    }
                    if (flag == false)
                    {
                        return 0;//Module Range 설정 오류. Mapping 시 자동으로 Module IO 설정하는 여부는 추후 고민. 현재는 파일 설정을 딱 맞춰줘야 함.
                    }
                    
                    IOValue = (setValue - (double)CalibrationTable[i + 1].RealValue) * ((double)CalibrationTable[i].IOValue - (double)CalibrationTable[i + 1].IOValue) / ((double)CalibrationTable[i].RealValue - (double)CalibrationTable[i + 1].RealValue) + (double)CalibrationTable[i + 1].IOValue;
                }
            }
            else if (CalType == CalibrationType.None)
            {
                IOValue = SetValue;
            }

            return IOValue;
        }
        public double GetRealValue(double IOValue)
        {
            if(this.name == "토크_Hz")
            {
                CalType = CalibrationType.None;
                return (IOValue / 100 - 60076) * 1000 / 30000;
            }
            else if (this.name == "스피드_Hz")
            {
                CalType = CalibrationType.None;
                return IOValue / 100 * 60 / 2048;
            }
            double RValue = 0;

            if (CalType == CalibrationType.Value)
            {
                RValue = IOValue * Factor + Offset;
            }
            else if (CalType == CalibrationType.Table)
            {
                if (CalibrationTable.Count == 0)
                {
                    RValue = IOValue;
                }
                else if (CalibrationTable.Count == 1)
                {
                    RValue = (double)CalibrationTable[0].RealValue / (double)CalibrationTable[0].IOValue * (double)IOValue; //Physical Value = d / c * Raw.
                }
                else
                {
                    int i;
                    bool flag = false;
                    for (i = 0; i < CalibrationTable.Count - 1; i++)
                    {
                        if (IOValue >= CalibrationTable[i].IOValue && IOValue <= CalibrationTable[i + 1].IOValue)
                        {
                            flag = true;
                            break;
                        }
                    }
                    if (flag == false)
                    {
                        return 0;//Module Range 설정 오류. Mapping 시 자동으로 Module IO 설정하는 여부는 추후 고민. 현재는 파일 설정을 딱 맞춰줘야 함.
                    }

                    RValue = ((double)CalibrationTable[i].RealValue - (double)CalibrationTable[i + 1].RealValue) / ((double)CalibrationTable[i].IOValue - (double)CalibrationTable[i + 1].IOValue) * ((double)IOValue - (double)CalibrationTable[i + 1].IOValue) + (double)CalibrationTable[i + 1].RealValue;
                }
            }
            else if (CalType == CalibrationType.Formula)
            {
                string strFormula = VariableToRealValue(Formula);
                Expression e = new Expression(strFormula);
                try
                {
                    RValue = e.calculate();
                }
                catch (Exception)
                {

                }
            }
            else if (CalType == CalibrationType.None)
            {
                RValue = IOValue;
            }

            return RValue;
        }

        public static string VariableToRealValue(string Formula)
        {
            if(Formula == null)
            {
                return string.Empty;
            }

            string strFormula = Formula.Replace(" ", "");
            CommunicationConnector communicationConnector = CommunicationConnector.GetInstance();
            char[] chars = new char[] { '+', '-', '*', '/', '^', '(', ')' };

            string[] temp = strFormula.Split(chars);
            List<Tuple<string, string>> pairs = new List<Tuple<string, string>>();

            for (int j = 0; j < temp.Length; j++)
            {
                if(temp[j] == string.Empty)//위에 chars에 해당하는 기호가 string.Empty로 변환된 경우 패스
                {
                    continue;
                }
                VariableInfo var = communicationConnector.FindVariable(temp[j]);

                if (var != null)
                {
                    pairs.Add(new Tuple<string, string>(temp[j], var.RealValue.ToString()));
                }
                else
                {
                    double result;
                    if (double.TryParse(temp[j], out result))
                    {
                        pairs.Add(new Tuple<string, string>(temp[j], result.ToString()));
                    }
                    else
                    {
                        pairs.Add(new Tuple<string, string>(temp[j], "0"));
                    }
                }
            }

            for (int r = 0; r < pairs.Count; r++)
            {
                strFormula = strFormula.Replace(pairs[r].Item1, pairs[r].Item2);
            }

            strFormula = strFormula.Replace("+-", "-");

            return strFormula;
        }
        #endregion
    }

    [Serializable]
    public class ValuePair
    {
        private double ioValue = 0;
        private double realValue = 0.0;
        public ValuePair()
        {
        }

        public ValuePair(double IOValue, double RealValue)
        {
            this.IOValue = IOValue;
            this.RealValue = RealValue;
        }

        public double IOValue { get => ioValue; set => ioValue = value; }
        public double RealValue { get => realValue; set => realValue = value; }
    }
}
