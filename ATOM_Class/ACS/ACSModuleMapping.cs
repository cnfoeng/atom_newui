﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace ATOM_Class
{
    [Serializable]
    public class ACSModuleInfo
    {
        public String ProductName;
        public String ProductDesc;
        public int VendorID;
        public int ProductID;
        public IOType IO;
        public UseFor UseFor;
        public int OffsetBase;
        public int SlaveNo;
        public int ChannelEa;
        public long ACSIOMin;
        public long ACSIOMax;
        public long ModuleIOMin;
        public long ModuleIOMax;
        public string Unit;
        public ACSModuleInfo()
        { }

        public ACSModuleInfo(String ProductName, String ProductDesc, int VendorID, int ProductID, IOType IO, int OffsetBase, int SlaveNo, int ChannelEa, long ACSIOMin, long ACSIOMax, long ModuleIOMin, long ModuleIOMax, string Unit)
        {
            
            this.ProductName = ProductName;
            this.ProductDesc = ProductDesc;
            this.VendorID = VendorID;
            this.ProductID = ProductID;
            this.IO = IO;
            this.OffsetBase = OffsetBase;
            this.SlaveNo = SlaveNo;
            this.ChannelEa = ChannelEa;
            this.ACSIOMin = ACSIOMin;
            this.ACSIOMax = ACSIOMax;
            this.ModuleIOMin = ModuleIOMin;
            this.ModuleIOMax = ModuleIOMax;
            this.Unit = Unit;
        }
    }
    [Serializable]
    public class ACSModules : List<ACSModuleInfo>
    {
        public int DigitalOutputChannelNumber
        {
            get
            {
                int result = 0;

                foreach(ACSModuleInfo module in this)
                {
                    if(module.UseFor == UseFor.DAQ && module.IO == IOType.DO)
                    {
                        result += module.ChannelEa;        
                    }
                }

                return result;
            }
        }
        public DataTable ReturnDataTableByType(IOType type, double[] arrValues)
        {
            DataTable dataTable = new DataTable("ACS " + type.ToString() + "Module Table");
            dataTable.Columns.Add("Channel");
            dataTable.Columns.Add("ModuleName");
            dataTable.Columns.Add("ModuleChannel");
            dataTable.Columns.Add("Value");

            int valueIndex = 0;

            for (int i = 0; i < this.Count; i++)
            {
                if (this[i].UseFor == UseFor.DAQ && this[i].IO == type)
                {
                    for (int j = 1; j <= this[i].ChannelEa; j++)
                    {
                        if (arrValues != null)
                        {
                            dataTable.Rows.Add(type.ToString() + " " + (valueIndex + 1), this[i].ProductDesc, j, string.Format("{0:F4}", arrValues[valueIndex]) + " " + this[i].Unit);
                            valueIndex++;
                        }
                    }
                }
            }

            return dataTable;
        }

        public ACSModuleInfo ReturnModuleFromChannelIndex(IOType type, int index)
        {
            int moduleIndex = 0;
            
            foreach(ACSModuleInfo module in this)
            {
                if(module.IO == type)
                {
                    if(index > module.ChannelEa)
                    {
                        moduleIndex++;
                        index = index - module.ChannelEa;
                        continue;
                    }

                    break;

                }
                else
                {
                    moduleIndex++;
                }
            }


            if (moduleIndex >= this.Count)
            {
                return null;
            }

            return this[moduleIndex];
        }
    }
    [Serializable]
    public class ACSMappingList : List<ACSMapping>
    {
        public void Init(List<VariableInfo> variables)
        {
            this.Clear();
            for (int i = 0; i < variables.Count; i++)
            {
                this.Add(new ACSMapping(variables[i].Name, variables[i].IOType));
            }
        }


        public DataTable ReturnDataTableByType(IOType type)
        {
            DataTable dataTable = new DataTable("ACSMapping" + type.ToString() + "Table");
            dataTable.Columns.Add("VariableName");
            dataTable.Columns.Add("AssignedChannel");

            for (int i = 0; i < this.Count; i++)
            {
                if (this[i].Type == type)
                {
                    if (this[i].ModuleChannelNumber == -1)//미할당
                    {
                        dataTable.Rows.Add(this[i].VariableName, null);
                    }
                    else//할당
                    {
                        dataTable.Rows.Add(this[i].VariableName, type.ToString() + " " + (this[i].ModuleChannelNumber + 1));
                    }
                }
            }

            return dataTable;

        }

        public void AssignVariable(string varName, int moduleChannelNumber, IOType type)
        {

            for (int i = 0; i < this.Count; i++)
            {
                if (this[i].Type != type)
                {
                    continue;
                }

                if (this[i].VariableName == varName)
                {
                    this[i].ModuleChannelNumber = moduleChannelNumber;
                }

            }
        }
        public void AssignAllMappingsSequentially()
        {
            int indexAI = 0;
            int indexAO = 0;
            int indexDI = 0;
            int indexDO = 0;

            for (int i = 0; i < this.Count; i++)
            {
                if (this[i].Type == IOType.AI)
                {
                    this[i].ModuleChannelNumber = indexAI;
                    indexAI++;
                }
                else if (this[i].Type == IOType.AO)
                {
                    this[i].ModuleChannelNumber = indexAO;
                    indexAO++;
                }
                else if (this[i].Type == IOType.DI)
                {
                    this[i].ModuleChannelNumber = indexDI;
                    indexDI++;
                }
                else
                {
                    this[i].ModuleChannelNumber = indexDO;
                    indexDO++;
                }
            }
        }
        public void EmptyAllMappings()
        {
            for (int i = 0; i < this.Count; i++)
            {
                this[i].ModuleChannelNumber = -1;
            }
        }

        public int ReturnModuleChannelNumber(string varName)
        {
            foreach (ACSMapping mapping in this)
            {
                if (mapping.VariableName == varName)
                {
                    return mapping.ModuleChannelNumber;
                }
            }

            return -1;
        }

        public IOType ReturnIOType(string varName)
        {
            foreach (ACSMapping mapping in this)
            {
                if (mapping.VariableName == varName)
                {
                    return mapping.Type;
                }
            }

            return IOType.None;
        }
    }

    [Serializable]
    public class ACSMapping
    {
        string variableName = string.Empty;
        int channelNumber = -1;
        IOType type = IOType.None;

        public string VariableName { get => variableName; set => variableName = value; }
        public int ModuleChannelNumber { get => channelNumber; set => channelNumber = value; }
        public IOType Type { get => type; set => type = value; }

        public ACSMapping(string variableName, IOType type)
        {
            this.variableName = variableName;
            this.type = type;
        }

    }
}

