﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.IO;
using System.Timers;
using System.Collections;
using System.Data;
using ACS.SPiiPlusNET;

namespace ATOM_Class
{
    [Serializable]
    public class ACSConnector : ICommunication
    {
        [NonSerialized]
        SystemLog systemLog;

        [NonSerialized]
        ACSCommunication ACS;


        public List<VariableInfo> Variables { get; set; } = new List<VariableInfo>();
        public bool VariableEditable { get => true; }
        public ACSMappingList MappingList = new ACSMappingList();

        public ACSModules Modules = new ACSModules();

        public CommunicationType CommType { get => CommunicationType.ACS; }
        public bool IsConnected { get; set; } = false;
        public bool IsSimulator { get; set; } = false;

        public string VariableFilePath { get; set; } = string.Empty;
        public string ConnectionString { get; set; } = string.Empty;
        public string Remark { get; set; } = string.Empty;

        private string checkModuleChange = string.Empty;

        [NonSerialized]
        Timer tmrMapping;

        [NonSerialized]
        Timer tmrSetValue;

        int mATTimer = 200;

        public ACSConnector()
        {

        }

        public object ReturnBaseObject()
        {
            return this;
        }

        public bool Connect()
        {
            ACS = new ACSCommunication();
            IsConnected = ACS.Connect(IsSimulator);

            systemLog = SystemLog.GetInstance();

            if (IsConnected == false)
            {
                systemLog.AddLog("ACS 연결 실패", LogType.Error);
                return false;
            }

            bool useHMI = (Remark == "WithHMI") ? true : false;
            Modules = ACS.SystemSetup(useHMI);
            string strModules = ACS.ReturnModuleNames();

            bool bChangeModule = false;
            if (checkModuleChange != strModules)
            {
                bChangeModule = true;
                checkModuleChange = strModules;
            }


            if (IsConnected && bChangeModule == false)
            {
                systemLog.AddLog("ACS 연결 성공", LogType.OK);
            }
            else if (IsConnected && bChangeModule == true)
            {
                systemLog.AddLog("ACS 연결 성공", LogType.OK);
                systemLog.AddLog("ACS 모듈 정보가 변경 되었습니다. 맵핑이 초기화됩니다.", LogType.Warning);

                //Todo : 모듈 맵핑 초기화, 자동 순차할당 로직 추가
            }

            tmrMapping = new Timer(mATTimer);

            tmrMapping.Elapsed += OnRun_Mapping;
            tmrMapping.Enabled = true;

            tmrSetValue = new Timer(mATTimer);

            tmrSetValue.Elapsed += OnRun_SetValue;
            tmrSetValue.Enabled = true;

            return IsConnected;
        }
        private void OnRun_Mapping(Object source, ElapsedEventArgs e)
        {
            CommunicationConnector communicationConnector = CommunicationConnector.GetInstance();

            foreach (ACSMapping mapping in MappingList)
            {
                VariableInfo var = communicationConnector.FindVariable(mapping.VariableName);

                if(var == null)//연결하고 중간에 ACS Connection이 삭제된 경우
                {
                    this.Disconnect();
                    continue;
                }

                if (ACS.ErrorCheck())
                {
                    this.Disconnect();
                }

                if (mapping.ModuleChannelNumber == -1)//Unassigned
                {
                    var.IOValue = 0;
                }
                else
                {
                    if (ACS.ReturnRealValueArray(mapping.Type) != null)
                        var.IOValue = ACS.ReturnRealValueArray(mapping.Type)[mapping.ModuleChannelNumber];
                }
            }

        }

        private void OnRun_SetValue(Object source, ElapsedEventArgs e)
        {

            int DOLoopCount = Modules.DigitalOutputChannelNumber / 4;
            string[] DOVarNames = new string[Modules.DigitalOutputChannelNumber];
            bool[] DOUseSetValue = new bool[Modules.DigitalOutputChannelNumber];
            int[] DOSetValue = new int[Modules.DigitalOutputChannelNumber];

            foreach (ACSMapping mapping in MappingList)
            {
                if (mapping.Type == IOType.DO)
                {
                    if (mapping.ModuleChannelNumber != -1)
                    {
                        DOVarNames[mapping.ModuleChannelNumber] = mapping.VariableName;
                    }
                }
            }

            foreach (VariableInfo var in Variables)
            {
                if (var.IOType == IOType.AI || var.IOType == IOType.DI)
                {
                    continue;
                }
                else if (var.IOType == IOType.AO)
                {

                    if (var.Use_SetValue && var.RealValue != var.SetValue)
                    {
                        int channelNum = MappingList.ReturnModuleChannelNumber(var.Name);
                        IOType type = MappingList.ReturnIOType(var.Name);
                        double ioValue = var.SetIOValue;//Voltage

                        ACSModuleInfo module = Modules.ReturnModuleFromChannelIndex(type, channelNum);

                        double slope = (double)(module.ACSIOMax - module.ACSIOMin) / (double)(module.ModuleIOMax - module.ModuleIOMin);
                        double intercept = (double)module.ACSIOMin - (slope * (double)module.ModuleIOMin);

                        double ACSSetValue = slope * ioValue + intercept;

                        ACS.SetValue(type, channelNum, ACSSetValue);// Setvalue 제대로 들어가나 테스트, DO 어떻게 넣을건지 생각하기.

                        var.Use_SetValue = false;
                    }
                }
                else if (var.IOType == IOType.DO)
                {
                    //기본적으로 8개가 변수 하나로 묶임. (0)(0) = 255 면 첫번째모듈 왼쪽라인 8비트 전부 온. (0)(1) = 255면 첫번째모듈 두번째라인 8비트 전부 온

                    //setvalue의 모듈 대입값을 만들고 read로 값을 비교한 후, DO의 모듈번호, setvalue 조합으로 var.SetValue();에 들어가는 모듈 대입값을 만들어야됨. 



                    for (int i = 0; i < DOVarNames.Length; i++)
                    {

                        if (DOVarNames[i] == var.Name)
                        {
                            DOUseSetValue[i] = var.Use_SetValue;

                            if (var.Use_SetValue)
                            {
                                DOSetValue[i] = (var.SetValue > 0) ? 1 : 0;
                                var.Use_SetValue = false;
                            }
                            else
                            {
                                DOSetValue[i] = (var.RealValue > 0) ? 1 : 0;
                            }
                        }
                    }
                }
            }

            int num = -1;

            for (int j = 0; j < DOUseSetValue.Length; j++)
            {
                if (j < num)
                {
                    continue;
                }

                if (DOUseSetValue[j] == true)
                {
                    int quotient = j / 8;
                    int startIndex = 8 * quotient - 1;
                    num = startIndex + 8;

                    double setValue = BoolArrayToDouble(startIndex + 1, DOSetValue);

                    ACS.SetValue(IOType.DO, quotient, setValue);
                }
            }
        }
        private double BoolArrayToDouble(int startIndex, int[] array)
        {
            double sum = 0;

            //sum = 1 * array[startIndex] + 2 * array[startIndex + 1] + 2 * 2 * array[startIndex + 2] + 2 * 2 * 2 * array[startIndex + 3];

            for (int i = startIndex; i < startIndex + 8; i++)
            {
                sum += Math.Pow(2, i - startIndex) * array[i];
            }

            return sum;
        }
        public void Disconnect()
        {
            if (ACS.Disconnect())
            {
                IsConnected = false;

                tmrMapping.Enabled = false;
                tmrSetValue.Enabled = false;
                //TODO : 맵핑타이머 종료, 값 초기화.
            }
        }
        public void VariableModuleIOMapping()
        {
            for (int i = 0; i < Variables.Count; i++)
            {
                ACSModuleInfo module = Modules.ReturnModuleFromChannelIndex(Variables[i].IOType, MappingList.ReturnModuleChannelNumber(Variables[i].Name));
                if (module != null)
                {
                    Variables[i].CalibrationTable[0].IOValue = module.ModuleIOMin;
                    Variables[i].CalibrationTable[1].IOValue = module.ModuleIOMax;
                }
            }
        }
        public bool MakeVariableList()
        {
            if (File.Exists(VariableFilePath))
            {
                bool result = ImportVariableFile(VariableFilePath);
                MappingList.Init(Variables);
                return result;
            }
            else
            {
                return false;
            }
        }

        public bool ImportVariableFile(string filePath)
        {
            VariableInfo varItem;

            string[] Lines = System.IO.File.ReadAllLines(filePath, Encoding.Default);
            List<string> varlist = Lines.ToList<string>();
            varlist.RemoveAt(0);

            Variables?.Clear();

            if (varlist != null)
            {
                try
                {
                    foreach (string ACS_Data in varlist)
                    {
                        string[] arrACS_Data = ACS_Data.Split(',');

                        string variableName = arrACS_Data[0];
                        string iotype = arrACS_Data[1];
                        string unit = arrACS_Data[2];
                        double ioValue_Min = 0.0;
                        double ioValue_Max = 0.0;
                        double realValue_Min = double.Parse(arrACS_Data[3]);
                        double realValue_Max = double.Parse(arrACS_Data[4]);
                        bool use_Alarm = (arrACS_Data[5] == "TRUE") ? true : false;
                        double alarm_Low = double.Parse(arrACS_Data[6]);
                        double alarm_High = double.Parse(arrACS_Data[7]);
                        bool use_Warning = (arrACS_Data[8] == "TRUE") ? true : false;
                        double warning_Low = double.Parse(arrACS_Data[9]);
                        double warning_High = double.Parse(arrACS_Data[10]);
                        bool use_Averaging = (arrACS_Data[11] == "TRUE") ? true : false;
                        int averaging_Count = int.Parse(arrACS_Data[12]);
                        int round = int.Parse(arrACS_Data[13]);
                        string description = arrACS_Data[14];


                        varItem = new VariableInfo();
                        varItem.Name = variableName;
                        if (iotype == "DI")
                        {
                            varItem.IOType = IOType.DI;
                            ioValue_Min = 0;
                            ioValue_Max = 1;
                        }
                        else if (iotype == "DO")
                        {
                            varItem.IOType = IOType.DO;
                            ioValue_Min = 0;
                            ioValue_Max = 1;
                        }
                        else if (iotype == "AI")
                        {
                            varItem.IOType = IOType.AI;
                            ioValue_Min = 0;
                            ioValue_Max = 10;
                        }
                        else if (iotype == "AO")
                        {
                            varItem.IOType = IOType.AO;
                            ioValue_Min = 0;
                            ioValue_Max = 10;
                        }
                        varItem.Unit = unit;
                        varItem.CalType = CalibrationType.Table;
                        varItem.CalibrationTable.Add(new ValuePair(ioValue_Min, realValue_Min));
                        varItem.CalibrationTable.Add(new ValuePair(ioValue_Max, realValue_Max));
                        varItem.Use_Alarm = use_Alarm;
                        varItem.Alarm_Low = alarm_Low;
                        varItem.Alarm_High = alarm_High;
                        varItem.Use_Warning = use_Warning;
                        varItem.Warning_Low = warning_Low;
                        varItem.Warning_High = warning_High;
                        varItem.Use_Averaging = use_Averaging;
                        varItem.AveragingCount = averaging_Count;
                        varItem.Round = round;
                        varItem.Description = description;

                        Variables.Add(varItem);
                    }
                }
                catch (Exception)
                {
                    systemLog = SystemLog.GetInstance();
                    systemLog.AddLog("ACS Variable File Import 실패", LogType.Error);
                    Debug.Print("ACS Variable File Import 에러");
                    return false;
                }
            }

            return true;
        }

        public bool ExportVariableFile(string filePath)
        {
            string Header = string.Empty;
            string Value = string.Empty;

            Header = "Variable Name,IO Type,Unit,Real Value Min,Real Value Max,Use Alarm,Alarm Low,Alarm High,Use Warning,Warning Low,Warning High,Use Averaging,Averaging Count,Round,Description\r\n";
            try
            {
                File.WriteAllText(filePath, Header);

                foreach (VariableInfo var in Variables)
                {
                    Value = $"{var.Name},{var.IOType.ToString()},{var.Unit},{var.CalibrationTable[0].RealValue},{var.CalibrationTable[1].RealValue},{var.Use_Alarm},{var.Alarm_Low},{var.Alarm_High},{var.Use_Warning},{var.Warning_Low},{var.Warning_High},{var.Use_Averaging},{var.AveragingCount},{var.Round},{var.Description}";
                    System.IO.File.AppendAllText(filePath, Value + "\r\n");
                }

            }
            catch (Exception)
            {
                systemLog = SystemLog.GetInstance();
                systemLog.AddLog("ACS Variable File Export 실패", LogType.Error);
                Debug.Print("ACS Variable File Export 에러");
                return false;
            }

            return true;
        }

        public double[] ReturnRealValueArray(IOType type)
        {
            return ACS.ReturnRealValueArray(type);
        }
    }

    public class ACSCommunication
    {
        #region Member
        Api ch;
        Timer tmrGetValues;

        bool isConnected = false;
        bool errorOn = false;
        //상위 클래스 값 그대로 받음. ACSCommunication는 Serializtion 저장이 안되는 문제 때문에.
        public List<ACSModuleInfo> Modules = new List<ACSModuleInfo>();

        int mATTimer = 100;

        string moduleNames = string.Empty;

        int[] arrDIRawValues;
        int[] arrDORawValues;
        int[] arrAIRawValues;
        int[] arrAORawValues;

        double[] arrDIRealValues;
        double[] arrDORealValues;
        double[] arrAIRealValues;
        double[] arrAORealValues;
        #endregion

        #region Method
        public bool Connect(bool isSimulator)
        {
            ch = new Api();

            string IP = "10.0.0.100";

            int Protocol = (int)EthernetCommOption.ACSC_SOCKET_STREAM_PORT;

            if (isSimulator)
            {
                ch.OpenCommSimulator();
            }
            else
            {
                try
                {
                    ch.OpenCommEthernet(IP, Protocol);
                }
                catch (ACSException)
                {
                    Debug.Print("ACS 연결 실패");
                    isConnected = false;
                    return false;
                }
            }

            if (ch.IsConnected)
            {
                tmrGetValues = new Timer(mATTimer);

                tmrGetValues.Elapsed += OnRun;
                tmrGetValues.Enabled = true;

                isConnected = true;
                Debug.Print("ACS 연결 성공");

                return true;
            }
            else
            {
                Debug.Print("ACS 연결 실패");

                isConnected = false;
                return false;
            }
        }

        public bool Disconnect()
        {
            if (tmrGetValues != null)
            {
                tmrGetValues.Enabled = false;
                tmrGetValues.Stop();
                tmrGetValues.Close();
            }

            try
            {
                isConnected = false;
                ch.CloseComm();
                return true;
            }
            catch (Exception)
            {
                Debug.Print("ACS Close 중 에러");
                System.Threading.Thread.Sleep(300);
                return false;
            }

        }
        public ACSModules SystemSetup(bool useHMI)
        {
            int EK1100 = 0;
            ACSModules modules = new ACSModules();
            String Slaves = ch.Transaction("?ECGETSLAVES()");
            int SLAVES = 0;
            int.TryParse(Slaves, out SLAVES);

            for (int i = 0; i < SLAVES; i++)
            {
                ACSModuleInfo NewModule = GetModule((int)(ch.GetEtherCATSlaveVendorID(i)), (int)(ch.GetEtherCATSlaveProductID(i)), i);
                Debug.Print(NewModule.ProductName);
                int a = (int)ch.GetEtherCATSlaveProductID(i);
                Debug.Print(a.ToString("X"));
                if (NewModule.ProductName == "EK1100")
                {
                    EK1100++;
                }

                if (useHMI && EK1100 == 2)
                {
                    NewModule.UseFor = UseFor.HMI;
                }
                else
                {
                    NewModule.UseFor = UseFor.DAQ;
                }
                modules.Add(NewModule);
                moduleNames = moduleNames + NewModule.ProductName;
            }
            Modules = modules;

            return modules;
        }
        public double[] ReturnRealValueArray(IOType type)
        {
            if (arrAIRealValues == null)
            {
                return null;
            }

            if (type == IOType.AI)
            {
                return arrAIRealValues;
            }
            else if (type == IOType.AO)
            {
                return arrAORealValues;
            }
            else if (type == IOType.DI)
            {
                return arrDIRealValues;
            }
            else
            {
                return arrDORealValues;
            }
        }
        public string ReturnModuleNames()
        {
            return moduleNames;
        }
        private void OnRun(Object source, ElapsedEventArgs e)
        {
            Read();
        }

        public void SetValue(IOType type, int channelNumber, double setIOValue)
        {
            double scalar = setIOValue;

            if (type == IOType.AO)
            {
                ch.WriteVariable(scalar, "g_eCAT_AO_Data", ProgramBuffer.ACSC_NONE, 0, 0, channelNumber, channelNumber);

            }
            else if (type == IOType.DO)
            {
                ch.WriteVariable(scalar, "g_eCAT_DO_Data", ProgramBuffer.ACSC_NONE, 0, 0, channelNumber, channelNumber);
            }
        }
        private void Read()
        {
            try
            {
                object Result;
                Result = ch.ReadVariable("g_eCAT_AI_Data", ProgramBuffer.ACSC_NONE, -1, -1, -1, -1);

                arrAIRawValues = (Result as IEnumerable).Cast<object>().Select(x => (int)x).ToArray();

                Result = ch.ReadVariable("g_eCAT_AO_Data", ProgramBuffer.ACSC_NONE, -1, -1, -1, -1);

                arrAORawValues = (Result as IEnumerable).Cast<object>().Select(x => (int)x).ToArray();

                Result = ch.ReadVariable("g_eCAT_DI_Data", ProgramBuffer.ACSC_NONE, -1, -1, -1, -1);

                arrDIRawValues = (Result as IEnumerable).Cast<object>().Select(x => (int)x).ToArray();

                Result = ch.ReadVariable("g_eCAT_DO_Data", ProgramBuffer.ACSC_NONE, -1, -1, -1, -1);

                arrDORawValues = (Result as IEnumerable).Cast<object>().Select(x => (int)x).ToArray();

                int lenAIRawArray = arrAIRawValues.Length;
                int lenAORawArray = arrAORawValues.Length;
                int lenDIRawArray = arrDIRawValues.Length;
                int lenDORawArray = arrDORawValues.Length;

                if (arrAIRealValues == null)
                {
                    arrAIRealValues = new double[lenAIRawArray];
                    arrAORealValues = new double[lenAORawArray];
                    arrDIRealValues = new double[lenDIRawArray];
                    arrDORealValues = new double[lenDORawArray];
                }

                int indexAI = 0;
                int indexAO = 0;
                int indexDI = 0;
                int indexDO = 0;

                //RawValue -> RealValue
                foreach (ACSModuleInfo module in Modules)
                {
                    if (module.IO == IOType.None)
                    {
                        continue;
                    }

                    if (module.IO == IOType.AI)
                    {
                        for (int i = indexAI; i < indexAI + module.ChannelEa; i++)
                        {
                            arrAIRealValues[i] = Math.Round(((double)arrAIRawValues[i] / module.ACSIOMax) * module.ModuleIOMax, 4);
                        }
                        indexAI = indexAI + module.ChannelEa;
                    }

                    if (module.IO == IOType.AO)
                    {
                        for (int i = indexAO; i < indexAO + module.ChannelEa; i++)
                        {
                            arrAORealValues[i] = Math.Round(((double)arrAORawValues[i] / module.ACSIOMax) * module.ModuleIOMax, 4);
                        }
                        indexAO = indexAO + module.ChannelEa;
                    }

                    if (module.IO == IOType.DI)
                    {

                        int loopCount = 1;

                        if (module.ChannelEa > 8)
                        {
                            loopCount = module.ChannelEa / 8;
                        }

                        for (int i = 0; i < loopCount; i++)
                        {
                            int value = arrDIRawValues[i];
                            BitArray ba = new BitArray(new int[] { value });

                            for (int j = 0; j < 8; j++)
                            {
                                int io;
                                if (ba[j])
                                {
                                    io = 1;
                                }
                                else
                                {
                                    io = 0;
                                }
                                arrDIRealValues[indexDO + j] = (io / module.ACSIOMax) * module.ModuleIOMax;
                            }
                            indexDI = indexDI + 8;
                        }
                    }

                    if (module.IO == IOType.DO)
                    {
                        int loopCount = 1;

                        if (module.ChannelEa > 8)
                        {
                            loopCount = module.ChannelEa / 8;
                        }

                        for (int i = 0; i < loopCount; i++)
                        {
                            int value = arrDORawValues[i];
                            BitArray ba = new BitArray(new int[] { value });

                            for (int j = 0; j < 8; j++)
                            {
                                int io;
                                if (ba[j])
                                {
                                    io = 1;
                                }
                                else
                                {
                                    io = 0;
                                }
                                arrDORealValues[indexDO + j] = (io / module.ACSIOMax) * module.ModuleIOMax;
                            }
                            indexDO = indexDO + 8;
                        }
                    }
                }


            }
            catch (ACSException)
            {
                errorOn = true;
            }
        }
        public bool ErrorCheck()
        {
            if (errorOn)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        private ACSModuleInfo GetModule(int VendorID, int ProductID, int SlaveNo)
        {
            ACSModuleInfo module;
            string ProductName = string.Empty;
            string ProductDesc = string.Empty;
            IOType IO = IOType.None;

            int OffsetBase = 0;
            int ChannelNo = 0;

            long ACSIOMin = 0;//Per channel
            long ACSIOMax = 0;//Per channel
            long ModuleIOMin = 0;//Per module
            long ModuleIOMax = 0;//Per module

            string Unit = string.Empty;
            switch (VendorID)
            {
                // BeckHoff Module
                case 0x02:
                    switch (ProductID)
                    {
                        case 0x44C2C52:
                            ProductName = "EK1100";
                            ProductDesc = "EK1100 EtherCAT Coupler";
                            IO = IOType.None;
                            OffsetBase = 0;
                            ChannelNo = 0;
                            ACSIOMin = 0;
                            ACSIOMax = 0;
                            ModuleIOMin = 0;
                            ModuleIOMax = 0;
                            Unit = string.Empty;
                            break;
                        case 0x03F03052:
                            ProductName = "EL1008";
                            ProductDesc = "EL1008 8Ch. Digital Input 24V, 3ms";
                            IO = IOType.DI;
                            OffsetBase = 1;
                            ChannelNo = 8;
                            ACSIOMin = 0;
                            ACSIOMax = 1;
                            ModuleIOMin = 0;
                            ModuleIOMax = 24;
                            Unit = "V";
                            break;
                        case 0x07113052:
                            ProductName = "EL1809";
                            ProductDesc = "EL1809 16Ch. Digital Input 24V DC, 3ms";
                            IO = IOType.DI;
                            OffsetBase = 1;
                            ChannelNo = 16;
                            ACSIOMin = 0;
                            ACSIOMax = 1;
                            ModuleIOMin = 0;
                            ModuleIOMax = 24;
                            Unit = "V";
                            break;
                        case 0x0BBC3052:
                            ProductName = "EL3004";
                            ProductDesc = "EL3004 4Ch. Analog Input +/- 10V single-ended, 12 bit";
                            IO = IOType.AI;
                            OffsetBase = 4;
                            ChannelNo = 4;
                            ACSIOMin = -32768;
                            ACSIOMax = 32767;
                            ModuleIOMin = -10;
                            ModuleIOMax = 10;
                            Unit = "V";
                            break;
                        case 0x0C203052:
                            ProductName = "EL3104";
                            ProductDesc = "EL3104 4Ch. Analog Input +/- 10V Diff, 16 bit";
                            IO = IOType.AI;
                            OffsetBase = 4;
                            ChannelNo = 4;
                            ACSIOMin = -32768;
                            ACSIOMax = 32767;
                            ModuleIOMin = -10;
                            ModuleIOMax = 10;
                            Unit = "V";
                            break;
                        case 0x0BF83052:
                            ProductName = "EL3064";
                            ProductDesc = "EL3064 4Ch. Analog Input 0 ~ 10V single-ended, 12 bit";
                            IO = IOType.AI;
                            OffsetBase = 4;
                            ChannelNo = 4;
                            ACSIOMin = 0;
                            ACSIOMax = 32767;
                            ModuleIOMin = 0;
                            ModuleIOMax = 10;
                            Unit = "V";
                            break;
                        case 0x0BC03052:
                            ProductName = "EL3008";
                            ProductDesc = "EL3008 8Ch. Analog Input +/- 10V single-ended, 12 bit";
                            IO = IOType.AI;
                            OffsetBase = 4;
                            ChannelNo = 8;
                            ACSIOMin = -32768;
                            ACSIOMax = 32767;
                            ModuleIOMin = -10;
                            ModuleIOMax = 10;
                            Unit = "V";
                            break;
                        case 0x0D173052:
                            ProductName = "EL3351";
                            ProductDesc = "EL3351 1Ch. Analog Input Resistor Bridge Terminal, Diff. 16 bit";
                            IO = IOType.AI;
                            OffsetBase = 6;
                            ChannelNo = 2;
                            ACSIOMin = -32768;
                            ACSIOMax = 32767;
                            ModuleIOMin = -10;
                            ModuleIOMax = 10;
                            Unit = "V";//?
                            break;
                        case 0x0C343052:
                            ProductName = "EL3124";
                            ProductDesc = "EL3124 4Ch. Analog Input 4-20mA Diff, 16 bit";
                            IO = IOType.AI;
                            OffsetBase = 4;
                            ChannelNo = 4;
                            ACSIOMin = 0;
                            ACSIOMax = 32767;
                            ModuleIOMin = 4;
                            ModuleIOMax = 20;
                            Unit = "mA";
                            break;
                        case 0x0C883052:
                            ProductName = "EL3208";
                            ProductDesc = "EL3208 8Ch. Analog Input PT100(RTD), 0.1 °C per digit";
                            IO = IOType.AI;
                            OffsetBase = 4;
                            ChannelNo = 8;
                            ACSIOMin = -2500;
                            ACSIOMax = 8500;
                            ModuleIOMin = -200;
                            ModuleIOMax = 850;
                            Unit = "°C";
                            break;
                        case 0x0C8E3052:
                            ProductName = "EL3214";
                            ProductDesc = "EL3214 4Ch. Analog Input PT100(RTD), for 3-wire connection";
                            IO = IOType.AI;
                            OffsetBase = 4;
                            ChannelNo = 4;
                            ACSIOMin = -2500;
                            ACSIOMax = 8500;
                            ModuleIOMin = -200;
                            ModuleIOMax = 850;
                            Unit = "°C";
                            break;

                        case 0x0CF63052:
                            ProductName = "EL3318";
                            ProductDesc = "EL3318 8Ch. Analog Thermocouple(TC) Input with Open-circuit Recognition diff";
                            IO = IOType.AI;
                            OffsetBase = 4;
                            ChannelNo = 8;
                            ACSIOMin = -2000;
                            ACSIOMax = 10000;
                            ModuleIOMin = -200;
                            ModuleIOMax = 1000;
                            Unit = "°C";
                            break;
                        //case 0x14203052:
                        //    ProductName = "EL5152";
                        //    ProductDesc = "EL5152 2Ch. Incremental encoder interface, 24 V HTL, 100 kHz"; //Position Measurement
                        //    IO = IOType.AI;
                        //    OffsetBase = 10;
                        //    ChannelNo = 2;
                        //    ACSIOMin = 0;
                        //    ACSIOMax = 3000000‬;
                        //    ModuleIOMin = 0;
                        //    ModuleIOMax = 30000006;
                        //    Unit = string.Empty;
                        //    break;
                        //case 0x141F3052:
                        //    ProductName = "EL5151";
                        //    ProductDesc = "EL5151 1Ch. Incremental encoder interface, 24 V HTL, 100 kHz";
                        //    IO = IOType.AI;
                        //    OffsetBase = 2;
                        //    ChannelNo = 2;
                        //    ACSIOMin = 0;
                        //    ACSIOMax = 3000000‬;
                        //    ModuleIOMin = 0;
                        //    ModuleIOMax = 3000000;
                        //    Unit = string.Empty;
                        //    break;
                        case 0x13ED3052:
                            ProductName = "EL5101";
                            ProductDesc = "EL5101 Incremental encoder interface, RS422, TTL, 1 MHz";
                            //ProductName = "EL3124";
                            //ProductDesc = "EL3124 4Ch. Analog Input 4-20mA Diff, 16 bit";
                            IO = IOType.AI;
                            OffsetBase = 4;
                            ChannelNo = 1;
                            ACSIOMin = 0;
                            ACSIOMax = 32767;
                            ModuleIOMin = 0;
                            ModuleIOMax = 32767;
                            Unit = "";
                            break;
                        case 0x07D83052:
                            ProductName = "EL2008";
                            ProductDesc = "EL2008 8Ch. Digital Output 24V DC, 0.5A";
                            IO = IOType.DO;
                            OffsetBase = 1;
                            ChannelNo = 8;
                            ACSIOMin = 0;
                            ACSIOMax = 1;
                            ModuleIOMin = 0;
                            ModuleIOMax = 24;
                            Unit = "V";
                            break;
                        case 0x0B493052:
                            ProductName = "EL2889";
                            ProductDesc = "EL2889 16Ch. Digital Output 24V, 0.5A, Ground switching";
                            IO = IOType.DO;
                            OffsetBase = 1;
                            ChannelNo = 16;
                            ACSIOMin = 0;
                            ACSIOMax = 1;
                            ModuleIOMin = 0;
                            ModuleIOMax = 24;
                            Unit = "V";
                            break;
                        case 0x0AF93052:
                            ProductName = "EL2809";
                            ProductDesc = "EL2809 16Ch. Digital Output 24V DC, 0.5A";
                            IO = IOType.DO;
                            OffsetBase = 1;
                            ChannelNo = 16;
                            ACSIOMin = 0;
                            ACSIOMax = 1;
                            ModuleIOMin = 0;
                            ModuleIOMax = 24;
                            Unit = "V";
                            break;
                        case 0x0FA43052:
                            ProductName = "EL4004";
                            ProductDesc = "EL4004 4Ch. Analog Output 0 ~ 10V, 12 bit";
                            IO = IOType.AO;
                            OffsetBase = 2;
                            ChannelNo = 4;
                            ACSIOMin = 0;
                            ACSIOMax = 32767;
                            ModuleIOMin = 0;
                            ModuleIOMax = 10;
                            Unit = "V";
                            break;
                        case 0x0FC23052:
                            ProductName = "EL4034";
                            ProductDesc = "EL4034 4Ch. Analog Output +/- 10V, 12 bit";
                            IO = IOType.AO;
                            OffsetBase = 2;
                            ChannelNo = 4;
                            ACSIOMin = -32768;
                            ACSIOMax = 32767;
                            ModuleIOMin = -10;
                            ModuleIOMax = 10;
                            Unit = "V";
                            break;
                        case 0x0FC63052:
                            ProductName = "EL4038";
                            ProductDesc = "EL4038 8Ch. Analog Output +/- 10V, 12 bit";
                            IO = IOType.AO;
                            OffsetBase = 2;
                            ChannelNo = 4;
                            ACSIOMin = -32768;
                            ACSIOMax = 32767;
                            ModuleIOMin = -10;
                            ModuleIOMax = 10;
                            Unit = "V";
                            break;
                        case 0x10263052:
                            ProductName = "EL4134";
                            ProductDesc = "EL4134 4Ch. Analog Output +/- 10V, 16 bit";
                            IO = IOType.AO;
                            OffsetBase = 2;
                            ChannelNo = 4;
                            ACSIOMin = -32768;
                            ACSIOMax = 32767;
                            ModuleIOMin = -10;
                            ModuleIOMax = 10;
                            Unit = "V";
                            break;
                        case 0x17723052:
                            ProductName = "EL6002";
                            ProductDesc = "EL6002 4Ch. Serial Interface RS232, D-sub connection";
                            IO = IOType.None;
                            OffsetBase = 24;
                            ChannelNo = 2;
                            ACSIOMin = -32768;
                            ACSIOMax = 32767;
                            ModuleIOMin = -32768;
                            ModuleIOMax = 32767;
                            Unit = string.Empty;
                            break;
                        default:
                            ProductName = "UNKNOWN";
                            ProductDesc = "UNKNOWN";
                            IO = IOType.None;
                            OffsetBase = 0;
                            ChannelNo = 0;
                            ACSIOMin = -32768;
                            ACSIOMax = 32767;
                            ModuleIOMin = -32768;
                            ModuleIOMax = 32767;
                            Unit = string.Empty;
                            break;
                    }
                    break;
            }
            module = new ACSModuleInfo(ProductName, ProductDesc, VendorID, ProductID, IO, OffsetBase, SlaveNo, ChannelNo, ACSIOMin, ACSIOMax, ModuleIOMin, ModuleIOMax, Unit);

            return module;
        }


        #endregion
    }

}
