﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace ATOM_Class
{
    public enum CommunicationType
    {
        ACS,
        CAN,
        ABBDrive,
        OvalFC200,
        WTC,
        FTC,
        OTC,
    }

    [Serializable]
    public class CommunicationConnector
    {
        private static CommunicationConnector communicationConnector = null;

        List<ICommunication> Communications = new List<ICommunication>();

        public static CommunicationConnector GetInstance()
        {
            if (communicationConnector == null)
            {
                communicationConnector = Load(AppDomain.CurrentDomain.BaseDirectory + "\\Config\\CommunicationConfig.bin");
            }

            return communicationConnector;
        }
        public void Save(string saveFilePath)
        {

            FileStream fileStreamObject;
            string FileName = saveFilePath;
            fileStreamObject = new FileStream(FileName, FileMode.Create);
            BinaryFormatter binaryFormatter = new BinaryFormatter();
            binaryFormatter.Serialize(fileStreamObject, this);
            fileStreamObject.Close();
        }
        public static CommunicationConnector Load(string loadFilePath)
        {
            try
            {
                FileStream fileStreamObject;
                string FileName = loadFilePath;
                fileStreamObject = new FileStream(FileName, FileMode.Open);
                BinaryFormatter binaryFormatter = new BinaryFormatter();
                object dd = binaryFormatter.Deserialize(fileStreamObject);
                fileStreamObject.Close();

                return (CommunicationConnector)dd;
            }
            catch (Exception)
            {
                return new CommunicationConnector();
            }
        }

        public void Connect()
        {
            foreach (ICommunication com in Communications)
            {
                com.Connect();
                System.Threading.Thread.Sleep(100);
            }
        }
        public void Disconnect()
        {
            foreach (ICommunication com in Communications)
            {
                com.Disconnect();
                System.Threading.Thread.Sleep(100);
            }
        }
        public List<ICommunication> GetCommunications()
        {
            return Communications;
        }
        public CommunicationType GetCommTypeByVariable(VariableInfo var)
        {
            foreach (ICommunication com in Communications)
            {
                if (com.Variables.Contains(var))
                {
                    return com.CommType;
                }
            }

            return CommunicationType.ACS;
        }
        public void ResetVariableValues()
        {
            foreach (ICommunication com in Communications)
            {
                foreach (VariableInfo var in com.Variables)
                {
                    var.IOValue = 0;
                }
            }
        }
        public void ResetCommunication()
        {
            foreach (ICommunication com in Communications)
            {
                com.IsConnected = false;
            }
        }
        public List<VariableInfo> GetVariables(CommunicationType commType)
        {
            return FindCommunication(commType).Variables;
        }
        public void AddCommunication(ICommunication communication)
        {
            Communications.Add(communication);
        }
        public void RemoveCommunication(CommunicationType commType)
        {
            bool flag = false;
            int index = -1;

            foreach (ICommunication com in Communications)
            {
                index++;
                if (com.CommType == commType)
                {
                    flag = true;
                    break;
                }
            }

            if (flag)
            {
                Communications.RemoveAt(index);
            }
        }
        public ICommunication FindCommunication(CommunicationType commType)
        {
            foreach (ICommunication com in Communications)
            {
                if (com.CommType == commType)
                {
                    return com;
                }
            }

            return null;
        }

        public VariableInfo FindVariable(string varName)
        {
            foreach (ICommunication com in Communications)
            {
                foreach (VariableInfo var in com.Variables)
                {
                    if (var.Name == varName)
                    {
                        return var;
                    }
                }
            }

            return null;
        }
    }

}
